﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Concept.BLL.BaseBD;
using Concept.BLL.BaseBD.Log;
using Concept.Entity.BaseDB;
using Concept.Entity.BaseDB.Log;
using Concept.Entity.SysMode;
using ConceptApiController.BLL;
using ConceptCommon;
using ConceptCommon.ReturnMode; 

namespace ConceptApiController
{
    public class CardController: BaseController
    {

        /// <summary>
        ///  获取card对应dashboard信息
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        [HttpGet]
        public NormalReturn<CardSum> GetCardSum(int cardId,string externalPjId)
        { 
            CardSum sum = new CardSum();
            sum.answers = new List<QuestionAnswer>();
            NormalReturn<CardSum> nr = new NormalReturn<CardSum> {
               Message="成功",
               Result=0
            };
        
            try
            {
                var card=ConceptCardBLL.GetConceptCardByCardId(cardId);
                
                sum.cardName = card.ConceptCardName;
                sum.recyCount= AdoptAnswerBLL.GetAnswerCount(card.CardId,externalPjId);
                var project=ProjectBLL.GetProjectByAutoId(card.ProjectAutoId);
                sum.allCount = VariableAnswerBLL.GetProjectUserCount(externalPjId);
                sum.notInvolved=sum.allCount-sum.recyCount;
                var questions = GradingQuestionBLL.GetGradingQuestionList(card.ProjectAutoId);

                List<AdoptAnswer> answers= AdoptAnswerBLL.Search(cardId,externalPjId);
                foreach (var item in questions)
                {
                    QuestionAnswer data = new QuestionAnswer();
                    data.Name = item.QuestionText;
                    data.Scores = (from t in answers
                                   where t.QuestionId == item.GradingQuestionId 
                                   group t by t.Value into g
                                   let count = answers.Count(a => a.QuestionId == item.GradingQuestionId)
                                   select new ScoreData { Score = g.Key, Count = g.Count(), Percent = g.Count()/ (count * 1.0f) }).ToList();

                    var newList = new List<ScoreData>();
                    for (int i = 0; i < 5; i++)
                    {
                        var score = new ScoreData();
                        score.Score = (i + 1).ToString();
                        var findScore = data.Scores.FirstOrDefault(s => s.Score == score.Score);
                        score.Count = findScore == null ? 0 : findScore.Count;
                        score.Percent = findScore == null ? 0 : (float)Math.Round(findScore.Percent*100);
                        newList.Add(score);
                    }
                    
                    data.Scores = newList;
                    data.SumCount = answers.Where(a => a.QuestionId == item.GradingQuestionId).Sum(x=>int.Parse(x.Value));
                    data.Average = sum.recyCount == 0 ? "0" : String.Format("{0:N1}", (data.SumCount / (sum.recyCount * 1.0f)));
                    sum.answers.Add(data);
                }
                
                nr.data = sum;
                 
                return nr;
            }
            catch (Exception ex)
            {
                nr.Message = ex.ToString();
            }

            return nr;
        }
        /// <summary>
        /// 打分表情包报告
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        [HttpGet]
        public GradingReportReturn GetGradingIconReport(int projectAutoId,string externalPjId,string groupid="")
        {
            GradingReportReturn result = new GradingReportReturn();
            try
            { 
                return  ReportLogical.Instance.GetGradingEmotionDataNew(projectAutoId,externalPjId,groupid); 
            }
            catch (Exception ex)
            {
                LogLogical.Instance.AddException(ex, "ReportController-GetGradingIconReport==projectAutoId:" + projectAutoId);
                return null;
            }

            return result;
        }
     
        /// <summary>
        /// 排序报告
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        [HttpGet]
        public OrderReportReturn GetOrderReport(int projectAutoId,string externalPjId,string groupid="")
        {
            OrderReportReturn result = new OrderReportReturn();
            try
            { 
                return  ReportLogical.Instance.GetOrderDataNew(projectAutoId,externalPjId,groupid); 
            }
            catch (Exception ex)
            {
                LogLogical.Instance.AddException(ex, "ReportController-GetOrderReport==projectAutoId:" + projectAutoId);
                return null;
            }

            return result;
        }

        /// <summary>
        /// 分堆报告
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        [HttpGet]
        public ClassifyReportReturn GetClassifyReport(int projectAutoId,string externalpjid="",string groupid="")
        {
            ClassifyReportReturn result = new ClassifyReportReturn();
            try
            {
                return ReportLogical.Instance.GetClassifyDataNew(projectAutoId,externalpjid,groupid); 
            }
            catch (Exception ex)
            {
                LogLogical.Instance.AddException(ex, "ReportController-GetClassifyReport==projectAutoId:" + projectAutoId);
                return null;
            }

            return result;
        }

        /// <summary>
        /// 涂鸦报告
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        [HttpGet]
        public PaintingReportReturn GetPaintingReport(int projectAutoId,string externalPjId,string groupid="")
        {
            PaintingReportReturn result = new PaintingReportReturn();
            try
            {
                result.header = new List<string>(); 
                result.header.Add("用户ID");
                result.header.Add("概念卡"); 

                List<Group> groupList = GroupBLL.GetGroupList(projectAutoId); 
                List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId,externalPjId,groupid).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();
                List<ConceptCard> conceptCardList = ConceptCardBLL.GetConceptCardList(projectAutoId); 
                
                if (groupList != null && groupList.Count > 0)
                {
                    result.groups = new List<PaintingGroup>();
                    foreach (var groupItem in groupList)
                    {

                        PaintingGroup paintingGroup = new PaintingGroup();
                        paintingGroup.name = groupItem.GroupName;
                        paintingGroup.device = new List<PaintingDeviceInfo>();
                        var groupByDeviceAnswers = AllAnswerList.Where(a => a.GroupId == groupItem.GroupId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.Painting)
                                                                .GroupBy(pet => pet.DeviceId)
                                                                .Select(g => (new { DeviceId = g.Key, Answers = g.ToList() }));
                        foreach (var device in groupByDeviceAnswers)
                        {
                            PaintingDeviceInfo paintingDeviceInfoItem = new PaintingDeviceInfo();
                            paintingDeviceInfoItem.cards = new List<PaintingCard>();
                            paintingDeviceInfoItem.deviceId = VariableAnswerBLL.GetUserNameByOpenId(device.DeviceId);


                            if (conceptCardList!=null && conceptCardList.Count>0)
                            {
                                foreach(var card in conceptCardList)
                                {
                                   PaintingCard paintingCardItem = new PaintingCard();
                                   var thisCardAnswer = device.Answers.Where(a => a.CardId == card.CardId).FirstOrDefault();
                                   if(thisCardAnswer!=null)
                                   {
                                       string DrawingimagePath = urlMatch(thisCardAnswer.QuestionAnswer); 
                                       paintingCardItem.name = "C" + card.CardOrder;
                                       paintingCardItem.url = DrawingimagePath;
                                       paintingCardItem.detailName = card.ConceptCardName;
                                       paintingDeviceInfoItem.cards.Add(paintingCardItem);
                                   }
                                }
                            }
                           // paintingDeviceInfoItem.details = VariableAnswerBLL.GetUserDetail(paintingDeviceInfoItem.deviceId);
                            paintingGroup.device.Add(paintingDeviceInfoItem);
                        }
                        result.groups.Add(paintingGroup);
                    }
                } 
            }
            catch (Exception ex)
            {
                LogLogical.Instance.AddException(ex, "ReportController-GetPaintingReport==projectAutoId:" + projectAutoId);
                return null;
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <param name="reportType"></param>
        [HttpGet]
        public void DownLoadData(int projectAutoId, int reportType)
        {
            List<List<string>> dataList = new List<List<string>>();
            switch (reportType)
            {
                //case 1: dataList = ReportLogical.Instance.GetGradingEmotionData(projectAutoId); break;
                //case 2: dataList = ReportLogical.Instance.GetOrderData(projectAutoId); break;
                case 3: dataList = ReportLogical.Instance.GetClassifyData(projectAutoId); break;
                case 4: dataList = ReportLogical.Instance.GetPaintingData(projectAutoId); break;
                default:break;
            } 
            if(dataList.Count>0)
            {
                string rawDataPath = ConfigurationManager.AppSettings["DownloadData"];
                string filename = reportType + "_report.xlsx";
                if (!Directory.Exists(rawDataPath + projectAutoId + "\\"))
                {
                    Directory.CreateDirectory(rawDataPath + projectAutoId + "\\");
                }
                if (Directory.Exists(rawDataPath + projectAutoId + "\\" + filename))
                {
                    Directory.Delete(rawDataPath + projectAutoId + "\\" + filename);
                }
                DataSet ds = new DataSet();
                DataTable dt = ReportLogical.Instance.TransferToDT(dataList); 
                ds.Tables.Add(dt);
                CreateExcelFile.CreateExcelDocument(ds, rawDataPath + projectAutoId + "\\" + filename);
                HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings["DisplayDownloadData"] + projectAutoId + "/" + filename);
            }
        }

        [HttpGet]
        public GradingTotalReturn GetGradingTotalReport(int projectAutoId,string externalpjid="",string groupid="")
        {
            GradingTotalReturn result = new GradingTotalReturn();
            try
            {
                return ReportLogical.Instance.GetGradingTotalNew(projectAutoId,externalpjid,groupid);
                //result.Header = new List<string>() { "", "维度1", "维度2", "维度3", "维度总分" };
                //List<List<string>> c = new List<List<string>>();
                //List<string> r1 = new List<string>() { "概念卡A", "20","15","23","58"};
                //c.Add(r1);
                //List<string> r2 = new List<string>() { "概念卡B", "10", "25", "43", "78" };
                //c.Add(r2);
                ////List<string> r3 = new List<string>() { "结果", "概念卡A", "概念卡B", "概念卡B", "概念卡B" };
                ////c.Add(r3);
                //List<string> r3 = new List<string>() { "结果"};
                

                //for (int j = 1; j < c[0].Count; j++)
                //{
                //    string dd = c.Select(cn => new { cardName = cn[0], dimValue = cn[j] }).OrderByDescending(i => i.dimValue).FirstOrDefault().cardName;
                //    r3.Add(dd);
                //}
                //c.Add(r3);
                //result.Contents = c;
            }
            catch (Exception ex)
            {
                LogLogical.Instance.AddException(ex, "ReportController-GetGradingTotalReport==projectAutoId:" + projectAutoId); 
                return null;
            }
            return result;
        }
        private string urlMatch(string map)
        {
            if (map.IndexOf("http") > -1)
                return map;
            else
                return ConfigurationManager.AppSettings["DisplayPaintingCardImage"] + map;
        }
     

    }
}
