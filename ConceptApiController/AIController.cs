﻿using Concept.BLL.BaseBD;
using Concept.Entity.BaseDB;
using Concept.Entity.EnumMode;
using Concept.Entity.External;
using Concept.Entity.SysMode;
using ConceptCommon.ReturnMode;
using ExternalAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace ConceptApiController
{
   public class AIController:BaseController
    {
       
          readonly   Dictionary<SearchStatusEnum, string> dictionary = new Dictionary<SearchStatusEnum, string>() {
                 {SearchStatusEnum.dealing,"正在计算..."},
                { SearchStatusEnum.done,"计算完成"},
                {SearchStatusEnum.error,"百度目前无法对该数据进行计算，具体原因请咨询管理员!" },
              {SearchStatusEnum.topicNotEnded,"还没有触发计算，如果是全局计算请确认已经结束话题！" }
            };

        /// <summary>
        /// 获取wordcloud 数据。
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpGet]
        public NormalReturn<ExternalAIData> GetWordCloudData(string externalPjId, string groupId, string topicId)
        {

            NormalReturn<ExternalAIData> wordClouds = new NormalReturn<ExternalAIData>()
            {
                Result = 0,
                Message = "接口调用成功",
            };
            try
            {
                SearchStatusEnum status = TaskParamLogBLL.GetStatus(externalPjId,groupId,topicId);
                if (status == SearchStatusEnum.done)
                {
                    var rowData = ExternalJsonBLL.GetWordCloudData(externalPjId, groupId, topicId);
                    wordClouds.data = JsonConvert.DeserializeObject<ExternalAIData>(rowData.JSON);
                }
                else
                {
                    wordClouds.Result = -1;
                }
                wordClouds.Message = dictionary[status];
            }
            catch (System.Exception e)
            {
                throw e;
            }
            return wordClouds;
        }
        
        /// <summary>
        /// 获取聊天数据。
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpGet]
        public List<Message> GetTopicInfo(string externalPjId, string groupId, string topicId)
        {

            try
            {
                // var data = MessageBLL.GetMessages(externalPjId, groupId, topicId);
                 var data = MessageBLL.GetMessagesWithOpenId(externalPjId, groupId, topicId);
                return data;
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }
    }
}
