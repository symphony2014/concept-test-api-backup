﻿using Concept.Entity.BaseDB;
using Concept.Entity.SysMode;
using System;
using System.Collections.Generic;
using Concept.BLL.BaseBD;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Web;
using System.IO;
using ConceptCommon.ReturnMode;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using ConceptCommon;
using ConceptCommon.ConvertMethod;
using ConceptApiController.SyncReport;
using Concept.Entity.EnumMode;

namespace ConceptApiController
{
    public class MobileController: BaseController
    {
        #region 获取项目信息及压缩zip包文件
        /// <summary>
        /// 图片资源zip包压缩
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        private string Zip(int projectAutoId)
        {
            // 根路径
            // string rootPath = HttpContext.Current.Server.MapPath("~");
             string rootPath = @"D:\svn\conceptTest\ConceptWebApi\";
            // 卡片存放根路径
            string resRootPath = ConfigurationManager.AppSettings["UploadCardImage"];
            // 表情图片存放路径
            string emojiPath = rootPath + "Images";
            // 卡片存放目录
            string resPath = resRootPath + projectAutoId;
            // 所有的表情图片
            List<string> emojiNames = Directory.EnumerateFiles(emojiPath).ToList();

            foreach (string emoji in emojiNames)
            {
                string[] strs = emoji.Split('\\');
                string destFileName = resPath + "\\" + strs[strs.Length - 1];
                // 删除存在的表情图片
                if (File.Exists(destFileName))
                {
                    File.Delete(destFileName);
                }
                // 拷贝表情图片
                File.Copy(emoji, destFileName);
            }
            // 压缩后zip文件路径
            string zipPath = resRootPath + projectAutoId + ".zip";
            // 删除现有zip包
            if (File.Exists(zipPath))
            {
                File.Delete(zipPath);
            }
            // 压缩文件
            ZipClass.GetInstance().Zip(resPath, zipPath);

            return zipPath;

        }

        [HttpGet]
        public NormalReturn<ProjectInfo> GetProjectInfo(string pjid,Boolean needZip=true)
        {
            var baseUrl = ConfigurationManager.AppSettings["domain"];
            NormalReturn<ProjectInfo> result = new NormalReturn<ProjectInfo>();
            result.Result = 1;

            #region 项目信息
            Project p = ProjectBLL.GetProjectById(pjid);
            if (null == p)
            {
                result.Result = 0;
                result.Message = "不存在的项目ID";
                return result;
            }

            ProjectInfo projectInfo = new ProjectInfo();
            projectInfo.ProjectId = p.ProjectId;
            projectInfo.ProjectName = p.ProjectName;
            projectInfo.ProjectAutoId = p.ProjectAutoId;
            projectInfo.IsNeedDoodled = p.IsNeedDoodled;
            projectInfo.ModuleOrder = p.ModuleOrder;
            #endregion 

            #region  项目的group信息
            List<Group> Groups = GroupBLL.GetGroupList(p.ProjectAutoId);
            if (Groups != null && Groups.Count > 0)
            {
                projectInfo.Groups = new List<GroupInfo>();
                GroupInfo group = null;
                Groups.ForEach(r =>
                {
                    group = new GroupInfo();
                    group.GroupName = r.GroupName;
                    group.GroupId = r.GroupId;
                    group.CardOrder = r.CardOrder;

                    projectInfo.Groups.Add(group);
                });
            }
            #endregion

            #region  项目的卡片信息
            List<ConceptCard> Cards = ConceptCardBLL.GetConceptCardList(p.ProjectAutoId);
            if (Cards != null && Cards.Count > 0)
            {
                projectInfo.Cards = new List<CardInfo>();
                CardInfo card = null;
                Cards.ForEach(r =>
                {
                    card = new CardInfo();
                    card.CardOrder = r.CardOrder;
                    card.CardId = r.CardId;
                    card.CardName = r.ConceptCardName;
                    card.CardImageUrl = Path.Combine(baseUrl,"UploadCardImage",r.ImageUrl).Replace('/','\\');
                    if (!string.IsNullOrEmpty(r.ImageUrl))
                    {
                        string[] strs = r.ImageUrl.Split('/');
                        if (strs != null && strs.Length > 0)
                        {
                            card.CardImageName = strs[1];
                        }
                    }
                    projectInfo.Cards.Add(card);
                });
            }
            #endregion

            #region 压缩Zip包，获取zip包的大小和下载地址
            try
            {
                if (needZip)
                {
                    string zipPath = Zip(p.ProjectAutoId);
                    if (File.Exists(zipPath))
                    {
                        // zip包的大小
                        FileInfo fileInfo = new FileInfo(zipPath);
                        projectInfo.ResZipSize = fileInfo.Length;

                        // zip包的下载路径
                        //projectInfo.ResZipPath = zipPath;
                    }
                }
            }
            catch (Exception e)
            {
                projectInfo.ResZipPath = string.Empty;
                projectInfo.ResZipSize = 0;
                result.Result = 0;
                result.Message = "获取资源Zip包信息失败！ " + e.Message;
                return result;
            }
            
            #endregion
            
            projectInfo.Questions = new QuestionInfo();

            #region 评分页面的题
            GradingEmoticonQuestion QuestionInfo = GradingEmoticonQuestionBLL.GetGradingEmoticonQuestion(p.ProjectAutoId);
            if (null != QuestionInfo)
            {

                #region 表情包题
                if (QuestionInfo.IsShowEmoticon == 1 && !string.IsNullOrEmpty(QuestionInfo.EmoticonDesc))
                {
                    EmoticonQuestionInfo eq = new EmoticonQuestionInfo();
                    eq.QuestionText = QuestionInfo.EmoticonDesc;
                    eq.QuestionType = QuestionType.Icon;

                    List<EmoticonQuestionOption> Options = EmoticonQuestionOptionBLL.GetEmoticonQuestionOptionList(p.ProjectAutoId);
                    if (null != Options && Options.Count > 0)
                    {
                        // 只需要属性是显示并且有描述文字的表情
                        Options = Options.Where(r => r.IsShow == 1 && !string.IsNullOrEmpty(r.IconDesc)).ToList();
                        eq.Options = new List<EmoticonQuestionOptionInfo>();
                        EmoticonQuestionOptionInfo option = null;
                        List<Icon> icons = IconBLL.GetIconList();
                        Options.ForEach(r =>
                        {
                            option = new EmoticonQuestionOptionInfo();
                            option.OptionId = r.EmoticonOptionId;
                            option.OptionText = r.IconDesc;
                            option.OrderId = r.OrderId;
                            option.Score = r.Score;
                            Icon icon = icons.Find(i => i.IconId == r.IconId);
                            if (icon != null && icon.IconUrl != null)
                            {
                                string[] strs = icon.IconUrl.Split('/');
                                if (strs != null && strs.Length > 0)
                                {
                                    option.IconName = strs[2];
                                }
                            }

                            eq.Options.Add(option);

                        });
                    }

                    projectInfo.Questions.EmoticonQuestion = eq;
                }
                #endregion

                #region 开放题
                if (QuestionInfo.IsShowOpenQuestion == 1 && !string.IsNullOrEmpty(QuestionInfo.OpenQuestion))
                {
                    OpenQuestionInfo oq = new OpenQuestionInfo();
                    oq.QuestionText = QuestionInfo.OpenQuestion;
                    oq.QuestionType = QuestionType.OpenText;
                    projectInfo.Questions.OpenQuestion = oq;
                }
                #endregion

                #region 评分题
                if (QuestionInfo.IsShowGrading == 1)
                {
                    List<GradingQuestion> gradingQuesrtions = GradingQuestionBLL.GetGradingQuestionList(p.ProjectAutoId);
                    if (gradingQuesrtions != null && gradingQuesrtions.Count > 0)
                    {
                        // 没有题干的评分提默认不出示
                        gradingQuesrtions = gradingQuesrtions.Where(r => !string.IsNullOrEmpty(r.QuestionText)).ToList();
                        List<GradingQuestionInfo> questions = new List<GradingQuestionInfo>();
                        gradingQuesrtions.ForEach(r =>
                        {
                            GradingQuestionInfo gq = new GradingQuestionInfo();
                            gq.OrderId = r.OrderId;
                            gq.QuestionId = r.GradingQuestionId;
                            gq.QuestionText = r.QuestionText;
                            gq.QuestionType = QuestionType.Grading;
                            questions.Add(gq);
                        });

                        projectInfo.Questions.GradingQuestions = questions;
                    }
                }
                #endregion
            }
            #endregion

            #region 排序题
            List<OrderQuestion> orderqs =  OrderQuestionBLL.GetOrderQuestionList(p.ProjectAutoId);

            if (null != orderqs && orderqs.Count > 0)
            {
                orderqs = orderqs.Where(r => !string.IsNullOrEmpty(r.OrderQuestionText)).ToList();
            }

            if (orderqs != null && orderqs.Count > 0)
            {
                List<OrderQuestionInfo> questions = new List<OrderQuestionInfo>();

                orderqs.ForEach(r =>
                {
                    OrderQuestionInfo question = new OrderQuestionInfo();
                    question.OrderId = r.OrderId;
                    question.QuestionId = r.OrderQuestionId;
                    question.QuestionText = r.OrderQuestionText;
                    question.QuestionType = QuestionType.OrderQuestion;
                    questions.Add(question);
                });
                projectInfo.Questions.OrderQuestions = questions;
            }
            #endregion

            #region 分类题
            List<ClassifyQuestion> classifyqs = ClassifyQuestionBLL.GetClassifyQuestionList(p.ProjectAutoId);
            if (classifyqs != null && classifyqs.Count > 0)
            {
                // 没有题干的排序题也默认不出示
                classifyqs = classifyqs.Where(r => !string.IsNullOrEmpty(r.ClassifyQuestionText)).ToList();
                List<ClassifyQuestionInfo> questions = new List<ClassifyQuestionInfo>();
                classifyqs.ForEach(r =>
                {
                    ClassifyQuestionInfo question = new ClassifyQuestionInfo();
                    question.OrderId = r.OrderId;
                    question.QuestionId = r.ClassifyQuestionId;
                    question.QuestionText = r.ClassifyQuestionText;
                    question.QuestionType = QuestionType.CategoryQuestion;
                    questions.Add(question);
                });
                projectInfo.Questions.ClassifyQuestions = questions;
            }
            #endregion

            //#region 涂鸦题
            projectInfo.Questions.DoodledQuestions = new List<DoodledQuestionInfo>();
            projectInfo.Questions.DoodledQuestions.Add(new DoodledQuestionInfo()
            {
                QuestionType= QuestionType.Doodle
            });
            //#endregion
            result.data = projectInfo;

            return result;
        }
        #endregion

        #region 提交答案
        [HttpPost]
        public NormalListReturn<object> SubmitAnswer(List<DeviceAnswer> answers,bool needRedirect=false)
        {

            NormalListReturn<object> result = new NormalListReturn<object>();
            result.Result = 1;

            if (answers == null || answers.Count == 0)
            {
                result.Result = 0;
                result.Message = "回传数据中没有答案！";
                return result;
            }
            
            // 保存答案
            answers.ForEach(r =>
            {
                try
                {
                    r.CreateDate = DateTime.Now;
                    int i = DeviceAnswerBLL.AddItem(r);
                    
                    //拆分答案
                    AdoptAnswerBLL.SaveFromDeviceAnswer(r);
                    if (i < 0)
                    {
                        if (result.data == null)
                        {
                            result.data = new List<object>();
                        }

                        result.data.Add(new { QuestionType = r.QuestionType, QuestionId = r.QuestionId });
                    }
                }
                catch (Exception e)
                {
                    if (result.data == null)
                    {
                        result.data = new List<object>();
                    }

                    result.data.Add(new {QuestionType = r.QuestionType, QuestionId = r.QuestionId});
                }
            });

            if (result.data != null && result.data.Count > 0)
            {
                result.Result = 0;
                result.Message = "有提交失败的答案";
                result.TotalCount = result.data.Count;
            }

            ////提交数据到report
            //Sync sync = new Sync(answers.FirstOrDefault().ProjectAutoId);
            //Task<IpsosReport.ModelBiz.External.ReturnData> task=sync.SyncToReport();
            //task.Wait();
            //var data = task.Result;

            //拆分答案
            //跳转到concept链接
            if (needRedirect)
            {
                string returnUrl = ConceptLinkBLL.GetConceptLinkByGroupOpenIdOrMobileByAppend(answers[0].AppendData+"&openId="+answers[0].DeviceId);
            }
            return result;
        }




        #endregion
        #region 提交答案
        [HttpPost]
        public NormalListReturn<object> SubmitAnswerData(DeviceAnswerData data)
        {
           
            NormalListReturn<object> result = new NormalListReturn<object>();
            result.Result = 1;

            if (data.answers == null || data.answers.Count == 0)
            {
                result.Result = 0;
                result.Message = "回传数据中没有答案！";
                return result;
            }
            //替换Icon题的答案
            for (int i = 0; i < data.answers.Count; i++)
            {
                var text = data.answers[i].QuestionAnswer;
                if (data.answers[i].QuestionType == DeviceAnswerQuestionTypeEnum.Icon && !string.IsNullOrWhiteSpace(text))
                {
                    var ids = new List<string>();
                    foreach (var item in text.Split(','))
                    {
                    ids.Add( EmoticonQuestionOptionBLL.get(item).IconId.ToString());
                    }
                    data.answers[i].QuestionAnswer = string.Join(",",ids);
                }
            }

            // 保存答案
            data.answers.ForEach(r =>
            {
                try
                {
                    r.CreateDate = DateTime.Now;
                    int i = DeviceAnswerBLL.AddItem(r);

                    //拆分答案
                    AdoptAnswerBLL.SaveFromDeviceAnswer(r);
                    if (i < 0)
                    {
                        if (result.data == null)
                        {
                            result.data = new List<object>();
                        }

                        result.data.Add(new { QuestionType = r.QuestionType, QuestionId = r.QuestionId });
                    }
                }
                catch (Exception e)
                {
                    if (result.data == null)
                    {
                        result.data = new List<object>();
                    }

                    result.data.Add(new { QuestionType = r.QuestionType, QuestionId = r.QuestionId });
                }
            });

            if (result.data != null && result.data.Count > 0)
            {
                result.Result = 0;
                result.Message = "有提交失败的答案";
                result.TotalCount = result.data.Count;
            }

            ////提交数据到report
            //Sync sync = new Sync(answers.FirstOrDefault().ProjectAutoId);
            //Task<IpsosReport.ModelBiz.External.ReturnData> task=sync.SyncToReport();
            //task.Wait();
            //var data = task.Result;

            //拆分答案
            //跳转到concept链接
            //if (needRedirect)
            //{
            //    string returnUrl = ConceptLinkBLL.GetConceptLinkByGroupOpenIdOrMobileByAppend(answers[0].AppendData + "&openId=" + answers[0].DeviceId);
            //}
            return result;
        }




        #endregion
        #region 下载文件
        [HttpGet]
        public NormalReturn<byte[]> DownloadMedias(int pai, long startPos, long endPos)
        {

            NormalReturn<byte[]> result = new NormalReturn<byte[]>();
            result.Result = 1;

            // zip包文件
            string zipFile = ConfigurationManager.AppSettings["UploadCardImage"] + pai + ".zip";

            if (!File.Exists(zipFile))
            {
                result.Result = 0;
                result.Message = "未找到该项目的媒体文件";
                return result;
            }

            FileStream fileStream = new FileStream(zipFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

            if (fileStream.Length < endPos)
            {
                result.Result = 0;
                result.Message = "参数错误：结束位置超出文件大小";
                fileStream.Close();
                return result;
            }

            fileStream.Seek((int)startPos, SeekOrigin.Begin);
            byte[] sendBuffers = new byte[endPos - startPos];
            fileStream.Read(sendBuffers, 0, sendBuffers.Length);
            result.data = sendBuffers;
            fileStream.Close();
            return result;
        }

        [HttpGet]
        public HttpResponseMessage DownloadMedias2(int pai, long startPos, long endPos)
        {
            // zip包文件
            string zipFile = ConfigurationManager.AppSettings["UploadCardImage"] + pai + ".zip";

            if (!File.Exists(zipFile))
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }

            FileStream fileStream = new FileStream(zipFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

            if (fileStream.Length < endPos)
            {
                fileStream.Close();
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }

            fileStream.Seek((int)startPos, SeekOrigin.Begin);
            byte[] sendBuffers = new byte[endPos - startPos];
            fileStream.Read(sendBuffers, 0, sendBuffers.Length);
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(sendBuffers);
            fileStream.Close();
            //response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-zip-compressed");
            //response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            //{
            //    FileName = pai + ".zip"
            //};
            return response;
        }
        #endregion

        #region 上传文件
        [HttpGet]
        public NormalReturn<string> GetResumeStart(string fn, int pai)
        {
            NormalReturn<string> result = new NormalReturn<string>();
            result.Result = 1;
            try
            {
                //用于获取当前文件是否是续传。和续传的字节数开始点
                var filePath = ConfigurationManager.AppSettings["UploadPaintingCardImage"] + pai + "\\" + fn + ".zip";
                if (File.Exists(filePath))
                {
                    FileInfo fileinfo = new FileInfo(filePath);
                    result.data = fileinfo.Length.ToString();
                }
            }
            catch (Exception e)
            {
                result.Result = 0;
                result.Message = "服务器读取文件大小失败";
            }

            return result;
        }

        [HttpPost]
        public VoidReturn Upload(UploadParam param)
        {
            VoidReturn result = new VoidReturn();
            result.Result = 1;
            string fn = param.fn; // zip包文件名： 以DeviceId命名, 无需后缀
            string pai = param.pai; // 项目自增长Id
            string gid = param.gid; // groupid
            bool isend = param.isend; // 最后一次上传
            string data = param.data; // 文件的base64编码后的字符串
            string deviceid = fn; // 设备id
            WriteLog("gid = " + gid);
           
            byte[] bytes = Convert.FromBase64String(data);

            var pjRootPath = ConfigurationManager.AppSettings["UploadPaintingCardImage"] + pai;
            if (!Directory.Exists(pjRootPath))
            {
                Directory.CreateDirectory(pjRootPath);
            }

            WriteLog("data = " + data);

            var zipPath = pjRootPath + "\\" + fn + "_" + gid + ".zip"; // deviceid_groupid 避免同一个设备不同的group上传zip覆盖问题
            try
            {
                // 保存上传数据
                //var file = HttpContext.Current.Request.InputStream;
                SaveAs(zipPath, bytes, param.startPostion, param.endPosition);
            }
            catch (Exception e)
            {
                result.Result = 0;
                result.Message = "服务器保存文件失败";
                return result;
            }

            if (!isend) return result;

            bool unzipSuccess = false;
            try
            {
                // zip包解压
                string unzipPath = pjRootPath + "\\" + deviceid + "\\" + gid;
                if (!Directory.Exists(unzipPath))
                {
                    Directory.CreateDirectory(unzipPath);
                }
                unzipSuccess = ZipClass.GetInstance().UnZip(zipPath, unzipPath);
                //File.Delete(zipPath);
            }
            catch (Exception e)
            {
                result.Result = 0;
                result.Message = e.Message;
                return result;
            }

            if (!unzipSuccess)
            {
                result.Result = 0;
                result.Message = "服务器解压zip包失败";
                return result;
            }

            #region 不用的代码
            // 将图片路径写入数据库
            //if (unzipSuccess)
            //{
            //    string cardPath = pjRootPath + "\\" + fn;
            //    List<string> filenames = Directory.EnumerateFiles(cardPath).ToList();

            //    if (filenames != null && filenames.Count > 0)
            //    {
            //        filenames.ForEach(r =>
            //        {
            //            string name = Path.GetFileNameWithoutExtension(r);
            //            string[] attrs = name.Split('_');
            //            string deviceid1 = attrs[0];
            //            string groupid = attrs[1];
            //            string cardid = attrs[2];
            //            DeviceAnswer da = new DeviceAnswer();
            //            da.ProjectAutoId = int.Parse(pai);
            //            da.DeviceId = deviceid1;
            //            da.GroupId = int.Parse(groupid);
            //            da.CardId = int.Parse(cardid);
            //            da.CreateDate = DateTime.Now;
            //            da.QuestionType = Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.Painting;
            //            da.QuestionAnswer = pai + "/" + fn + "/" + Path.GetFileName(r);
            //            DeviceAnswerBLL.AddItem(da);
            //        });
            //    }
            //}
            #endregion

            return result;
        }

        /// <summary>
        /// 保存数据流到zip文件
        /// </summary>
        /// <param name="saveFilePath"></param>
        /// <param name="stream"></param>
        private void SaveAs(string saveFilePath, byte[] bytes, string start, string end) 
        {
            long startPosition = long.Parse(start);
            long endPosition = long.Parse(end);
            long lStartPos = 0;

            WriteLog("start = " + startPosition + "\n end = " + endPosition);

            FileStream fs;
            if (File.Exists(saveFilePath))
            {
                fs = File.OpenWrite(saveFilePath);
                lStartPos = fs.Length;
            }
            else
            {
                fs = new FileStream(saveFilePath, FileMode.Create);
                lStartPos = 0;
            }


            if (lStartPos > endPosition)
            {
                fs.Close();
                return;
            }
            else if (lStartPos < startPosition)
            {
                lStartPos = startPosition;
            }
            else if (lStartPos > startPosition && lStartPos < endPosition)
            {
                lStartPos = startPosition;
            }
            fs.Seek(lStartPos, SeekOrigin.Current);
            fs.Write(bytes, 0, bytes.Length);
            fs.Close();
        }

        #endregion 上传文件
        [HttpGet]
        public void WriteLog(string str)
        {
            StreamWriter sw = null;
            try
            {
                string logfilename = DateTime.Now.ToString("yyyy-MM-dd");
                string logfiledir = HttpContext.Current.Server.MapPath("~") + "log\\";
                if (!Directory.Exists(logfiledir))
                {
                    Directory.CreateDirectory(logfiledir);
                }
                string logfilepath = logfiledir + logfilename + ".txt";
                if (!File.Exists(logfilepath))
                {
                    FileStream stream = File.Create(logfilepath);
                    stream.Close();
                }
                string time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                sw = File.AppendText(logfilepath);
                sw.WriteLine(time + " : ==> " + str);
                sw.Flush();
            }
            finally
            {
                if (null != sw)
                {
                    sw.Close();
                }
            }
        }

        public class UploadParam {
            public string fn { get; set; } // zip包文件名： 以DeviceId命名, 无需后缀
            public string pai { get; set; } // 项目自增长Id
            public string gid { get; set; } // groupid
            public bool isend { get; set; } // 最后一次上传
            public string data { get; set; } // 文件的base64编码后的字符串
            public string startPostion { get; set; } // 起始位置
            public string endPosition { get; set; } // 结束位置
        }

        #region Test Code
        [HttpGet]
        public object Base64Encode()
        {
            string path = "C:\\Users\\Administrator\\Desktop\\pad001.zip";
            FileStream stream = File.Open(path, FileMode.Open);
            long bytes = stream.Length;
            byte[] filebytes = new byte[bytes];
            stream.Read(filebytes, 0, filebytes.Length);
            stream.Close();
            string data = Convert.ToBase64String(filebytes);
            return new { data = data, length = data.Length };
        }
        
        public long Base64Decode(Param data)
        {
            string path = "C:\\Users\\Administrator\\Desktop\\pad001_1.zip";

            byte[] bytes = Convert.FromBase64String(data.Data);
            FileStream fs = new FileStream(path, FileMode.Create);
            long s = data.Data.Length;
            fs.Write(bytes, 0, bytes.Length);
            fs.Close();
            return bytes.Length;
        }

        public bool UnZip()
        {
            string path = "C:\\Users\\Administrator\\Desktop\\pad001_1.zip";
            string path2 = "C:\\Users\\Administrator\\Desktop\\pad001_unzip\\";
            return ZipClass.GetInstance().UnZip(path,path2);
        }
    }

    public class Param {
        public string Data { get; set; }
    }
    #endregion
}
