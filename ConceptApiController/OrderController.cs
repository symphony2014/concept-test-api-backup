﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Concept.BLL.BaseBD;
using Concept.Entity.BaseDB;
using ConceptCommon.ReturnMode;

namespace ConceptApiController
{
    public class OrderController : BaseController
    {
        /// <summary>
        /// 获取排序题目设置
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        public NormalListReturn<OrderQuestion> GetOrderQuestion(int projectAutoId)
        {
            NormalListReturn<OrderQuestion> OrderQuestionList = new NormalListReturn<OrderQuestion>()
            {
                Result = 0,
                Message = "成功"
            };
            try
            { 
                OrderQuestionList.data = OrderQuestionBLL.GetOrderQuestionList(projectAutoId);
            }
            catch (Exception ex)
            {
                OrderQuestionList.Result = 2;
                OrderQuestionList.Message = "InternalError";
            }
            return OrderQuestionList;
        }
        /// <summary>
        /// 更新排序题目设置
        /// </summary>
        /// <param name="gradingSetting"></param>
        /// <returns></returns>
        [HttpPost]
        public VoidReturn UpdateOrderQuestion(List<OrderQuestion> orderQuestionList)
        {
            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                foreach (var item in orderQuestionList)
                {
                    item.UpdateDate = DateTime.Now;
                }
                OrderQuestionBLL.UpdateItemList(orderQuestionList, new string[] { "OrderQuestionText", "UpdateDate","OpenText" });
            }
            catch (Exception ex)
            {
                vr.Result = 2;
                vr.Message = "InternalError";
            }
            return vr;
        }

        /// <summary>
        /// 获取分堆题目设置
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        public NormalListReturn<ClassifyQuestion> GetClassifyQuestion(int projectAutoId)
        {
            NormalListReturn<ClassifyQuestion> ClassifyQuestionList = new NormalListReturn<ClassifyQuestion>()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                ClassifyQuestionList.data = ClassifyQuestionBLL.GetClassifyQuestionList(projectAutoId);
            }
            catch (Exception ex)
            {
                ClassifyQuestionList.Result = 2;
                ClassifyQuestionList.Message = "InternalError";
            }
            return ClassifyQuestionList;
        }
        /// <summary>
        /// 更新分堆题目设置
        /// </summary>
        /// <param name="gradingSetting"></param>
        /// <returns></returns>
        [HttpPost]
        public VoidReturn UpdateClassifyQuestion(List<ClassifyQuestion> classifyQuestionList)
        {
            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                foreach (var item in classifyQuestionList)
                {
                    item.UpdateDate = DateTime.Now;
                }
                ClassifyQuestionBLL.UpdateItemList(classifyQuestionList, new string[] { "ClassifyQuestionText", "UpdateDate","OpenText" });
            }
            catch (Exception ex)
            {
                vr.Result = 2;
                vr.Message = "InternalError";
            }
            return vr;
        }
    }
}
