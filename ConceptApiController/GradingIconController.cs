﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Concept.BLL.BaseBD;
using Concept.Entity.BaseDB;
using Concept.Entity.SysMode;
using ConceptCommon.ReturnMode;

namespace ConceptApiController
{
    public class GradingIconController : BaseController
    {
        /// <summary>
        /// 获取打分题设置
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        public NormalReturn<GradingSetting> GetGradingSetting(int projectAutoId)
        {
            NormalReturn<GradingSetting> GradingSetting = new NormalReturn<GradingSetting>()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                GradingSetting gradingSetting = new GradingSetting();
                gradingSetting.ProjectAutoId = projectAutoId;
                GradingEmoticonQuestion gradingEmotionQ = GradingEmoticonQuestionBLL.GetGradingEmoticonQuestion(projectAutoId);
                if(gradingEmotionQ!=null)
                { 
                    gradingSetting.IsShowGrading = Convert.ToBoolean(gradingEmotionQ.IsShowGrading);
                    gradingSetting.IsShowOpenQuestion = Convert.ToBoolean(gradingEmotionQ.IsShowOpenQuestion);
                    gradingSetting.OpenQuestion = gradingEmotionQ.OpenQuestion;
                }
                else
                { 
                    gradingSetting.IsShowGrading = false;
                    gradingSetting.IsShowOpenQuestion = false;
                    gradingSetting.OpenQuestion = "";
                }

                List<GradingQuestion> gradingQList = GradingQuestionBLL.GetGradingQuestionList(projectAutoId);
                gradingSetting.GradingQuestionList = gradingQList;
                GradingSetting.data = gradingSetting;
            }
            catch (Exception ex)
            {
                GradingSetting.Result = 2;
                GradingSetting.Message = "InternalError";
            }
            return GradingSetting;
        }
        /// <summary>
        /// 更新打分题设置
        /// </summary>
        /// <param name="gradingSetting"></param>
        /// <returns></returns>
        [HttpPost]
        public VoidReturn UpdateGradingSetting(GradingSetting gradingSetting)
        {
            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                GradingEmoticonQuestion gradingEmotionQ = GradingEmoticonQuestionBLL.GetGradingEmoticonQuestion(gradingSetting.ProjectAutoId);
                if (gradingEmotionQ==null)
                {
                    vr.Result = 1;
                    vr.Message = "项目不存在";
                    return vr;
                }
                gradingEmotionQ.IsShowOpenQuestion = Convert.ToInt32(gradingSetting.IsShowOpenQuestion) ;
                gradingEmotionQ.IsShowGrading = Convert.ToInt32(gradingSetting.IsShowGrading);
                gradingEmotionQ.OpenQuestion = gradingSetting.OpenQuestion==null?"":gradingSetting.OpenQuestion;
                gradingEmotionQ.UpdateDate = DateTime.Now;
                GradingEmoticonQuestionBLL.UpdateItem(gradingEmotionQ, new string[] { "IsShowOpenQuestion", "IsShowGrading", "OpenQuestion", "UpdateDate" });
                if (gradingSetting.GradingQuestionList != null && gradingSetting.GradingQuestionList.Count>0)
                {
                    foreach(var item in gradingSetting.GradingQuestionList)
                    {
                        item.UpdateDate = DateTime.Now;
                    }
                    GradingQuestionBLL.UpdateItemList(gradingSetting.GradingQuestionList, new string[] { "QuestionText", "UpdateDate" });
                }
                

            }
            catch (Exception ex)
            {
                vr.Result = 2;
                vr.Message = "InternalError";
            }
            return vr;
        }

        /// <summary>
        /// 获取表情包设置
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        public NormalReturn<EmoticonSetting> GetEmoticonSetting(int projectAutoId)
        {
            NormalReturn<EmoticonSetting> EmoticonSetting = new NormalReturn<EmoticonSetting>()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                EmoticonSetting emoticonSetting = new EmoticonSetting();
                emoticonSetting.ProjectAutoId = projectAutoId;
                GradingEmoticonQuestion gradingEmotionQ = GradingEmoticonQuestionBLL.GetGradingEmoticonQuestion(projectAutoId);
                if (gradingEmotionQ != null)
                {
                    emoticonSetting.IsShowEmoticon = Convert.ToBoolean(gradingEmotionQ.IsShowEmoticon);
                    emoticonSetting.EmoticonDesc = gradingEmotionQ.EmoticonDesc; 
                }
                else
                {
                    emoticonSetting.IsShowEmoticon = false;
                    emoticonSetting.EmoticonDesc = ""; 
                }

                List<EmoticonQuestionOption> emoticonQuestionOptionList = EmoticonQuestionOptionBLL.GetEmoticonQuestionOptionList(projectAutoId);
                var IconList = IconBLL.GetIconList();
                emoticonSetting.EmoticonQuestionOptionList = new List<SysEmoticonQuestionOption>();
                foreach(EmoticonQuestionOption emotionOption in emoticonQuestionOptionList)
                {
                    SysEmoticonQuestionOption seqo = new SysEmoticonQuestionOption();
                    EmoticonQuestionOptionFront newemotionOption = new EmoticonQuestionOptionFront();
                    newemotionOption.EmoticonOptionId = emotionOption.EmoticonOptionId;
                    newemotionOption.ProjectAutoId = emotionOption.ProjectAutoId;
                    newemotionOption.OrderId = emotionOption.OrderId;
                    newemotionOption.IsShow = Convert.ToBoolean(emotionOption.IsShow);
                    newemotionOption.Score = emotionOption.Score;
                    newemotionOption.IconId = emotionOption.IconId;
                    newemotionOption.IconDesc = emotionOption.IconDesc; 
                    seqo.EmoticonQuestionOptionItem = newemotionOption;
                    var icon = IconList.Where(i => i.IconId == emotionOption.IconId).FirstOrDefault();
                    if(icon!=null)
                    {
                        seqo.EmoticonUrl = ConfigurationManager.AppSettings["domain"] + icon.IconUrl;
                    }
                    emoticonSetting.EmoticonQuestionOptionList.Add(seqo);
                }
                EmoticonSetting.data = emoticonSetting;
            }
            catch (Exception ex)
            {
                EmoticonSetting.Result = 2;
                EmoticonSetting.Message = "InternalError";
            }
            return EmoticonSetting;
        }
        /// <summary>
        /// 更新表情包设置
        /// </summary>
        /// <param name="gradingSetting"></param>
        /// <returns></returns>
        [HttpPost]
        public VoidReturn UpdateEmoticonSetting(EmoticonSetting emoticonSetting)
        {
            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                GradingEmoticonQuestion gradingEmotionQ = GradingEmoticonQuestionBLL.GetGradingEmoticonQuestion(emoticonSetting.ProjectAutoId);
                if (gradingEmotionQ == null)
                {
                    vr.Result = 1;
                    vr.Message = "项目不存在";
                    return vr;
                }
                gradingEmotionQ.IsShowEmoticon = Convert.ToInt32(emoticonSetting.IsShowEmoticon);
                gradingEmotionQ.EmoticonDesc = emoticonSetting.EmoticonDesc==null? "" : emoticonSetting.EmoticonDesc; 
                gradingEmotionQ.UpdateDate = DateTime.Now;
                GradingEmoticonQuestionBLL.UpdateItem(gradingEmotionQ, new string[] { "IsShowEmoticon", "EmoticonDesc", "UpdateDate" });
                if (emoticonSetting.EmoticonQuestionOptionList != null && emoticonSetting.EmoticonQuestionOptionList.Count > 0)
                {
                    List<EmoticonQuestionOption> eqoList = new List<EmoticonQuestionOption>();
                    foreach (var item in emoticonSetting.EmoticonQuestionOptionList)
                    {
                        EmoticonQuestionOption emotionDBItem = new EmoticonQuestionOption();
                        emotionDBItem.EmoticonOptionId = item.EmoticonQuestionOptionItem.EmoticonOptionId;
                        emotionDBItem.ProjectAutoId = item.EmoticonQuestionOptionItem.ProjectAutoId;
                        emotionDBItem.OrderId = item.EmoticonQuestionOptionItem.OrderId;
                        emotionDBItem.IsShow = Convert.ToInt32(item.EmoticonQuestionOptionItem.IsShow);
                        emotionDBItem.Score = item.EmoticonQuestionOptionItem.Score;
                        emotionDBItem.IconId = item.EmoticonQuestionOptionItem.IconId;
                        emotionDBItem.IconDesc = item.EmoticonQuestionOptionItem.IconDesc;
                        emotionDBItem.UpdateDate = DateTime.Now;
                        eqoList.Add(emotionDBItem);
                    }
                    EmoticonQuestionOptionBLL.UpdateItemList(eqoList, new string[] { "IsShow", "Score", "IconDesc", "UpdateDate" });
                } 
            }
            catch (Exception ex)
            {
                vr.Result = 2;
                vr.Message = "InternalError";
            }
            return vr;
        }
    }
}
