﻿using Concept.BLL.BaseBD;
using Concept.Entity.BaseDB;
using Concept.Entity.SysMode;
using IpsosReport.ModelBiz.External;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ConceptApiController.SyncReport
{
    public class Sync
    {

        HttpClient client = new HttpClient();

        const string PK= "XV6sHMQ41HTS07d6nDDh";
        
        private int PjId { get; set; }
        public Sync(int pjId)
        {
            var reportAPI = ConfigurationManager.AppSettings["reportAPI"];
            client.BaseAddress = new Uri(reportAPI);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            this.PjId = pjId;
        }

        public async Task<ReturnData> SyncToReport()
        {
            try
            {
                var ts = DateTime.Now.Ticks.ToString();

                RequestTokenInfo token = new RequestTokenInfo
                {
                    p = "ipsos",
                    u = "ipsos",
                    ts = ts,
                };
                token.sign = MD5Base64Crypto(token.u, token.p, token.ts, PK);
                var result =  GetToken(token);
                result.Wait();
                MobileController mobile = new MobileController();
                var p = ProjectBLL.GetProjectByAutoId(this.PjId);
                var project = mobile.GetProjectInfo(p.ProjectId);

                var question = ConverToStruct(project.data);

                RequestStructureInfo structInfo = new RequestStructureInfo
                {
                    ExternalPjId = p.ProjectAutoId.ToString(),
                    questions = question,
                    sign = token.sign,
                    tk = result.Result.Data,
                    ts=ts
                };

                var result2 = await PostStruct(structInfo);



                RequestSampleDataInfo sample = new RequestSampleDataInfo { };
                var result3 = await PostSample(sample);

                return result3;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

      
        #region 通过MD5 64位 UTF8编码加密 private string MD5Base64Crypto
        /// <summary>
        /// 通过MD5 64位 UTF8编码加密
        /// </summary>
        /// <param name="u">用户名</param>
        /// <param name="p">密码</param>
        /// <param name="ts">时间戳</param>
        /// <param name="pk">private key</param>
        /// <returns></returns>
        private string MD5Base64Crypto(string u, string p, string ts, string pk)
        {
            string cryptoText = u + p + ts + pk;
            MD5 md5 = MD5.Create();
            byte[] s = md5.ComputeHash(Encoding.UTF8.GetBytes(cryptoText));
            return Convert.ToBase64String(s);
        }
        #endregion
        private List<StructureDataInfo> ConverToStruct (ProjectInfo project )
        {
          
            List<StructureDataInfo> structInfo = new List<StructureDataInfo>();

            //Card 上评分，开放，
            project.Cards.ForEach(card =>
            {
                CardConvert(project, card, structInfo);
            });

            //分堆题
            project.Questions.ClassifyQuestions.ForEach(cla =>
            {
               StructureDataInfo classicStructure = new StructureDataInfo();
               var classicVariable = new IpsosReport.ModelBiz.External.Variable(); 
               classicVariable.Code =  "classify"+ "_" + cla.QuestionId;
               classicVariable.Text = cla.QuestionText;
              classicVariable.QuesTypeId = "Multi";
              classicVariable.VarType = "Choice";
              classicVariable.ValueType = "Code";
               classicVariable.IsUsed = true;

                var classifyVarValues = new List<VariableValue>();
                for (int i = 0; i < project.Cards.Count; i++)
                {
                var classifyVarValue = new VariableValue();
                classifyVarValue.Code = i.ToString();
                classifyVarValue.Text = i.ToString();
                classifyVarValue.IsUsed = true;
                classifyVarValues.Add(classifyVarValue);
                }

              classicStructure.variable = classicVariable;
              classicStructure.values = classifyVarValues;
              structInfo.Add(classicStructure);
            });


            //排序题
   
            project.Questions.ClassifyQuestions.ForEach(cla =>
            {
            StructureDataInfo orderStructure = new StructureDataInfo();
                var orderVariable = new IpsosReport.ModelBiz.External.Variable
                {
                    Code = "order" + "_" + PjId,
                    Text = "排序",
                    QuesTypeId = "Multi",
                    VarType = "Choice",
                    ValueType = "Code",
                    IsUsed = true
                };
                var classifyVarValues = new List<VariableValue>();
                for (int i = 0; i < project.Cards.Count; i++)
                {
                    var classifyVarValue = new VariableValue();
                    classifyVarValue.Code = i.ToString();
                    classifyVarValue.Text = i.ToString();
                    classifyVarValue.IsUsed = true;
                    classifyVarValues.Add(classifyVarValue);
                }
                orderStructure.variable = orderVariable;
                orderStructure.values = classifyVarValues;
                structInfo.Add(orderStructure);
            });



            //必须题
            StructureDataInfo inReport1 = new StructureDataInfo();
            inReport1.variable = new IpsosReport.ModelBiz.External.Variable();
            inReport1.variable.Code = "Sys_Resp_Id";
            inReport1.variable.Text = "";
            structInfo.Add(inReport1);
            StructureDataInfo inReport2 = new StructureDataInfo();
            inReport2.variable = new IpsosReport.ModelBiz.External.Variable();
            inReport2.variable.Code = "Sys_Upload_Time";
            inReport2.variable.Text = "";

            structInfo.Add(inReport2);

            StructureDataInfo inReport3 = new StructureDataInfo();
            inReport3.variable = new IpsosReport.ModelBiz.External.Variable();
            inReport3.variable.Code = "Sys_RespStatus";
            inReport3.variable.Text = "Complete";
            structInfo.Add(inReport3);

            return structInfo;

        }

        private void CardConvert(ProjectInfo project, CardInfo card, List<StructureDataInfo> structInfo)
        {
            var cardPrefix = "c" + card.CardId;
            var index = 0;

            //评分题
            project.Questions.GradingQuestions.ForEach(grad =>
            {
                StructureDataInfo structureData = new StructureDataInfo();
                var variable = new IpsosReport.ModelBiz.External.Variable();
                variable.Code = cardPrefix + "_" + grad.QuestionId + "_" + index++;
                variable.Text = grad.QuestionText;
                variable.QuesTypeId = "Single";
                variable.VarType = "Choice";
                variable.ValueType = "Code";
                variable.IsUsed = true;
                structureData.variable = variable;

                var varlistG = new List<VariableValue>();
                int count = 5;//分制

                for (int i = 1; i <=count; i++)
                {
                    var item = new VariableValue();
                    item.Code =i.ToString();
                    item.Text = i.ToString();
                    item.IsUsed = true;
                    varlistG.Add(item);
                }
                structureData.values = varlistG;
                structInfo.Add(structureData);
            });


            //表情包

            StructureDataInfo emoData = new StructureDataInfo();
            var emovariable = new IpsosReport.ModelBiz.External.Variable();
            emovariable.Code = cardPrefix + "_" + "emo" + "_"+project.ProjectAutoId;
            emovariable.Text = project.Questions.EmoticonQuestion.QuestionText;
            emovariable.QuesTypeId = "Multi";
            emovariable.VarType = "Choice";
            emovariable.ValueType = "Code";
            emovariable.IsUsed = true;
            emoData.variable = emovariable;
            var varlist = new List<VariableValue>();
            project.Questions.EmoticonQuestion.Options.ForEach(grad =>
            {
                    var item = new VariableValue();
                    item.Code = grad.OptionText;
                    item.Text = grad.Score.ToString();
                    item.IsUsed = true;
                    varlist.Add(item);
            });
            emoData.values = varlist;
            structInfo.Add(emoData);

            //开放题

            StructureDataInfo openStr = new StructureDataInfo();
            openStr.variable = new IpsosReport.ModelBiz.External.Variable();
            openStr.variable.Code = "open_"+project.ProjectId;
            openStr.variable.Text = project.Questions.OpenQuestion.QuestionText;
            openStr.variable.IsUsed = true;
            openStr.variable.QuesTypeId = "Opentext";
            openStr.variable.ValueType = "Char";
            openStr.variable.VarType = "Text";

            structInfo.Add(openStr);
        }

        private  async Task<ReturnData> GetToken(RequestTokenInfo token)
        {
            try
            {

            ReturnData data = null;
            HttpResponseMessage response = await client.PostAsJsonAsync(
                "api/external/Token", token);
            if (response.IsSuccessStatusCode)
            {
             data = await response.Content.ReadAsAsync<ReturnData>();
            } 
            // return URI of the created resource.
            return data;
            }
            catch ( Exception e)
            {
                throw e;
            }
        }

        private async Task<ReturnDataStruct> PostStruct(RequestStructureInfo param)
        {
            ReturnDataStruct data = null;
            HttpResponseMessage response = await client.PostAsJsonAsync(
                "api/external/Structure", param);
            if (response.IsSuccessStatusCode)
            {
                data = await response.Content.ReadAsAsync<ReturnDataStruct>();
            }

            // return URI of the created resource.
            return data;
        }

        private async Task<ReturnData> PostSample(RequestSampleDataInfo param)
        {
            ReturnData data = null;
            HttpResponseMessage response = await client.PostAsJsonAsync(
                "api/external/SampleData", param);
            if (response.IsSuccessStatusCode)
            {

                data = await response.Content.ReadAsAsync<ReturnData>();
            }

            // return URI of the created resource.
            return data;
        }
    }
}
