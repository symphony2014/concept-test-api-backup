﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IpsosReport.ModelBiz.External
{
    public class ReturnData
    {
        public int Status { get; set; }
        public string Error { get; set; }
        public string Data { get; set; }
    }

    public class ReturnDataStruct
    {
        public int Status { get; set; }
        public string Error { get; set; }
        public List<DataError> Datas { get; set; }
    }
    public class DataError
    {
        public string ExternalVarId { get; set; }
        public string VarCode { get; set; }
        public string Value { get; set; }
        public string Erro { get; set; }
    }
    public class NormalReturn<T> : ReturnData where T : class
    {
        public T Data { get; set; }
    }

    public class ValueTypeNormalReturn<T> : ReturnData where T : struct
    {
        public T Data { get; set; }
    }

    public class NormalListReturn<T> : ReturnData where T : class, new()
    {
        public List<T> Data { get; set; }
        public int TotalCount { get; set; }
    }

    public class NormalListReturnType<T> : ReturnData where T : class, new()
    {
        public List<T> Data { get; set; }
        public int nState { get; set; }
        public string ErrMessage { get; set; }
    }


    public class NormalReturnType<T> : ReturnData where T : class, new()
    {
        public T Data { get; set; }
        public int nState { get; set; }
        public string ErrMessage { get; set; }
    }
}
