﻿

using Concept.Entity.BaseDB;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IpsosReport.ModelBiz.External
{


    public class RequestInfo
    {
        public string tk { get; set; }
        public string sign { get; set; }
        public string ts { get; set; }
    }

    public class RequestTokenInfo : RequestInfo
    {
        public string u { get; set; }
        public string p { get; set; }
    }

    public class RequestStructureInfo: RequestInfo
    {
       
        public string ExternalPjId { get; set; }
        public List<StructureDataInfo> questions { get; set ;}
    }

    public class RequestSampleDataInfo : RequestInfo
    {
      
        public long pjid { get; set; }
        public List<List<Dictionary<string, string>>> datas { get; set; }
    }

    public class StructureDataInfo
    {
        public Variable variable { get; set; }
        public List<VariableValue> values { get; set; }
    }

    public class Variable
    {
        public string ExternalVarId { get; set; }
        public string Code { get; set; }
        public string VarType { get; set; }
        public string ValueType { get; set; }
        public Boolean IsUsed { get; set; }
        public string Text { get; set; }
        public string QuesTypeId { get; set; }
    }
    public class VariableValue
    {
        public string Code { get; set; }
        public string Text { get; set; }
        public Boolean IsUsed { get; set; }
    }
}
