﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Concept.Entity.BaseDB;
using System.Web;
using System.Configuration;

namespace ConceptApiController
{
    public class BaseController : ApiController
    {
        public readonly string PGToken = ConfigurationManager.AppSettings["PG-Token"];
        public readonly bool PGTokenCheck = Convert.ToBoolean(ConfigurationManager.AppSettings["PG-TokenCheck"]);
        public readonly string TopicPath = ConfigurationManager.AppSettings["TopicPath"];
        public BaseController()
        {

        }
        public Account UserIdentity
        {
            get
            {
                var identity = User.Identity as ClaimsIdentity;
                Account user = new Account() { };
                user.LoginName = identity.Claims.FirstOrDefault(c => c.Type == "UserName").Value;
                user.AccountId = int.Parse(identity.Claims.FirstOrDefault(c => c.Type == "UserPassportId").Value);
                //AccountRoleType result;
                //if (Enum.TryParse<AccountRoleType>(identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value, out result))
                //    user.RoleType = result;

                return user;
            }
        }

        public bool CheckPGAuth()
        {
            bool result = false;
            if (PGTokenCheck && (HttpContext.Current.Request.Headers["X-Token"] == null || HttpContext.Current.Request.Headers["X-Token"] != PGToken))
            {
                HttpContext.Current.Response.End();
            }
            else
            {
                result = true;
            }
            return result;
        }
    }
}
