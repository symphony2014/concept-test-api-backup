﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Concept.BLL.BaseBD;
using Concept.Entity.BaseDB;
using ConceptCommon.ReturnMode;
using ConceptApiController.BLL;
using ConceptCommon.ParaMode;
using System.Web;
using Concept.Entity.SysMode;


using System.Configuration;
using ConceptCommon;
using Newtonsoft.Json.Linq;
using Concept.BLL.BaseBD.Log;
using Concept.Entity.BaseDB.Log;
using Concept.Entity.External;
using Concept.Entity.EnumMode;

namespace ConceptApiController
{
    public class PGRuleController : BaseController
    { 
        /// <summary>
        /// 拉每个groupid下的分组结果
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpPost]
        public VoidReturn PullRuleResult(PGParaMode ruleParaInfo)
        {
            VoidReturn pgReturn = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            if (!CheckPGAuth()) 
            { 
                pgReturn.Result = -1;
                pgReturn.Message = "请求受限";
                return pgReturn;
            }
            
            try
            {
                pgReturn = PGRuleLogical.Instance.PullRuleResult(ruleParaInfo);
            }
            catch(Exception ex)
            {
                pgReturn.Result = -1;
                pgReturn.Message = "异常错误";
            }
            return pgReturn;
        }

        /// <summary>
        /// 设置具体规则时跳转地址
        /// </summary>
        /// <param name="externalPjId"></param>
        /// <param name="groupId"></param>
        /// <param name="ruleId"></param>
        /// <param name="type"></param>
        /// <param name="SrvId"></param>
        [HttpGet]
        public void GotoSetRule(string externalPjId,string groupId,string ruleId)
        {
            var ruleItem = PGRuleInfoBLL.GetItem(groupId, externalPjId, ruleId);
            if(ruleItem!=null)
            {
                switch (ruleItem.RuleType)
                {
                    case 1:
                        HttpContext.Current.Response.Redirect("http://Conceptsetting?externalPjId=" + externalPjId + "&groupId=" + groupId + "&ruleId=" + groupId + "&SrvId=" + ruleItem.SrvId);
                        HttpContext.Current.Response.End();
                        break;
                    case 2:
                        HttpContext.Current.Response.Write("");
                        HttpContext.Current.Response.End();
                        break;
                    case 3:
                        HttpContext.Current.Response.Write("");
                        HttpContext.Current.Response.End();
                        break;
                    default: break;
                }
            }
            else
            {
                HttpContext.Current.Response.Write("信息错误");
                HttpContext.Current.Response.End();
            }

        }

        /// <summary>
        /// 添加规则
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public VoidReturn AddRule(PGRuleInfoSys rule)
        {
            VoidReturn pgReturn = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            if (!CheckPGAuth())
            {
                pgReturn.Result = -1;
                pgReturn.Message = "请求受限";
                return pgReturn;
            }
            try
            {
                rule.CreateDate = DateTime.Now;
                rule.IsUse = 1;
                PGRuleInfoBLL.AddItem(rule);
                List<PGQualiRuleTopic> topicList = new List<PGQualiRuleTopic>();
                foreach(var item in rule.TopicIdList)
                {
                    PGQualiRuleTopic insertItem = new PGQualiRuleTopic();
                    insertItem.RuleId = rule.RuleId;
                    insertItem.TopicId = item;
                    topicList.Add(insertItem);
                }
                PGQualiRuleTopicBLL.AddItems(topicList);
            }
            catch(Exception ex)
            {
                pgReturn.Result = -1;
                pgReturn.Message = "异常错误";
            }
            return pgReturn;
        }
        /// <summary>
        /// 逻辑删除
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public VoidReturn LogicalDelRule(PGRuleInfo rule)
        {
            VoidReturn pgReturn = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            if (!CheckPGAuth())
            {
                pgReturn.Result = -1;
                pgReturn.Message = "请求受限";
                return pgReturn;
            }
            try
            {
                var ruleItem = PGRuleInfoBLL.GetItemByRuleId(rule.RuleId);
                if(ruleItem!=null)
                {
                    ruleItem.IsUse = 0;
                    PGRuleInfoBLL.UpdateItem(ruleItem, new string[] { "IsUse" });
                }
                else
                {
                    pgReturn.Result = -1;
                    pgReturn.Message = "rule不存在";
                    return pgReturn;
                }
            }
            catch (Exception ex)
            {
                pgReturn.Result = -1;
                pgReturn.Message = "异常错误";
            }
            return pgReturn;
        }

        /// <summary>
        /// 按照group的话题进行规则分析(首先要拉聊天记录,也需要放入任务列表执行),分析当前topic下的规则（定性，定量）扔到待分析列表，分析当前topic下的报告（定量，定性）扔到分析列表
        /// 如果是结束所有话题，需要分析group下的所有定性数据扔到待分析列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public VoidReturn AnalysisByTopic(PGAnalysisParaMode analysisParaInfo)
        {
            VoidReturn pgReturn = new VoidReturn()
            {
                Result = 0,
                Message = "任务已经创建"
            };
            if (!CheckPGAuth())
            {
                pgReturn.Result = -1;
                pgReturn.Message = "请求受限";
                return pgReturn;
            }

            try
            {
                //record paramMode info into

                var paramModeJson=JObject.FromObject(analysisParaInfo);
                Concept_OpMessageBLL.AddOpMessage(new Concept_OpMessage { AddTime = DateTime.Now, Msg = paramModeJson.ToString() });


                //获取topic信息


                    TaskParamLogBLL.AddItem(new TaskParamLog
                    {
                        prjId = analysisParaInfo.ExternalPjId,
                        sampGrpId = analysisParaInfo.GroupId,
                        topicId = analysisParaInfo.EndTopicId,
                        status = 0,
                        AddDate = DateTime.Now,
                        paramType = Convert.ToInt32(ParamType.getMessages)
                    });
                 if(analysisParaInfo.CloseAllTopic)
                {
                    TaskParamLogBLL.AddItem(new TaskParamLog
                    {
                        prjId = analysisParaInfo.ExternalPjId,
                        sampGrpId = analysisParaInfo.GroupId,
                        topicId = analysisParaInfo.EndTopicId,
                        status = 0,
                        AddDate = DateTime.Now,
                        paramType = Convert.ToInt32(ParamType.getMessages)
                    });
                    TaskParamLogBLL.AddItem(new TaskParamLog
                    {
                        prjId = analysisParaInfo.ExternalPjId,
                        sampGrpId = analysisParaInfo.GroupId,
                        topicId = "-1",
                        status = 0,
                        AddDate = DateTime.Now,
                        paramType = Convert.ToInt32(ParamType.getMessages)
                    });
                }
                //string date = MessageBLL.GetLastUpdate(analysisParaInfo.ExternalPjId, analysisParaInfo.GroupId);
                //ExternalTopicParam param = new ExternalTopicParam();
                //param.parms = new param
                //{
                //    sttm = date,
                //    prjId = analysisParaInfo.ExternalPjId,
                //    sampGrpId = analysisParaInfo.GroupId,
                //};
                //Task<ExternalMessage> topicInfo = Import.GetTopicInfo(param);


                //topicInfo.Wait();


                //if (topicInfo.Result.data.rows.Count > 0)
                //{
                //    foreach (var item in topicInfo.Result.data.rows)
                //    {
                //        MessageBLL.AddItem(item);
                //    }
                //}


                //生成Excel
               // Import.GenreateExcel(TopicPath, analysisParaInfo.GroupId, analysisParaInfo.ExternalPjId, analysisParaInfo.EndTopicId);

               

                ////生成百度分词
                //ExternalJsonBLL.RemoveBy(analysisParaInfo.ExternalPjId,analysisParaInfo.GroupId,analysisParaInfo.EndTopicId);


                //var messages = MessageBLL.GetMessages(analysisParaInfo.ExternalPjId, analysisParaInfo.GroupId, analysisParaInfo.EndTopicId);
               
                //var result = BaiduAI.GetcommentTags(messages);
                //ExternalJsonBLL.AddItem(new ExternalJson {
                //    JSON = JObject.FromObject(result).ToString(),
                //    ExternalPjId = analysisParaInfo.ExternalPjId,
                //    GroupId = analysisParaInfo.GroupId,
                //    TopicId = analysisParaInfo.EndTopicId,
                //    UpdateTime=DateTime.Now
                //});

                    //计算全局词云图以项目为单位
                    //var allMessages = MessageBLL.GetAllMessages(analysisParaInfo.ExternalPjId);
                    //var allresult = BaiduAI.GetcommentTags(messages);
                    //ExternalJsonBLL.AddItem(new ExternalJson
                    //{
                    //    JSON = JObject.FromObject(allresult).ToString(),
                    //    ExternalPjId = analysisParaInfo.ExternalPjId,
                    //    GroupId ="-1",
                    //    TopicId = "-1",
                    //    UpdateTime = DateTime.Now
                    //});
               
            }
            catch (Exception ex)
            {
                throw ex;
                pgReturn.Result = -1;
                pgReturn.Message = "异常错误";
            }
            return pgReturn;
        }

        /// <summary>
        /// 通知重新计算某个规则
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public  VoidReturn  AnalysisByRule(PGRuleInfo rule)
        {
            VoidReturn pgReturn = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            if (!CheckPGAuth())
            {
                pgReturn.Result = -1;
                pgReturn.Message = "请求受限";
                return pgReturn;
            }

            try
            {
               

                //pgReturn = PGRuleLogical.Instance.PullRuleResult(ruleParaInfo);
            }
            catch (Exception ex)
            {
                pgReturn.Result = -1;
                pgReturn.Message = "异常错误";
            }

            return pgReturn;
        }
    }
}
