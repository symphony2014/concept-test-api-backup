﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Concept.BLL.BaseBD;
using Concept.Entity.BaseDB;
using ConceptCommon.ReturnMode; 

namespace ConceptApiController
{
    public class ProjectController : BaseController
    {
    
        /// <summary>
        /// 添加更新项目信息
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        [HttpPost]
        public VoidReturn AddOrUpdateProject(Project project)
        {
            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            try
            { 
                if (project.ProjectAutoId == 0)
                {

                    var projectExist = ProjectBLL.GetProjectById(project.ProjectId);
                    if (projectExist != null)
                    {
                        vr.Result = 3;
                        vr.Message = "projectId 已经存在！";
                        return vr;
                    }
                    project.OwnerId = UserIdentity.AccountId;
                    project.CreateDate = DateTime.Now;
                    if(project.ProjectName==null || project.ProjectName==string.Empty || project.ProjectId==null || project.ProjectId==string.Empty)
                    {
                        vr.Result = 1;
                        vr.Message = "项目名称或项目id为空";
                        return vr;
                    }
                    int projectAutoId = ProjectBLL.AddItem(project);
                    //添加项目需要的东西 to be continue
                    List<GradingQuestion> GradingQuestionList = new List<GradingQuestion>();
                    List<ClassifyQuestion> ClassifyQuestionList = new List<ClassifyQuestion>();
                    List<OrderQuestion> OrderQuestionList = new List<OrderQuestion>();
                    List<EmoticonQuestionOption> EmoticonQuestionOptionList = new List<EmoticonQuestionOption>();
                    GradingEmoticonQuestion geq = new GradingEmoticonQuestion();
                    geq.IsShowEmoticon = geq.IsShowGrading = geq.IsShowOpenQuestion = 0;
                    geq.OpenQuestion = "";
                    geq.ProjectAutoId = projectAutoId;
                    geq.UpdateDate = geq.CreateDate = DateTime.Now;
                    GradingEmoticonQuestionBLL.AddItem(geq);
                    var IconList = IconBLL.GetIconList();
                    for(int i =0;i<5;i++)
                    {
                        GradingQuestion gq = new GradingQuestion();
                        gq.ProjectAutoId = projectAutoId;
                        gq.QuestionText = "";
                        gq.OrderId = i + 1;
                        gq.CreateDate = gq.UpdateDate = DateTime.Now;
                        GradingQuestionList.Add(gq);

                        ClassifyQuestion cq = new ClassifyQuestion();
                        cq.ProjectAutoId = projectAutoId;
                        cq.ClassifyQuestionText = "";
                        cq.OrderId = i + 1;
                        cq.CreateDate = cq.UpdateDate = DateTime.Now;
                        ClassifyQuestionList.Add(cq);

                        OrderQuestion oq = new OrderQuestion();
                        oq.ProjectAutoId = projectAutoId;
                        oq.OrderQuestionText = "";
                        oq.OrderId = i + 1;
                        oq.CreateDate = oq.UpdateDate = DateTime.Now;
                        OrderQuestionList.Add(oq);

                        EmoticonQuestionOption eqo = new EmoticonQuestionOption();
                        eqo.CreateDate = eqo.UpdateDate = DateTime.Now;
                        eqo.IconDesc = "";
                        eqo.IsShow = 0;
                        eqo.ProjectAutoId = projectAutoId;
                        eqo.Score = 0;
                        eqo.IconId = IconList[i].IconId;
                        EmoticonQuestionOptionList.Add(eqo);
                    }
                    GradingQuestionBLL.addItemList(GradingQuestionList);
                    ClassifyQuestionBLL.addItemList(ClassifyQuestionList);
                    OrderQuestionBLL.addItemList(OrderQuestionList);
                    EmoticonQuestionOptionBLL.addItemList(EmoticonQuestionOptionList);
                }
                else
                {
                    ProjectBLL.UpdateItem(project, new string[] { "ProjectName", "ProjectDesc", "ProjectId", "IsNeedDoodled", "ModuleOrder" });
                }

            }
            catch (Exception ex)
            {
                vr.Result = 2;
                vr.Message = "InternalError";
            }
            return vr;
        }
       public NormalReturn<Dictionary<int,string>> GetModuleOrder(string prjId) {
            
            Dictionary<int, string> filter = new Dictionary<int, string>();
            NormalReturn<Dictionary<int,string>> returnValue = new NormalReturn<Dictionary<int,string>>() { Message = "success" };
          var project=  ProjectBLL.GetProjectById(prjId);
            var data = project.ModuleOrder;

           
            if(data.IndexOf("1") >-1 || data.IndexOf("2") > -1)
            {
               var cards= ConceptCardBLL.GetConceptCardList(project.ProjectAutoId);
                foreach (var item in cards)
                {
                    filter.Add(item.CardId,item.ConceptCardName);
                }
            }

            if (data.IndexOf("3") > -1)
            {
                filter.Add(3,"排序报告");
            }
            if (data.IndexOf("4") > -1)
            {
                filter.Add(4, "分堆报告");
            }
            returnValue.data = filter;
            return returnValue;
        }
        /// <summary>
        /// 分页获取当前用户的项目列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public NormalListReturn<Project> GetProjectList(int pageIndex, int pageSize)
        {
            NormalListReturn<Project> projectList = new NormalListReturn<Project>()
            {
                Result = 0,
                Message = "成功"
            };
            int totalCount = 0;
            try
            {
                projectList.data = ProjectBLL.GetProjectList(pageIndex, pageSize, UserIdentity.AccountId, out totalCount);
                projectList.TotalCount = totalCount;
            }
            catch (Exception ex)
            {
                projectList.Result = 2;
                projectList.Message = "InternalError";
            }
            return projectList;
        }
        /// <summary>
        /// 删除项目
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        [HttpGet]
        public VoidReturn DelProject(int projectAutoId)
        {
            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                bool delresult = ProjectBLL.DelItemByPjId(projectAutoId);
                if(!delresult)
                {
                    vr.Result = 1;
                    vr.Message = "项目不存在";
                }

            }
            catch (Exception ex)
            {
                vr.Result = 2;
                vr.Message = "InternalError";
            }
            return vr;
        }
    }
}
