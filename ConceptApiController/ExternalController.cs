﻿using Concept.BLL.BaseBD;
using Concept.BLL.BaseBD.Log;
using Concept.Entity.BaseDB;
using Concept.Entity.BaseDB.Log;
using Concept.Entity.External;
using ConceptCommon;
using ConceptCommon.ReturnMode;
using ExternalAPI;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ConceptApiController
{
    public class ExternalController : BaseController
    {
        [HttpPost]
        public async Task<HttpResponseMessage> UploadExcel(string externalPjId, bool isRadar)
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/Resource");
            var provider = new MultipartFormDataStreamProvider(root);

            try
            {
                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);
                // This illustrates how to get the file names.
                string newFileName = string.Empty;
                string link = string.Empty;
                foreach (MultipartFileData file in provider.FileData)
                {
                    newFileName = file.LocalFileName + Path.GetExtension(file.Headers.ContentDisposition.FileName.Replace("\"", ""));
                    File.Move(file.LocalFileName, newFileName);
                    link = "Resource/" + Path.GetFileName(newFileName);
                }
                if (isRadar)
                {
                    Import.ImportRadar(newFileName);
                }
                Import.ImportVariables(newFileName, externalPjId);

                return Request.CreateResponse(HttpStatusCode.OK, 1);
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.ToString());
            }
        }
        public async Task<HttpResponseMessage> GetExternalProjects()
        {
            try
            {
                var projects = await Import.GetExternalProjects();
                return Request.CreateResponse(HttpStatusCode.OK, projects);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.ToString());
            }
        }
        public NormalReturn<ExternalMessage<eGroup>> GetGroups(string externalPjId)
        {
            NormalReturn<ExternalMessage<eGroup>> vr = new NormalReturn<ExternalMessage<eGroup>>()
            {
                Result = 0,
                Message = "成功"
            };
            vr.data=Import.GetGroupInfo(new ExternalParam<ExternalGroupParam> { parms=new ExternalGroupParam { prjId=externalPjId} }); 
            return vr;
        }
        public NormalReturn<ExternalMessage<eTopic>> GetTopics(string externalPjId)
        {
            NormalReturn<ExternalMessage<eTopic>> vr = new NormalReturn<ExternalMessage<eTopic>>()
            {
                Result = 0,
                Message = "成功"
            };
            vr.data = Import.GeteTopicInfo(new ExternalParam<ExternaleTopicParam> { parms = new ExternaleTopicParam { prjId = externalPjId,level="1" } });
            return vr;
        }
        public NormalReturn<ExternalMessage<eConceptProject>>   GetConceptProject(string choosedConceptPrjId)
        {
            NormalReturn<ExternalMessage<eConceptProject>> vr = new NormalReturn<ExternalMessage<eConceptProject>>()
            {
                Result = 0,
                Message = "成功"
            };
            vr.data = Import.GetConceptProjects(new ExternalParam<ExternaleTopicParam> { parms = new ExternaleTopicParam { prjId = choosedConceptPrjId} });

            return vr;
        }
        public NormalReturn<List<UserMap>> GetUsers(string externalPjId, string groupId="")
        {
            NormalReturn<List<UserMap>> vr = new NormalReturn<List<UserMap>>( );
            vr.Result = 0;
            vr.Message = "成功";

            if (!string.IsNullOrEmpty(groupId) && groupId!="-1")
            {
                vr.data = VariableAnswerBLL.GetUserDetailByGroupId(externalPjId,groupId);
            }
            else
            {
                vr.data = VariableAnswerBLL.GetUserDetailByPjId(externalPjId);
            }


            return vr;
        }

       //public NormalReturn<string> GetAIStatus(string externalPjId,string groupId,string topicId)
       // {
       //     NormalReturn<string> vr = new NormalReturn<string>();
       //    // TaskParamLogBLL.get

       // }

        /// <summary>
        /// 更新用户信息
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        public VoidReturn UpdateUserInfo(ExternalUserInfo userInfo)
        {
            Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { ModuleName = "ConceptController:UpdateUserInfo", MessDescript = JObject.FromObject(userInfo).ToString(), AddTime = DateTime.Now });
            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                List<VariableAnswer> answers = VariableAnswerBLL.GetUserData(userInfo.openId, userInfo.phone);
                foreach (var item in answers)
                {
                    if (!string.IsNullOrWhiteSpace(userInfo.openId))
                    {
                        item.OpenId = userInfo.openId;
                        VariableAnswerBLL.UpdateItem(item);
                    }
                    Variable variable = VariableBLL.GetById(item.varId);

                    if (variable != null)
                    {
                        var isOther = true;

                        foreach (var map in ExternalUserInfo.convertMapping)
                        {
                            if (map[1] == variable.ColumnName)
                            {
                                PropertyInfo prop = userInfo.GetType().GetProperty(map[0], BindingFlags.Public | BindingFlags.Instance);
                                var propValue = prop.GetValue(userInfo);
                                if (propValue != null)
                                {
                                    item.Value = prop.GetValue(userInfo).ToString();
                                    VariableAnswerBLL.UpdateItem(item);
                                    isOther = false;
                                }
                            }
                        }

                        if (isOther)
                        {
                            if (userInfo.jsonData != null)
                            {
                                var other = userInfo.jsonData.FirstOrDefault(o => o.key == variable.ColumnName);
                                if (other != null)
                                {
                                    item.Value = other.value;
                                    VariableAnswerBLL.UpdateItem(item);
                                }
                            }
                        }
                    }
                }

                return vr;
            }
            catch (System.Exception e)
            {
                Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { ModuleName = "ConceptController:UpdateUserInfo", ExceptMessage = e.ToString() });
                vr.Message = e.ToString();
                return vr;
            }
        }
    }



}
