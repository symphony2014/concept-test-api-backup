﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using Concept.BLL.BaseBD;
using Concept.BLL.BaseBD.Log;
using Concept.Entity.BaseDB;
using Concept.Entity.BaseDB.Log;
using Concept.Entity.EnumMode;
using Concept.Entity.SysMode;
using ConceptCommon;

namespace ConceptApiController.BLL
{
    public class ReportLogical
    {
        #region 单例定义p
        private static object lockObject = new object();
        protected ReportLogical()
        {
        }

        private static volatile ReportLogical _instance;
        public static ReportLogical Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new ReportLogical());
                    }
                }
                return _instance;
            }
        }

        #endregion 单例定义 

        public List<List<string>> GetGradingEmotionData(int projectAutoId)
        {
            List<List<string>> resultData = new List<List<string>>();
            List<string> header1 = new List<string>();
            header1.Add("");
            header1.Add("");
            List<string> header2 = new List<string>();
            header2.Add("组名");
            header2.Add("设备ID");

            List<Group> groupList = GroupBLL.GetGroupList(projectAutoId);
            List<ConceptCard> conceptCardList = ConceptCardBLL.GetConceptCardList(projectAutoId);
            List<GradingQuestion> gradingQList = GradingQuestionBLL.GetGradingQuestionList(projectAutoId).Where(gq => gq.QuestionText != "").ToList();
            GradingEmoticonQuestion geq = GradingEmoticonQuestionBLL.GetGradingEmoticonQuestion(projectAutoId);
            List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();


            if (conceptCardList != null && conceptCardList.Count > 0)
            {
                foreach (var card in conceptCardList)
                {
                    if (gradingQList.Count > 0)
                    {
                        for (int j = 1; j <= gradingQList.Count; j++)
                        {
                            if (j == 1)
                            {
                                header1.Add("概念卡" + card.CardOrder + "("+card.ConceptCardName+")");
                            }
                            else
                            {
                                header1.Add("");
                            }
                            header2.Add("维度" + j + "打分");
                        }
                    }
                    if (geq.IsShowOpenQuestion == 1)
                    {
                        header1.Add("");
                        header2.Add("开放题答案");
                    }
                    if (geq.IsShowEmoticon == 1)
                    {
                        header1.Add("");
                        header2.Add("表情包选择");
                    }
                }
            }
            resultData.Add(header1);
            resultData.Add(header2);
            if (groupList != null && groupList.Count > 0)
            {
                foreach (var groupItem in groupList)
                {
                    var groupByDeviceAnswers = AllAnswerList.Where(a => a.GroupId == groupItem.GroupId).GroupBy(pet => pet.DeviceId).Select(g => (new { DeviceId = g.Key, Answers = g.ToList() }));
                    foreach (var device in groupByDeviceAnswers)
                    {
                        List<string> rowItem = new List<string>();
                        rowItem.Add(groupItem.GroupName);
                        rowItem.Add(device.DeviceId);

                        if (conceptCardList != null && conceptCardList.Count > 0)
                        {
                            foreach (var card in conceptCardList)
                            {
                                if (gradingQList.Count > 0)
                                {
                                    foreach (var gradingQItem in gradingQList)
                                    {
                                        var thisGradingQAnswer = device.Answers.Where(a => a.CardId == card.CardId && a.QuestionId == gradingQItem.GradingQuestionId).FirstOrDefault();
                                        if (thisGradingQAnswer != null)
                                        {
                                            rowItem.Add(thisGradingQAnswer.QuestionAnswer);
                                        }
                                        else
                                        {
                                            rowItem.Add("");
                                        }
                                    }
                                }
                                if (geq.IsShowOpenQuestion == 1)
                                {
                                    var thisOpenQAnswer = device.Answers.Where(a => a.CardId == card.CardId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.OpenText).FirstOrDefault();
                                    if (thisOpenQAnswer != null)
                                    {
                                        rowItem.Add(thisOpenQAnswer.QuestionAnswer);
                                    }
                                    else
                                    {
                                        rowItem.Add("");
                                    }
                                }
                                if (geq.IsShowEmoticon == 1)
                                {
                                    var thisIconQAnswer = device.Answers.Where(a => a.CardId == card.CardId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.Icon).FirstOrDefault();
                                    if (thisIconQAnswer != null)
                                    {
                                        rowItem.Add(thisIconQAnswer.QuestionAnswer);
                                    }
                                    else
                                    {
                                        rowItem.Add("");
                                    }
                                }
                            }
                        }
                        resultData.Add(rowItem);
                    }
                }
            }
            return resultData;
        }

        public List<List<string>> GetOrderData(int projectAutoId)
        {
            List<List<string>> resultData = new List<List<string>>();
            List<string> header2 = new List<string>();
            header2.Add("组名");
            header2.Add("设备ID");

            List<Group> groupList = GroupBLL.GetGroupList(projectAutoId);
            List<OrderQuestion> orderQList = OrderQuestionBLL.GetOrderQuestionList(projectAutoId).Where(o => o.OrderQuestionText != "").ToList();
            List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();


            if (orderQList != null && orderQList.Count > 0)
            {
                for (int i = 1; i <= orderQList.Count; i++)
                {
                    header2.Add("维度" + i + "排序(" + orderQList[i - 1].OrderQuestionText + ")");
                }
            }
            resultData.Add(header2);
            if (groupList != null && groupList.Count > 0)
            {
                foreach (var groupItem in groupList)
                {
                    var groupByDeviceAnswers = AllAnswerList.Where(a => a.GroupId == groupItem.GroupId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.OrderQuestion).GroupBy(pet => pet.DeviceId).Select(g => (new { DeviceId = g.Key, Answers = g.ToList() }));
                    foreach (var device in groupByDeviceAnswers)
                    {
                        List<string> rowItem = new List<string>();
                        rowItem.Add(groupItem.GroupName);
                        rowItem.Add(device.DeviceId);

                        if (orderQList.Count > 0)
                        {
                            foreach (var orderQItem in orderQList)
                            {
                                var thisOrderQAnswer = device.Answers.Where(an => an.QuestionId == orderQItem.OrderQuestionId).FirstOrDefault();
                                if (thisOrderQAnswer != null)
                                {
                                    rowItem.Add(thisOrderQAnswer.QuestionAnswer);
                                }
                                else
                                {
                                    rowItem.Add("");
                                }
                            }
                        }
                        resultData.Add(rowItem);
                    }
                }
            }
            return resultData;
        }

        public List<List<string>> GetClassifyData(int projectAutoId)
        {
            List<List<string>> resultData = new List<List<string>>();
            List<string> header2 = new List<string>();
            header2.Add("组名");
            header2.Add("设备ID");

            List<Group> groupList = GroupBLL.GetGroupList(projectAutoId);
            List<ClassifyQuestion> classifyQList = ClassifyQuestionBLL.GetClassifyQuestionList(projectAutoId).Where(o => o.ClassifyQuestionText != "").ToList();
            List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();


            if (classifyQList != null && classifyQList.Count > 0)
            {
                for (int i = 1; i <= classifyQList.Count; i++)
                {
                    header2.Add("分堆" + i + "(" + classifyQList[i - 1].ClassifyQuestionText + ")");
                }
            }
            resultData.Add(header2);
            if (groupList != null && groupList.Count > 0)
            {
                foreach (var groupItem in groupList)
                {
                    var groupByDeviceAnswers = AllAnswerList.Where(a => a.GroupId == groupItem.GroupId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.CategoryQuestion).GroupBy(pet => pet.DeviceId).Select(g => (new { DeviceId = g.Key, Answers = g.ToList() }));
                    foreach (var device in groupByDeviceAnswers)
                    {
                        List<string> rowItem = new List<string>();
                        rowItem.Add(groupItem.GroupName);
                        rowItem.Add(device.DeviceId);

                        if (classifyQList.Count > 0)
                        {
                            foreach (var classifyQItem in classifyQList)
                            {
                                var thisClassifyQAnswer = device.Answers.Where(an => an.QuestionId == classifyQItem.ClassifyQuestionId).FirstOrDefault();
                                if (thisClassifyQAnswer != null)
                                {
                                    rowItem.Add(thisClassifyQAnswer.QuestionAnswer);
                                }
                                else
                                {
                                    rowItem.Add("");
                                }
                            }
                        }
                        resultData.Add(rowItem);
                    }
                }
            }
            return resultData;
        }

        public List<List<string>> GetPaintingData(int projectAutoId)
        {
            List<List<string>> resultData = new List<List<string>>();
            List<string> header2 = new List<string>();
           // header2.Add("组名");
            header2.Add("设备ID"); 
            

            List<Group> groupList = GroupBLL.GetGroupList(projectAutoId);
            List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();
            List<ConceptCard> conceptCardList = ConceptCardBLL.GetConceptCardList(projectAutoId);

            if(conceptCardList!=null && conceptCardList.Count>0)
            {
                foreach(var card in conceptCardList)
                {
                    header2.Add("概念卡" + card.CardOrder + "(" + card.ConceptCardName + ")");
                }
            }
            resultData.Add(header2);

            if (groupList != null && groupList.Count > 0)
            {
                foreach (var groupItem in groupList)
                {
                    var groupByDeviceAnswers = AllAnswerList.Where(a => a.GroupId == groupItem.GroupId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.Painting).GroupBy(pet => pet.DeviceId).Select(g => (new { DeviceId = g.Key, Answers = g.ToList() }));
                    foreach (var device in groupByDeviceAnswers)
                    {
                        List<string> rowItem = new List<string>();
                      //  rowItem.Add(groupItem.GroupName);
                        rowItem.Add(device.DeviceId);
                        //to be continue 
                        if (conceptCardList != null && conceptCardList.Count > 0)
                        {
                            foreach (var card in conceptCardList)
                            {
                                var thisCardAnswer = device.Answers.Where(a => a.CardId == card.CardId).FirstOrDefault();
                                if (thisCardAnswer != null)
                                {
                                  //  rowItem.Add(urlMatch(thisCardAnswer.QuestionAnswer));
                                }
                                else
                                {
                                    rowItem.Add("");
                                }
                            }
                        } 
                        resultData.Add(rowItem);
                    }
                }
            }
            return resultData;
        }

        public OrderReportReturn GetOrderDataNew(int projectAutoId,string externalPjId,string groupid="",string choosedConceptPrjId="")
        {
            OrderReportReturn result = new OrderReportReturn(); 
            result.header = new List<string>();
            result.header.Add("组名");
            result.header.Add("用户ID");


            List<Group> groupList = GroupBLL.GetGroupList(projectAutoId);
            List<OrderQuestion> orderQList = OrderQuestionBLL.GetOrderQuestionList(projectAutoId).Where(o => o.OrderQuestionText != "").ToList();
            List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId,externalPjId,groupid).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();

            result.groups = new List<OrderGroup>();
            if (orderQList != null && orderQList.Count > 0)
            {
                for (int i = 1; i <= orderQList.Count; i++)
                {
                    result.header.Add("维度" + i + "排序(" + orderQList[i - 1].OrderQuestionText + ")");
                    result.header.Add("开放题");
                }
            } 
            if (groupList != null && groupList.Count > 0)
            {
                foreach (var groupItem in groupList)
                {
                    OrderGroup orderGroup = new OrderGroup();
                    orderGroup.name = groupItem.GroupName;
                    orderGroup.device = new List<OrderDeviceInfo>();
                 
                    var groupByDeviceAnswers = AllAnswerList.Where(a => a.GroupId == groupItem.GroupId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.OrderQuestion).GroupBy(pet => pet.DeviceId).Select(g => (new { DeviceId = g.Key, Answers = g.ToList() }));
                    foreach (var device in groupByDeviceAnswers)
                    {
                        OrderDeviceInfo orderDeviceInfoItem = new OrderDeviceInfo();
                        orderDeviceInfoItem.deviceId = VariableAnswerBLL.GetUserNameByOpenId(device.DeviceId) ;
                        orderDeviceInfoItem.orders = new List<string>();
                         

                        if (orderQList.Count > 0)
                        {
                            foreach (var orderQItem in orderQList)
                            {
                                var thisOrderQAnswer = device.Answers.Where(an => an.QuestionId == orderQItem.OrderQuestionId).FirstOrDefault();
                                if (thisOrderQAnswer != null)
                                {
                                    orderDeviceInfoItem.orders.Add(thisOrderQAnswer.QuestionAnswer);
                                    orderDeviceInfoItem.orders.Add(thisOrderQAnswer.OpenText);
                                }
                                else
                                {
                                    orderDeviceInfoItem.orders.Add("");
                                    orderDeviceInfoItem.orders.Add("");
                                }
                            }
                        }
                       // orderDeviceInfoItem.details = VariableAnswerBLL.GetUserDetail(orderDeviceInfoItem.deviceId);
                        orderGroup.device.Add(orderDeviceInfoItem);
                    }
                    result.groups.Add(orderGroup);
                }
            }
            return result;
        }
        public OrderReportReturn GetOrderDataNew2(int projectAutoId, string externalPjId, string groupid = "", string choosedConceptPrjId = "")
        {
            OrderReportReturn result = new OrderReportReturn();
            result.header = new List<string>();
         
            result.header.Add("用户ID");


            List<Group> groupList = GroupBLL.GetGroupList(projectAutoId);
            List<OrderQuestion> orderQList = OrderQuestionBLL.GetOrderQuestionList(projectAutoId).Where(o => o.OrderQuestionText != "").ToList();
            List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId, externalPjId, groupid).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();

            result.groups = new List<OrderGroup>();
            if (orderQList != null && orderQList.Count > 0)
            {
                for (int i = 1; i <= orderQList.Count; i++)
                {
                    result.header.Add("维度" + i + "排序(" + orderQList[i - 1].OrderQuestionText + ")");
                    result.header.Add("开放题");
                }
            }
            if (groupList != null && groupList.Count > 0)
            {
                foreach (var groupItem in groupList)
                {
                    OrderGroup orderGroup = new OrderGroup();
                    orderGroup.name = groupItem.GroupName;
                    orderGroup.device = new List<OrderDeviceInfo>();

                    var groupByDeviceAnswers = AllAnswerList.Where(a => a.GroupId == groupItem.GroupId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.OrderQuestion).GroupBy(pet => pet.DeviceId).Select(g => (new { DeviceId = g.Key, Answers = g.ToList() }));
                    foreach (var device in groupByDeviceAnswers)
                    {
                        OrderDeviceInfo orderDeviceInfoItem = new OrderDeviceInfo();
                        orderDeviceInfoItem.deviceId = VariableAnswerBLL.GetUserNameByOpenId(device.DeviceId);
                        orderDeviceInfoItem.orders = new List<string>();


                        if (orderQList.Count > 0)
                        {
                            foreach (var orderQItem in orderQList)
                            {
                                var thisOrderQAnswer = device.Answers.Where(an => an.QuestionId == orderQItem.OrderQuestionId).FirstOrDefault();
                                if (thisOrderQAnswer != null)
                                {
                                    orderDeviceInfoItem.orders.Add(thisOrderQAnswer.QuestionAnswer);
                                    orderDeviceInfoItem.orders.Add(thisOrderQAnswer.OpenText);
                                }
                                else
                                {
                                    orderDeviceInfoItem.orders.Add("");
                                    orderDeviceInfoItem.orders.Add("");
                                }
                            }
                        }
                        // orderDeviceInfoItem.details = VariableAnswerBLL.GetUserDetail(orderDeviceInfoItem.deviceId);
                        orderGroup.device.Add(orderDeviceInfoItem);
                    }
                    result.groups.Add(orderGroup);
                }
            }
            return result;
        }
        public ClassifyReportReturn GetClassifyDataNew2(int projectAutoId, string externalPjId, string groupid = "")
        {
            ClassifyReportReturn result = new ClassifyReportReturn();

            result.header = new List<string>();
           
            result.header.Add("用户ID");

            List<Group> groupList = GroupBLL.GetGroupList(projectAutoId);
            List<ClassifyQuestion> classifyQList = ClassifyQuestionBLL.GetClassifyQuestionList(projectAutoId).Where(o => o.ClassifyQuestionText != "").ToList();
            List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId, externalPjId, groupid).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();
            result.groups = new List<ClassfiyGroup>();

            if (classifyQList != null && classifyQList.Count > 0)
            {
                for (int i = 1; i <= classifyQList.Count; i++)
                {
                    result.header.Add("分堆" + i + "(" + classifyQList[i - 1].ClassifyQuestionText + ")");
              
                }
                result.header.Add("开放题");
            }

            if (groupList != null && groupList.Count > 0)
            {

                foreach (var groupItem in groupList)
                {
                    ClassfiyGroup classfiyGroup = new ClassfiyGroup();
                    classfiyGroup.name = groupItem.GroupName;
                    classfiyGroup.device = new List<ClassfiyDeviceInfo>();
                    var groupByDeviceAnswers = AllAnswerList.Where(a => a.GroupId == groupItem.GroupId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.CategoryQuestion).GroupBy(pet => pet.DeviceId).Select(g => (new { DeviceId = g.Key, Answers = g.ToList() }));
                    foreach (var device in groupByDeviceAnswers)
                    {
                        ClassfiyDeviceInfo classfiyDeviceInfoItem = new ClassfiyDeviceInfo();
                        classfiyDeviceInfoItem.deviceId = VariableAnswerBLL.GetUserNameByOpenId(device.DeviceId);
                        classfiyDeviceInfoItem.duis = new List<string>();

                        if (classifyQList.Count > 0)
                        {
                            foreach (var classifyQItem in classifyQList)
                            {
                                var userAnswers = device.Answers.Where(an => an.QuestionId == classifyQItem.ClassifyQuestionId);
                                var qAnswers = userAnswers.Select(q => q.QuestionAnswer);
                                if (qAnswers != null)
                                {
                                    classfiyDeviceInfoItem.duis.Add(String.Join(",", userAnswers.Select(u => u.QuestionAnswer)));
                                   // classfiyDeviceInfoItem.duis.Add(String.Join(",", userAnswers.Select(u => u.OpenText)));

                                }
                                else
                                {
                                    classfiyDeviceInfoItem.duis.Add("");
                                  
                                }
                            }
                            //获取开放题
                            var OpenText = MapGetUtil.MapGet(device.Answers[0].AppendData,"OpenText");
                            classfiyDeviceInfoItem.duis.Add(OpenText);
                        }
                        // classfiyDeviceInfoItem.details = VariableAnswerBLL.GetUserDetail(classfiyDeviceInfoItem.deviceId);
                        classfiyGroup.device.Add(classfiyDeviceInfoItem);
                    }
                    result.groups.Add(classfiyGroup);
                }
            }
            return result;
        }


        public ClassifyReportReturn GetClassifyDataNew(int projectAutoId,string externalPjId,string groupid="")
        {
            ClassifyReportReturn result = new ClassifyReportReturn();
             
            result.header = new List<string>();
            result.header.Add("组名");
            result.header.Add("用户ID");

            List<Group> groupList = GroupBLL.GetGroupList(projectAutoId);
            List<ClassifyQuestion> classifyQList = ClassifyQuestionBLL.GetClassifyQuestionList(projectAutoId).Where(o => o.ClassifyQuestionText != "").ToList();
            List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId,externalPjId,groupid).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();
            result.groups = new List<ClassfiyGroup>();

            if (classifyQList != null && classifyQList.Count > 0)
            {
                for (int i = 1; i <= classifyQList.Count; i++)
                {
                    result.header.Add("分堆" + i + "(" + classifyQList[i - 1].ClassifyQuestionText + ")");
                    result.header.Add("开放题");
                }
            } 
            if (groupList != null && groupList.Count > 0)
            {
                
                foreach (var groupItem in groupList)
                {
                    ClassfiyGroup classfiyGroup = new ClassfiyGroup();
                    classfiyGroup.name = groupItem.GroupName;
                    classfiyGroup.device = new List<ClassfiyDeviceInfo>();
                    var groupByDeviceAnswers = AllAnswerList.Where(a => a.GroupId == groupItem.GroupId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.CategoryQuestion).GroupBy(pet => pet.DeviceId).Select(g => (new { DeviceId = g.Key, Answers = g.ToList() }));
                    foreach (var device in groupByDeviceAnswers)
                    {
                        ClassfiyDeviceInfo classfiyDeviceInfoItem = new ClassfiyDeviceInfo();
                        classfiyDeviceInfoItem.deviceId = VariableAnswerBLL.GetUserNameByOpenId(device.DeviceId);
                        classfiyDeviceInfoItem.duis = new List<string>(); 

                        if (classifyQList.Count > 0)
                        {
                            foreach (var classifyQItem in classifyQList)
                            {
                                var userAnswers = device.Answers.Where(an => an.QuestionId == classifyQItem.ClassifyQuestionId);
                                 var qAnswers=userAnswers.Select(q=>q.QuestionAnswer);
                                if (qAnswers != null)
                                {
                                    classfiyDeviceInfoItem.duis.Add(String.Join(",",userAnswers.Select(u=>u.QuestionAnswer)));
                                    classfiyDeviceInfoItem.duis.Add(String.Join(",",userAnswers.Select(u=>u.OpenText)));

                                }
                                else
                                {
                                    classfiyDeviceInfoItem.duis.Add("");
                                    classfiyDeviceInfoItem.duis.Add("");
                                }
                            }
                        }
                       // classfiyDeviceInfoItem.details = VariableAnswerBLL.GetUserDetail(classfiyDeviceInfoItem.deviceId);
                        classfiyGroup.device.Add(classfiyDeviceInfoItem);
                    }
                    result.groups.Add(classfiyGroup);
                }
            }
            return result;
        }

        /// <summary>
        /// 方法只用于conceptTest项目。
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <param name="externalPjId"></param>
        /// <param name="exgroupid"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public GradingReportReturn GetGradingEmotionDataNew2(int projectAutoId, string externalPjId, string exgroupid = "", int cardId = -1)
        {
            GradingReportReturn result = new GradingReportReturn();
            try
           
            {
            result.cards = new List<string>();
            result.header = new List<string>();
            //result.header.Add("组名");
            result.header.Add("用户ID");

            List<Group> groupList = GroupBLL.GetGroupList(projectAutoId);
            List<ConceptCard> conceptCardList = ConceptCardBLL.GetConceptCardList(projectAutoId);
            List<GradingQuestion> gradingQList = GradingQuestionBLL.GetGradingQuestionList(projectAutoId);
                //.Where(gq => gq.QuestionText != "").ToList();
            GradingEmoticonQuestion geq = GradingEmoticonQuestionBLL.GetGradingEmoticonQuestion(projectAutoId);
            List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId, externalPjId, exgroupid).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();
            if (conceptCardList != null && conceptCardList.Count > 0)
            {
                conceptCardList = cardId == -1 ? conceptCardList : conceptCardList.Where(c => c.CardId == cardId).ToList();
            }

            if (conceptCardList != null && conceptCardList.Count > 0)
            {
                foreach (var card in conceptCardList)
                {
                    result.cards.Add("概念卡" + card.CardOrder + "(" + card.ConceptCardName + ")");
                    if (gradingQList.Count > 0)
                    {
                        for (int j = 1; j <= gradingQList.Count; j++)
                        {
                            result.header.Add(string.Format("维度{0}分|{1}",j,gradingQList[j-1].QuestionText));
                        }
                    }
                 //   if (geq.IsShowOpenQuestion == 1)
                   // {
                       result.header.Add("开放题答案");
                  //  }
                ///    if (geq.IsShowEmoticon == 1)
                  //  {
                        result.header.Add("表情包选择");
                 //   }
                }
            }
            result.groups = new List<GradingGroup>();
            if (groupList != null && groupList.Count > 0)
            {
                foreach (var groupItem in groupList)
                {
                    GradingGroup gradingGroup = new GradingGroup();
                    gradingGroup.name = groupItem.GroupName;
                    gradingGroup.device = new List<GradingDeviceInfo>();
                        Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException
                        {
                            AddTime = DateTime.Now,
                            ExceptMessage = AllAnswerList.Count.ToString(),
                            MessDescript = "",
                            ModuleName = "ConceptController:gradingGroup:AllCount"
                        });
                        var groupByDeviceAnswers = AllAnswerList.Where(a => a.GroupId == groupItem.GroupId).GroupBy(pet => pet.DeviceId).Select(g => (new { DeviceId = g.Key, Answers = g.ToList() }));
                    foreach (var device in groupByDeviceAnswers)
                    {
                        GradingDeviceInfo gradingDeviceInfoItem = new GradingDeviceInfo();
                        gradingDeviceInfoItem.deviceId = VariableAnswerBLL.GetUserNameByOpenId(device.DeviceId);

                        gradingDeviceInfoItem.angles = new List<string>();

                        //if(gradingDeviceInfoItem.angles.Count==0)
                        //foreach (var item in result.cards)
                        //{
                        //        gradingDeviceInfoItem.angles.Add("");
                        //}

                        if (conceptCardList != null && conceptCardList.Count > 0)
                        {
                            foreach (var card in conceptCardList)
                            {

                                if (gradingQList.Count > 0)
                                {
                                    foreach (var gradingQItem in gradingQList)
                                    {
                                        var thisGradingQAnswer = device.Answers.Where(a => a.CardId == card.CardId && a.QuestionId == gradingQItem.GradingQuestionId).FirstOrDefault();
                                        if (thisGradingQAnswer != null)
                                        {
                                            gradingDeviceInfoItem.angles.Add(thisGradingQAnswer.QuestionAnswer);
                                        }
                                        else
                                        {
                                            gradingDeviceInfoItem.angles.Add("");
                                        }
                                    }
                                }

                                var thisOpenQAnswer = device.Answers.Where(a => a.CardId == card.CardId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.OpenText).FirstOrDefault();
                                if (thisOpenQAnswer != null)
                                {
                                    gradingDeviceInfoItem.angles.Add(thisOpenQAnswer.QuestionAnswer);
                                }
                                else
                                {
                                    gradingDeviceInfoItem.angles.Add("");
                                }


                                var thisIconQAnswer = device.Answers.Where(a => a.CardId == card.CardId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.Icon).FirstOrDefault();
                                if (thisIconQAnswer != null)
                                {
                                    gradingDeviceInfoItem.angles.Add(thisIconQAnswer.QuestionAnswer);
                                }
                                else
                                {
                                    gradingDeviceInfoItem.angles.Add("");
                                }

                            }
                        }

                        //gradingDeviceInfoItem.details = VariableAnswerBLL.GetUserDetail(gradingDeviceInfoItem.deviceId);
                        gradingGroup.device.Add(gradingDeviceInfoItem);

                     
                    }
                    //总分
                    var sumDevice = new GradingDeviceInfo();
                    sumDevice.angles = new List<string>();
                    sumDevice.deviceId = "总分";
                        Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException
                        {
                            AddTime = DateTime.Now,
                            ExceptMessage =gradingGroup.device.Count.ToString(),
                            MessDescript = "",
                            ModuleName = "ConceptController:gradingGroup"
                        });
                        for (int i = 0; i < gradingGroup.device[0].angles.Count; i++)
                    {
                        var allcount = gradingGroup.device.Sum(d =>
                        {
                            int val;
                            if (result.header[1 + i] != "表情包选择"  && result.header[1+i]!= "开放题答案")
                            {
                                try
                                {
                                    if (d.angles[i] == "") return 0;
                                     return int.Parse(d.angles[i]);
                                }
                                catch (Exception)
                                {

                                    throw;
                                }
                              
                            }
                            else
                            {
                                return -10000;
                            }
                        });
                        if (allcount > -10000)
                        {
                            sumDevice.angles.Add(allcount.ToString());
                        }
                        else
                        {
                            sumDevice.angles.Add("N/A");
                        }
                    }
                    //平均分
                    var avgDevice = new GradingDeviceInfo();
                    avgDevice.angles = new List<string>();
                    avgDevice.deviceId = "平均分";
                    for (int i = 0; i < gradingGroup.device[0].angles.Count; i++)
                    {
                        int Val;
                        if (sumDevice.angles[i] != "N/A")
                        {
                            if (int.TryParse(sumDevice.angles[i], out Val))
                            {
                                 string avgVal = String.Format("{0:N1}",(Val / (gradingGroup.device.Count * 1.0f)));
                                avgDevice.angles.Add(avgVal);
                            }
                        }
                        else
                        {
                            avgDevice.angles.Add("N/A");
                        }
                    }
                    gradingGroup.device.Insert(0,avgDevice);
                    gradingGroup.device.Insert(0,sumDevice);
                    result.groups.Add(gradingGroup);
                }
            }
            return result;
            }
            catch (Exception ex)
            {
                result.header = new List<string>();
                result.header.Add(ex.ToString());
                return result;
            }
        }
        public GradingReportReturn GetGradingEmotionDataNew(int projectAutoId,string externalPjId,string exgroupid="",int cardId=-1)
        {
            GradingReportReturn result = new GradingReportReturn();
            result.cards = new List<string>(); 
            result.header = new List<string>();
            result.header.Add("组名");
            result.header.Add("用户ID");

            List<Group> groupList = GroupBLL.GetGroupList(projectAutoId);
            List<ConceptCard> conceptCardList = ConceptCardBLL.GetConceptCardList(projectAutoId);
            List<GradingQuestion> gradingQList = GradingQuestionBLL.GetGradingQuestionList(projectAutoId).Where(gq => gq.QuestionText != "").ToList();
            GradingEmoticonQuestion geq = GradingEmoticonQuestionBLL.GetGradingEmoticonQuestion(projectAutoId);
            List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId,externalPjId,exgroupid).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();
            if (conceptCardList != null && conceptCardList.Count > 0)
            {
                conceptCardList = cardId == -1 ? conceptCardList : conceptCardList.Where(c => c.CardId == cardId).ToList();
            }

            if (conceptCardList != null && conceptCardList.Count > 0)
            {
                foreach (var card in conceptCardList)
                {
                    result.cards.Add("概念卡" + card.CardOrder + "(" + card.ConceptCardName + ")");
                    if (gradingQList.Count > 0)
                    {
                        for (int j = 1; j <= gradingQList.Count; j++)
                        { 
                            result.header.Add("维度" + j + "打分");
                        }
                    }
                    if (geq.IsShowOpenQuestion == 1)
                    { 
                        result.header.Add("开放题答案");
                    }
                    if (geq.IsShowEmoticon == 1)
                    { 
                        result.header.Add("表情包选择");
                    }
                }
            }
            result.groups = new List<GradingGroup>();
            if (groupList != null && groupList.Count > 0)
            {
                foreach (var groupItem in groupList)
                {
                    GradingGroup gradingGroup = new GradingGroup();
                    gradingGroup.name = groupItem.GroupName;
                    gradingGroup.device = new List<GradingDeviceInfo>();
                    var groupByDeviceAnswers = AllAnswerList.Where(a => a.GroupId == groupItem.GroupId).GroupBy(pet => pet.DeviceId).Select(g => (new { DeviceId = g.Key, Answers = g.ToList() }));
                    foreach (var device in groupByDeviceAnswers)
                    {
                        GradingDeviceInfo gradingDeviceInfoItem = new GradingDeviceInfo();
                        gradingDeviceInfoItem.deviceId = VariableAnswerBLL.GetUserNameByOpenId(device.DeviceId);

                        gradingDeviceInfoItem.angles = new List<string>();

                        //if(gradingDeviceInfoItem.angles.Count==0)
                        //foreach (var item in result.cards)
                        //{
                        //        gradingDeviceInfoItem.angles.Add("");
                        //}
                    
                        if (conceptCardList != null && conceptCardList.Count > 0)
                        {
                            foreach (var card in conceptCardList)
                            {

                                if (gradingQList.Count > 0)
                                {
                                    foreach (var gradingQItem in gradingQList)
                                    {
                                        var thisGradingQAnswer = device.Answers.Where(a => a.CardId == card.CardId && a.QuestionId == gradingQItem.GradingQuestionId).FirstOrDefault();
                                        if (thisGradingQAnswer != null)
                                        {
                                            gradingDeviceInfoItem.angles.Add(thisGradingQAnswer.QuestionAnswer);
                                        }
                                        else
                                        {
                                            gradingDeviceInfoItem.angles.Add("");
                                        }
                                    }
                                }
                                if (geq.IsShowOpenQuestion == 1)
                                {
                                    var thisOpenQAnswer = device.Answers.Where(a => a.CardId == card.CardId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.OpenText).FirstOrDefault();
                                    if (thisOpenQAnswer != null)
                                    {
                                        gradingDeviceInfoItem.angles.Add(thisOpenQAnswer.QuestionAnswer);
                                    }
                                    else
                                    {
                                        gradingDeviceInfoItem.angles.Add("");
                                    }
                                }
                              if (geq.IsShowEmoticon == 1)
                                {
                                    var thisIconQAnswer = device.Answers.Where(a => a.CardId == card.CardId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.Icon).FirstOrDefault();
                                    if (thisIconQAnswer != null)
                                    {
                                        gradingDeviceInfoItem.angles.Add(thisIconQAnswer.QuestionAnswer);
                                    }
                                    else
                                    {
                                        gradingDeviceInfoItem.angles.Add("");
                                    }
                                }
                            }
                        }
                        //gradingDeviceInfoItem.details = VariableAnswerBLL.GetUserDetail(gradingDeviceInfoItem.deviceId);
                        gradingGroup.device.Add(gradingDeviceInfoItem);
                    }
                    result.groups.Add(gradingGroup);
                }
            }
            return result;
        }
        public DataTable TransferToDT(List<List<string>> data)
        {
            DataTable resultTable = new DataTable();
            List<string> columns = data[0];
            for (int j = 0; j < columns.Count;j++ )
            {
                resultTable.Columns.Add("D"+j);
            }
            for (int i = 0; i < data.Count; i++)
            {
                DataRow newDr = resultTable.NewRow();
                newDr.ItemArray = data[i].ToArray();
                resultTable.Rows.Add(newDr);
            }
            return resultTable;
        }

        public GradingTotalReturn GetGradingTotalNew(int projectAutoId,string externalPjId,string groupid="")
        {
            GradingTotalReturn result = new GradingTotalReturn();

            List<ConceptCard> conceptCardList = ConceptCardBLL.GetConceptCardList(projectAutoId);
            List<GradingQuestion> gradingQList = GradingQuestionBLL.GetGradingQuestionList(projectAutoId).Where(gq => gq.QuestionText != "").ToList();

            List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId,externalPjId,groupid).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();
            result.Header = new List<string>();
            if (gradingQList != null && gradingQList.Count > 0)
            {
                for (int j = 1; j <= gradingQList.Count; j++)
                {
                    result.Header.Add("维度" + j + "-" + gradingQList[j - 1].QuestionText);
                }
                result.Header.Add("所有维度总分");
            }
            
            if (conceptCardList != null && conceptCardList.Count > 0)
            {
                result.Contents = new List<List<string>>(); 
                foreach (var card in conceptCardList)
                { 
                    long rowTotal = 0;
                    List<string> cardRow = new List<string>();
                    cardRow.Add(ConfigurationManager.AppSettings["DisplayCardImage"] + card.ImageUrl);
                    cardRow.Add(card.ConceptCardName);
                    
                    if (gradingQList.Count > 0)
                    {
                        foreach (var gradingQItem in gradingQList)
                        {
                            List<DeviceAnswer> thisCardQAnswer = AllAnswerList.Where(a => a.CardId == card.CardId && a.QuestionId == gradingQItem.GradingQuestionId).ToList();
                            var thisGradingQAnswerTotal = 0;
                            if (thisCardQAnswer!=null || thisCardQAnswer.Count>0)
                            {
                                thisGradingQAnswerTotal = thisCardQAnswer.Sum(it => Convert.ToInt32(it.QuestionAnswer));
                                rowTotal = rowTotal + thisGradingQAnswerTotal;
                            } 
                            cardRow.Add(thisGradingQAnswerTotal.ToString()); 
                        }
                        cardRow.Add(rowTotal.ToString());
                    } 
                    result.Contents.Add(cardRow); 
                }
                List<string> cardRowResult = new List<string>() { "", "结果" }; ;
                for (int j = 2; j < result.Contents[0].Count; j++)
                {
                    string diV = result.Contents.Select(cn => new { cardName = cn[1], dimValue = cn[j] }).OrderByDescending(i => Convert.ToInt32(i.dimValue)).FirstOrDefault().dimValue;
                    var namelist = result.Contents.Select(cn => new { cardName = cn[1], dimValue = cn[j] }).Where(ii => ii.dimValue == diV).ToList();
                    StringBuilder nameBuilder = new StringBuilder();
                    foreach (var n in namelist)
                    {
                        nameBuilder.Append(n.cardName);
                        nameBuilder.Append(";");
                    }
                    //string dd = result.Contents.Select(cn => new { cardName = cn[1], dimValue = cn[j] }).OrderByDescending(i => Convert.ToInt32(i.dimValue)).FirstOrDefault().cardName;
                    cardRowResult.Add(nameBuilder.ToString().TrimEnd(';'));
                }
                result.Contents.Add(cardRowResult); 
            }
                         
            return result;
        }
  

    }
 
}
