﻿using Concept.BLL.BaseBD.Log;
using Concept.Entity.BaseDB.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptApiController.BLL
{
    public class LogLogical
    {
        #region 单例定义
        private static object lockObject = new object();
        protected LogLogical()
        {
        }

        private static volatile LogLogical _instance;
        public static LogLogical Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new LogLogical());
                    }
                }
                return _instance;
            }
        }

        #endregion 单例定义 

        public void AddException(Exception ex, string ModelName)
        {
            Concept_LogException log = new Concept_LogException();
            log.AddTime = DateTime.Now;
            log.ExceptMessage = ex.ToString();
            log.ModuleName = ModelName;
            log.ExceptStackTrace = ex.StackTrace;
            Concept_LogExceptionBLL.InsertLogExceptionInfo(log);
        }
    }
}
