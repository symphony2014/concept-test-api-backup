﻿using Concept.BLL.BaseBD;
using Concept.Entity.BaseDB;
using ConceptCommon.ParaMode;
using ConceptCommon.ReturnMode;
using IpsosReport.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptApiController.BLL
{
    public class PGRuleLogical
    {
        #region 单例定义
        private static object lockObject = new object();
        protected PGRuleLogical()
        {
        }

        private static volatile PGRuleLogical _instance;
        public static PGRuleLogical Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new PGRuleLogical());
                    }
                }
                return _instance;
            }
        }

        #endregion 单例定义 
        public readonly string PGDomain = ConfigurationManager.AppSettings["ExternalAPI"];
        public VoidReturn PullRuleResult(PGParaMode ruleParaInfo)
        {
            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            List<RuleAnalysisResults> conceptRuleAnalysisResults = RuleAnalysisResultsBLL.GetItemList(ruleParaInfo.GroupId, ruleParaInfo.ExternalPjId);
            List<PGExternalRuleResult> postparms = new List<PGExternalRuleResult>();
            conceptRuleAnalysisResults.ForEach(item =>
            {
                PGExternalRuleResult resultItem = new PGExternalRuleResult();
                resultItem.callSampGrpId = item.OriginalGroupId;
                resultItem.prjId = item.ExternalPjId;
                resultItem.ruleId = item.RuleId;
                resultItem.sampGrpId = item.GotoGroupId;
                resultItem.userId = item.OpenId;
                postparms.Add(resultItem);
            });
            if(postparms.Count>0)
            {
                string postdata = JsonConvert.SerializeObject(postparms);
                string returnStr = HttpHelper.HttpPost(PGDomain+"/ruleresult/add", postdata);
                if (returnStr == "error")
                {
                    vr.Result = -1;
                    vr.Message = "异常错误";
                }
                else
                {
                    PGReturn pgReturn = JsonConvert.DeserializeObject(returnStr, typeof(PGReturn)) as PGReturn;
                    if(pgReturn.statusCode!=200)
                    {
                        vr.Result = -1;
                        vr.Message = pgReturn.errMsg;
                    }
                }
            }
            else
            {
                vr.Result = -1;
                vr.Message = "没有相应结果";
            }
            return vr;
        }
    }
}
