﻿using Concept.BLL.BaseBD;
using Concept.Entity.BaseDB;

using ConceptCommon;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using ExternalAPI;
using Concept.Entity.External;
using System.Net.Http.Headers;
using System.Configuration;

namespace ConceptApiController
{
    public class UploadController : BaseController
    {
        public readonly string topicPath = ConfigurationManager.AppSettings["TopicPath"];
        [HttpPost]
        public async Task<HttpResponseMessage> UploadExcel(string externalPjId, bool isRadar)
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/Resource");
            var provider = new MultipartFormDataStreamProvider(root);

            try
            {
                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);
                // This illustrates how to get the file names.
                string newFileName = string.Empty;
                string link = string.Empty;
                foreach (MultipartFileData file in provider.FileData)
                {
                    newFileName = file.LocalFileName + Path.GetExtension(file.Headers.ContentDisposition.FileName.Replace("\"", ""));
                    File.Move(file.LocalFileName, newFileName);
                    link = "Resource/" + Path.GetFileName(newFileName);
                }
                if (isRadar)
                {
                    Import.ImportRadar(newFileName);
                }
                else
                {
                    Import.ImportVariables(newFileName, externalPjId);
                }

                return Request.CreateResponse(HttpStatusCode.OK, 1);
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.ToString());
            }
        }
         [HttpPost]
        public async Task<HttpResponseMessage> AppendExcel(string externalPjId)
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/Resource");
            var provider = new MultipartFormDataStreamProvider(root);

            try
            {
                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);
                // This illustrates how to get the file names.
                string newFileName = string.Empty;
                string link = string.Empty;
                foreach (MultipartFileData file in provider.FileData)
                {
                    newFileName = file.LocalFileName + Path.GetExtension(file.Headers.ContentDisposition.FileName.Replace("\"", ""));
                    File.Move(file.LocalFileName, newFileName);
                    link = "Resource/" + Path.GetFileName(newFileName);
                }
                Import.ImportVariablesAppend(newFileName, externalPjId);

                return Request.CreateResponse(HttpStatusCode.OK, 1);
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.ToString());
            }
        }
        public async Task<HttpResponseMessage> GetExternalProjects()
        {
            try
            {
                var projects = await Import.GetExternalProjects();
                return Request.CreateResponse(HttpStatusCode.OK, projects);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.ToString());
            }
        }
        public HttpResponseMessage PostDownloadZip(ZipParam param)
        {
            string resourceName = DateTime.Now.Ticks.ToString();
            string directory = topicPath + "/resource/"+resourceName;
            Directory.CreateDirectory(directory);
            string filename = Import.GetTopicInfoExcel(directory, param.ExternalPjId, param.GroupId, param.TopicId);
            string jpgFile = Import.GenerateExcelFromDataUrl(param.DataUrl, directory);
            ZipClass.GetInstance().Zip(directory,directory+".zip");
            return Request.CreateResponse(HttpStatusCode.OK,Path.Combine("res",resourceName+".zip"));
       }

    }
}
