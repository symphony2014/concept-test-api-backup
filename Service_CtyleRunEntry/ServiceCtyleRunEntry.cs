﻿ 
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using ConceptCommon.Interface;
using MappingServeice.Base;


namespace Service_CtyleRunEntry
{
    public partial class ServiceCtyleRunEntry : MappingServiceBase
    {
        //private const string ZhengxinBusinessLogicDLLPath = "ZhengxinBusinessLogic.dll";
        public ServiceCtyleRunEntry()
        {
            InitializeComponent();
        }

        protected override void TestRunProcessLogic()
        {
            this.OnStartLogic(null);
        }

        protected override IServiceRun GetServiceRunProvider()
        {
            return this.GetServiceRunProviderFromConfig();
        }

        protected override void OnStartLogic(string[] args)
        {
            ISettingRun settingRunProvider = this.GetSettingRunProvider();
            IServiceRun serviceRunProvider = this.GetServiceRunProvider();

            string intervalRaw = ConfigurationManager.AppSettings["ExecuteInterval"];

            int intervalValue;
          
            if (string.IsNullOrEmpty(intervalRaw) || !int.TryParse(intervalRaw, out intervalValue))
            {
                throw new InvalidOperationException("无法获取ExecuteInterval配置。");
            }
         
            settingRunProvider.CtycleRun(serviceRunProvider, new TimeSpan(0, 0, intervalValue), null);
        }
    }
}
