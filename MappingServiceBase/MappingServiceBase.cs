﻿using System;
using System.Configuration;
using System.Reflection;
using System.ServiceProcess; 
using System.Linq; 
using System.IO; 
using System.Threading; 
using ConceptCommon.Interface;
using ConceptCommon.WinSerState;
using ConceptCommon;

namespace MappingServeice.Base
{
    public class MappingServiceBase : ServiceBase
    {
       
        protected string zhengxinName = string.Empty;

        protected const string ZhengxinBusinessLogicDLLPath = "ZhengxinBusinessLogic.dll";

        public void TestRun()
        {
            this.TestRunProcessLogic();
            //this.OnStart(null);
        }

        protected string GetServiceName(string strServiceName)
        {
            if (!strServiceName.Contains("."))
            {
                return strServiceName;
            }
            string strName = string.Empty;
            int nCount = strServiceName.LastIndexOf(".");
            if (nCount < 0)
            {
                throw new InvalidOperationException("配置文件不对"); 
            }
            string strServer= strServiceName.Substring(nCount + 1);
            return strServer;
        } 
         
        /// <summary>
        /// 服务状态函数
        /// </summary>
        /// <param name="winSerSateType"></param>
        protected void ServerRunState(WinSerSateType winSerSateType)
        {
            //WinSerRunSate winSerRunSate = new WinSerRunSate();
            //winSerRunSate.OpTypeState = winSerSateType.ToString();
            //if (string.IsNullOrEmpty(zhengxinName))
            //{
            //    throw new ArgumentException("配置文件没有取到");
            //}
            //zhengxinName =  GetServiceName(zhengxinName);
            //winSerRunSate.ProcessName = zhengxinName;
            //try
            //{
            //    WinSerRunSateBLL.InsertWinSerRunSate(winSerRunSate, new string[] { "OpTypeState", "ProcessName" });
            //}
            //catch(Exception e)
            //{
            //    IWriteLogMessProvider.WritreLogExption(e, "ZhengxinServiceBase/ServerRunState");
            //}
        }
         
        /// <summary>
        /// windows service 框架主入口点
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            this.OnStartLogic(args);
            ServerRunState(WinSerSateType.StartRunning);
        }

        /// <summary>
        /// Service 单步Run入口。
        /// </summary>
        protected virtual void TestRunProcessLogic()
        {
            IServiceRun serviceProvider = this.GetServiceRunProvider();
            serviceProvider.Process();
        }

        protected override void OnStop()
        {
            ServerRunState(WinSerSateType.StopRunning);
        }

        protected virtual ISettingRun GetSettingRunProvider()
        {
            return new SettingRunProvider();
        }
         
        /// <summary>
        /// 应用逻辑的主入口点，关联当前Service使用哪个IServiceRun 去处理业务。
        /// </summary>
        /// <returns></returns>
        protected virtual IServiceRun GetServiceRunProvider()
        {
            throw new NotImplementedException();
        }

        protected virtual IServiceRun GetServiceRunProviderFromConfig()
        {
            string configName = "IServiceRunTarget";
            var configItme = ConfigurationManager.AppSettings.AllKeys.Where(item => configName.Equals(item));
            int configItemCount = configItme.Count();
            if (configItme.Count() == 0)
            {
                throw new InvalidOperationException("无法获取IServiceRunTarget配置");
            }

            if (configItme.Count() != 1)
            {
                throw new InvalidOperationException("无法获取正确的IServiceRunTarget配置");
            }
             
            string targetTypeName = ConfigurationManager.AppSettings[configName];
            //获取配置名称
            zhengxinName = targetTypeName;

            string targetTypeDllName = ConfigurationManager.AppSettings["IServiceRunTargetDll"];
            if (string.IsNullOrEmpty(targetTypeDllName))
            {
                targetTypeDllName = ZhengxinBusinessLogicDLLPath;
            }

            IServiceRun result = ReflectionHelper.GetInstanceByTypeName<IServiceRun>(targetTypeName, targetTypeDllName);
             
            return result;

        }

        protected virtual void OnStartLogic(string[] args)
        {
            ISettingRun settingRunProvider = this.GetSettingRunProvider();
            IServiceRun serviceRunProvider = this.GetServiceRunProvider();
            settingRunProvider.SettingRun(serviceRunProvider);
        }
    }
}
