﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Concept.BLL.BaseBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.Entity.BaseDB;
using Concept.Entity.External;
using ExternalAPI;

namespace Concept.BLL.BaseBD.Tests
{
    [TestClass()]
    public class MessageBLLTests
    {
       
        [TestMethod()]
        public void 测试获取message最新时间()
        {
            string date = MessageBLL.GetLastUpdate("38","161", "1519");
            ExternalTopicParam param = new ExternalTopicParam();
            param.parms = new param
            {
                sttm = date,
                prjId ="38",
                sampGrpId = "161",
                tpcId="1519"
            };
            Task<ExternalMessage<Message>> topicInfo = Import.GetMessageInfo(param);
            topicInfo.Wait();
            var test = topicInfo.Result;
        }
      
        [TestMethod()]
        public void 测试message全局过滤功能()
        {
            MessageBLL.RemoveBy("1","-1","-1");
            var count1 = MessageBLL.GetMessages("1", "-1", "-1");
            Assert.AreEqual(count1.Count,0);
            MessageBLL.AddItem(new Message
            {
                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "1",
                tpcId = "1",
                msgCntnt = "昨晚华硕品牌日下的单，今天早上收到货，快递速度就不用说了，电脑?质量的话有两点我感觉不太喜欢，一是风扇的声音感觉稍微有点大，声音大说明风扇转速快，可是散热感觉也是呵呵呵！！！！！其二是屏幕分辨率，电脑配置显示是1920*1080，"
            });
            MessageBLL.AddItem(new Message
            {
                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "1",
                tpcId = "2",
                msgCntnt = "因为感觉配置与价钱性价比很高，今天早上收到货，快递速度就不用说了，电脑?质量的话有两点我感觉不太喜欢，一是风扇的声音感觉稍微有点大，声音大说明风扇转速快，可是散热感觉也是呵呵呵！！！！！其二是屏幕分辨率，电脑配置显示是1920*1080，"
            });
            MessageBLL.AddItem(new Message
            {
                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "2",
                tpcId = "21",
                msgCntnt = "昨晚华硕品牌日下的单，因为感觉配置与价钱性价比很高，，快递速度就不用说了，电脑?质量的话有两点我感觉不太喜欢，一是风扇的声音感觉稍微有点大，声音大说明风扇转速快，可是散热感觉也是呵呵呵！！！！！其二是屏幕分辨率，电脑配置显示是1920*1080，"
            });
            MessageBLL.AddItem(new Message
            {
                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "2",
                tpcId = "22",
                msgCntnt = "因为感觉配置与价钱性价比很高，今天早上收到货，快递速度就不用说了，电脑?质量的话有两点我感觉不太喜欢，一是风扇的声音感觉稍微有点大，声音大说明风扇转速快，可是散热感觉也是呵呵呵！！！！！其二是屏幕分辨率，电脑配置显示是1920*1080，"
            });
            MessageBLL.AddItem(new Message
            {
                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "3",
                tpcId = "31",
                msgCntnt = "家里一直用的金号毛巾，很柔软，吸水很好，还很便宜"
            });
            MessageBLL.AddItem(new Message
            {
                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "3",
                tpcId = "32",
                msgCntnt = "毛巾颜色漂亮，还有赠品，材质厚而柔软，用来洗脸很舒服，满意。"
            });
            MessageBLL.AddItem(new Message
            {
                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "3",
                tpcId = "33",
                msgCntnt = "价格很不错 吸水好价格便宜厚度适中图案很好看 手感很好。"
            });
        
         var count = MessageBLL.GetMessages("1", "-1", "-1");
            Assert.AreEqual(count.Count, 7);
            var count2 = MessageBLL.GetMessages("1", "2", "-1");
            Assert.AreEqual(count2.Count,2);
            var count3 = MessageBLL.GetMessages("1", "3", "-1");
            Assert.AreEqual(count3.Count, 3);
        }
    
    }
}