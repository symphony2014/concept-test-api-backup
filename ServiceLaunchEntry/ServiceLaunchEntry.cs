﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks; 
using ConceptCommon.Interface ;
using MappingServeice.Base;

namespace MappingServeice
{
    public partial class ServiceLaunchEntry : MappingServiceBase
    {
        
        public ServiceLaunchEntry()
        {
            InitializeComponent();
        }

        public void TestRun()
        {
            this.OnStartLogic(null);
        }

        protected void OnStartLogic(string[] args)
        {
            IServiceRun provider = this.GetServiceRunProviderFromConfig();
            provider.Process();
        }

        protected override void OnStart(string[] args)
        {
            //this.OnStartLogic(args);
            this.OnStartLogic(null);
        }


        ////for debug
        //public void OnStart(string[] args)
        //{
        //    this.OnStartLogic(args);
        //}
        protected override void OnStop()
        {
            //ServerRunState(WinSerSateType.StopRunning, ThirdPartyProcessorLanucher.strConfigName);
        }
          
    }
}
