﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace ServeName
{
    public class ServeNameConfigHelper
    {
        /// <summary>
        /// add by gaobinbin 2016/2/19 更改服务名称
        /// </summary>
        protected static string Get_ConfigValue(string configPath, string strKeyName)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(configPath);
            XmlNode AppSetingNodes = xmlDocument.SelectSingleNode(@"//configuration/appSettings/add[@key='IServiceRunTarget']");//获取AppSetting子节点集合 
            XmlAttribute valueAttribute = AppSetingNodes.Attributes["value"];
            if (valueAttribute == null)
            {
                return string.Empty;
            }
            return valueAttribute.Value;
        }

        public static string GetServiceName(string executeAssemblyPath)
        {
            if (string.IsNullOrEmpty(executeAssemblyPath))
            {
                throw new ArgumentNullException("executeAssemblyName");
            }

            string strServerName = string.Empty;

            string configSeverConfigpath = executeAssemblyPath + ".config";
            string strKeyName = "IServiceRunTarget";
            string SeverName = Get_ConfigValue(configSeverConfigpath, strKeyName);
            if (SeverName == string.Empty)
            {
                return string.Empty;
            }
            int nSeverName = SeverName.LastIndexOf(".");
            if (nSeverName >= 0)
            {
                strServerName = SeverName.Substring(nSeverName + 1);
            }

            return strServerName;
        }

    }
}
