﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Threading;
namespace ConceptCommon
{
    public sealed class IdHelper
    {
        #region Private Fields
        private static object obj = new object();
        private static long LAST_LONGID;
        private static string LAST_TIME = string.Empty;

        private static readonly string[] RANDOMBASECODE = {
                                                              "q", "w", "e", "r", "t",
                                                              //"o","i","1","0",
                                                              "y", "u", "p",
                                                              "a", "s", "d", "f", "g",
                                                              "h", "j", "k", "l", "z",
                                                              "x", "c", "v", "b", "n",
                                                              "m", "2", "3", "4",
                                                              "5", "6", "7", "8", "9"
                                                          };

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        /// 获取GUID的通用方法
        /// </summary>
        /// <returns></returns>
        public static string GetGuid()
        {
            return Guid.NewGuid().ToString();
        }

        /// <summary>
        /// 获取长整型ID方法
        /// 由年-月-日-小时-分-秒-毫秒-3位随机数组成
        /// 格式为：YYMMddhhmmssminrrr
        /// </summary>
        /// <returns></returns>
        public static long GetLongId()
        {
            DateTime now = DateTime.Now;
            string tmpDate = now.ToString("yyMMddHHmmss");

            string tmpmin = now.Millisecond.ToString("D3");
            //for (int i = tmpmin.Length; i < 3; i++) //补足3位
            //{
            //    tmpmin = "0" + tmpmin;
            //}

            string tmprrr = new Random().Next(0, 999).ToString("D3");
            //for (int i = tmprrr.Length; i < 3; i++) //补足3位
            //{
            //    tmprrr = "0" + tmprrr;
            //}

            return long.Parse(tmpDate + tmpmin + tmprrr);
        }

        public static long GetNewLongId()
        {
            lock (obj)
            {
                Thread.Sleep(1);
                DateTime now = DateTime.Now;
                string tmpDate = now.ToString("yyMMddHHmmssfff");


                //后面六位改为按每秒流水;
                if (tmpDate == LAST_TIME)
                {
                    LAST_LONGID++;
                    return LAST_LONGID;
                }
                else
                {
                    LAST_TIME = tmpDate;
                    LAST_LONGID = long.Parse(tmpDate + "001");
                    return LAST_LONGID;
                }
               
            }

            //string tmpmin = now.Millisecond.ToString().PadLeft(3,'0');
            /*
            for (int i = tmpmin.Length; i < 3; i++) //补足3位
            {
                tmpmin = "0" + tmpmin;
            }
            */
            //string tmprrr = new Random().Next(0, 999).ToString().PadLeft(3, '0'); 
            /*
            for (int i = tmprrr.Length; i < 3; i++) //补足3位
            {
                tmprrr = "0" + tmprrr;
            }
            */

            // long tmp = long.Parse(tmpDate + tmpmin + tmprrr);

            // if (tmp == LAST_LONGID)
            // {
            //     LAST_LONGID++;
            //     return LAST_LONGID;
            // }
            // else
            //  {
            //      LAST_LONGID = tmp;
            //    return tmp;
        }

        /// <summary>
        /// 获取随机字符串
        /// 由数字和字母组合
        /// 2012.08.02 By Tim
        /// </summary>
        /// <param name="length">所需字符串长度</param>
        /// <returns>生成字符串</returns>
        public static string GetRandomCode(int length)
        {
            var r = new Random(GetRandomSeed());
            var sb = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                int rnd = r.Next(0, 32);
                sb.Append(RANDOMBASECODE[rnd]);
            }
            return sb.ToString();
        }

        /// <summary>
        /// 获得随即函数种子
        /// 2012.08.02 By Tim
        /// </summary>
        /// <returns>Seed</returns>
        public static int GetRandomSeed()
        {
            var bytes = new byte[32];
            var rng = new RNGCryptoServiceProvider();
            rng.GetBytes(bytes);
            return BitConverter.ToInt32(bytes, 0);
        }

        #endregion Public Methods

        #region Private Methods

        #endregion Private Methods
    }
}
