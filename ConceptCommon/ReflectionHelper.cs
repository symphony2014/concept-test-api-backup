﻿using ConceptCommon.ReturnMode.CustomAttriBase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text; 
using System.Threading.Tasks; 

namespace ConceptCommon
{
    public class ReflectionHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="OUT"></typeparam>
        /// <param name="instance"></param>
        /// <param name="propertyName"></param>
        /// <returns>对于某些指定的类型，如果无法匹配实际的值得类型，可能会返回exception.</returns>
        public static OUT GetPropertyValueByName<T, OUT>(T instance, string propertyName) where T : class, new()
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentNullException("propertyName");
            }

            if (instance == null)
            {
                throw new ArgumentNullException("instance");
            }

            Type currentType = typeof(T);
            OUT result = GetPropertyValueByName<T, OUT>(instance, currentType, propertyName);
            return result;
        }

        /// <summary>
        /// 最原始的反射取值函数。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="OUT"></typeparam>
        /// <param name="instance"></param>
        /// <param name="instanceType"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static OUT GetPropertyValueByName<T, OUT>(T instance, Type instanceType, string propertyName) where T : class, new()
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentNullException("propertyName");
            }

            if (instance == null)
            {
                throw new ArgumentNullException("instance");
            }

            if (instanceType == null)
            {
                throw new ArgumentNullException("instanceType");
            }

            PropertyInfo property = instanceType.GetProperty(propertyName);
            if (property == null)
            {
                return default(OUT);
            }

            object propertyValue = property.GetValue(instance,null);
            OUT result;
            try
            {
                // 可能会报错。
                result = (OUT)propertyValue;
            }
            catch (Exception)
            {
                result = default(OUT);
            }

            return result;
        }

        public static OUT GetPropertyValueByIgnoreCaseName<T, OUT>(T instance, PropertyInfo[] proteryInfos, string propertyName) where T : class, new()
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentNullException("propertyName");
            }

            if (instance == null)
            {
                throw new ArgumentNullException("instance");
            }

            if (proteryInfos == null || proteryInfos.Length == 0)
            {
                throw new ArgumentNullException("proteryInfos");
            }

            var matchProperty = proteryInfos.Where<PropertyInfo>(item => item.Name.Equals(propertyName, StringComparison.OrdinalIgnoreCase));
            PropertyInfo property = null;
            if (matchProperty.Count() > 0)
            {
                property = matchProperty.FirstOrDefault<PropertyInfo>();
            }

            if (property == null)
            {
                return default(OUT);
            }

            object propertyValue = property.GetValue(instance,null);
            OUT result;
            try
            {
                // 可能会报错。
                result = (OUT)propertyValue;
            }
            catch (Exception)
            {
                result = default(OUT);
            }

            return result;
        }

        public static T GetInstanceByTypeName<T>(string typeName, string dllName = null) where T : class
        {
            object targetObj = GetInstanceByTypeName(typeName, dllName);

            T result = targetObj as T;

            return result ?? default(T);
        }

        public static object GetInstanceByTypeName(string typeName, string dllName = null)
        {
            if (string.IsNullOrEmpty(typeName))
            {
                throw new ArgumentNullException("typeName");
            }

            Assembly callIngAssembly = Assembly.GetCallingAssembly();

            var matchTypes = from typeItem in callIngAssembly.GetTypes()
                             where typeItem.FullName.Equals(typeName)
                             select typeItem;

            Type expectedType = null;
            if (matchTypes != null && matchTypes.Count() > 0)
            {
                expectedType = matchTypes.FirstOrDefault<Type>();
            }
            else
            {
                // 如果无法直接获取，从指定的dll path中寻找
                if (string.IsNullOrEmpty(dllName))
                {
                    return null;
                }

                string dllpath = Path.GetDirectoryName(callIngAssembly.CodeBase);
                dllpath = CombineDllPath(dllpath, dllName);
                Assembly loadAssembly = Assembly.LoadFrom(dllpath);
                expectedType = loadAssembly.GetType(typeName, true);
            }

            object targetObj = Activator.CreateInstance(expectedType);
            return targetObj;
        }

        public static string CombineDllPath(string dllPath, string dllName)
        {
            if (string.IsNullOrEmpty(dllPath))
            {
                throw new ArgumentNullException("dllPath");
            }

            if (string.IsNullOrEmpty(dllName))
            {
                throw new ArgumentNullException("dllName");
            }

            int removeStartIndex = dllPath.IndexOf(@"file:\");
            if (removeStartIndex == 0)
            {
                dllPath = dllPath.Remove(0, @"file:\".Length);
            }

            if (dllPath.EndsWith(@"/"))
            {
                dllPath = dllPath.Remove(dllPath.Length, 1);
            }

            dllPath = string.Concat(dllPath, @"\", dllName);
            return dllPath;
        }

        /// <summary>
        /// 获取当前Type的 custom attribute中的 指定属性值。
        /// </summary>
        /// <typeparam name="OUT"></typeparam>
        /// <typeparam name="Attr"></typeparam>
        /// <param name="currentInstanceType"></param>
        /// <param name="customAttributeName"></param>
        /// <param name="propertyNameUnderAttribute"></param>
        /// <returns></returns>
        public static OUT GetPropertyFromCustomAttribute<OUT, Attr>(Type currentInstanceType, string propertyNameUnderAttribute)
         where Attr : ReturnMode.CustomAttriBase.CustomAttriBase
        {
            if (string.IsNullOrEmpty(propertyNameUnderAttribute))
            {
                throw new ArgumentNullException("customAttributeName");
            }

            if (currentInstanceType == null)
            {
                throw new ArgumentNullException("currentInstanceType");
            }

            Attr serviceInfo = currentInstanceType.GetCustomAttributes(typeof(Attr), false).FirstOrDefault() as Attr; //currentInstanceType.GetCustomAttribute<Attr>();
            if (serviceInfo.Equals(default(OUT)))
            {
                throw new InvalidOperationException(string.Format("无法为[{0}]类型获取[{1}]Attribute", currentInstanceType.Name, typeof(Attr).Name));
            }

            // 从Attr 指定的Custom attribute中 获取指定属性的值。
            OUT result = GetValueFromCustomAttributeInstance<OUT>(serviceInfo, propertyNameUnderAttribute);
            return result;
        }

        protected static OUT GetValueFromCustomAttributeInstance<OUT>(CustomAttriBase customAttribute, string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentNullException("propertyName");
            }

            if (customAttribute == null)
            {
                throw new ArgumentNullException("customAttribute");
            }

            Type currentType = customAttribute.GetType();
            PropertyInfo property = currentType.GetProperty(propertyName);
            if (property == null)
            {
                return default(OUT);
            }

            object propertyValue = property.GetValue(customAttribute,null);
            OUT result;
            try
            {
                // 可能会报错。
                result = (OUT)propertyValue;
            }
            catch (Exception)
            {
                result = default(OUT);
            }

            return result;

        }

        protected PropertyInfo GetClassInstance(Type type)
        {
            if (type == null)
            {
                throw new ArgumentException("ReflectionHelperGet/type 为空");
            }

            PropertyInfo Instance = type.GetProperty("Instance", BindingFlags.Static);
            if (Instance == null)
            {
                throw new ArgumentException("ReflectionHelperGet/Instance 为空");
            }

            return Instance;

        }

        public static Type[] GetInterfaceImplmentations<T>(string dllName = null) where T : class
        {
            Type interfaceType = typeof(T);
            string interfaceName = interfaceType.Name;
            if (!interfaceType.IsInterface)
            {
                throw new InvalidOperationException(string.Format("当前Type[{0}]不是interface类型", interfaceName));
            }

            if (string.IsNullOrEmpty(interfaceName))
            {
                throw new ArgumentNullException("interfaceName");
            }

            Assembly currentAssembly = Assembly.GetExecutingAssembly();
            Type[] currentTypes = currentAssembly.GetTypes();
            if (currentTypes == null || currentTypes.Length == 0)
            {
                throw new ArgumentNullException("无法获得当前运行Assembly");
            }

            Type[] implementations = GetInterfaceImplmentationsByName(interfaceName, currentAssembly);

            if (implementations.Count() > 0)
            {
                return implementations.ToArray<Type>();
            }

            // 如果当前Assembly无法获取，且不指定dll name, throw exception.
            if (string.IsNullOrEmpty(dllName))
            {
                throw new InvalidOperationException(string.Format(@"无法获得[{0}]接口的实现", interfaceName));
            }

            // 如果无法直接获取，从指定的dll path中 load Assembly进行查找。
            string dllpath = Path.GetDirectoryName(currentAssembly.CodeBase);
            dllpath = CombineDllPath(dllpath, dllName);
            currentAssembly = Assembly.LoadFrom(dllpath);

            // 重新查找
            implementations = GetInterfaceImplmentationsByName(interfaceName, currentAssembly);

            // 找不到则直接抛出异常
            if (implementations.Count() == 0)
            {
                throw new InvalidOperationException(string.Format(@"无法获得[{0}]接口的实现", interfaceName));
            }

            return implementations.ToArray<Type>();
        }

        protected static Type[] GetInterfaceImplmentationsByName(string interfaceName, Assembly targetAssembly)
        {
            if (string.IsNullOrEmpty(interfaceName))
            {
                throw new ArgumentNullException("interfaceName");
            }

            if (targetAssembly == null)
            {
                throw new ArgumentNullException("targetAssembly");
            }

            Type[] currentTypes = targetAssembly.GetTypes();
            if (currentTypes == null || currentTypes.Length == 0)
            {
                throw new ArgumentNullException("无法获得当前运行Assembly");
            }

            // 获取实现了指定的接口名字，且不是Abstract
            var implemtations = currentTypes.Where<Type>(item =>
            {
                // IParseHDResult`1 表示实现了 IParseHDResult<T>
                return item.GetInterface(interfaceName, false) != null && !item.IsAbstract;
            });

            return implemtations.ToArray<Type>();
        }

        public static OUT GetValueFromAttribute<OUT, Attr>(Type currentChildType, string propertyNameUnderAttribute) where Attr : Attribute
        {
            if (currentChildType == null)
            {
                throw new ArgumentNullException("currentChildType");
            }

            if (string.IsNullOrEmpty(propertyNameUnderAttribute))
            {
                throw new ArgumentNullException("propertyNameUnderAttribute");
            }

            var attrInstance = currentChildType.GetCustomAttributes(typeof(Attr), false).FirstOrDefault() as Attr;  
            if (attrInstance == null)
            {
                throw new InvalidOperationException("无法获得ServiceInfo.ServiceNum定义。");
            }

            OUT result = GetValueFromAttrByPropertyName<OUT, Attr>(attrInstance, propertyNameUnderAttribute);

            return result;
        }

        protected static OUT GetValueFromAttrByPropertyName<OUT, Attr>(Attr customAttribute, string propertyName) where Attr : Attribute
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentNullException("propertyName");
            }

            if (customAttribute == null)
            {
                throw new ArgumentNullException("customAttribute");
            }

            Type currentType = customAttribute.GetType();
            PropertyInfo property = currentType.GetProperty(propertyName);
            if (property == null)
            {
                return default(OUT);
            }

            object propertyValue = property.GetValue(customAttribute,null);
            OUT result;
            try
            {
                // 可能会报错。
                result = (OUT)propertyValue;
            }
            catch (Exception)
            {
                result = default(OUT);
            }

            return result;

        }

        public static string GetExecutingPath(Assembly currentExecutingAssembly)
        {
            if (currentExecutingAssembly == null)
            {
                throw new ArgumentNullException("currentExecutingAssembly");
            }

            // 获得执行Assembly的 path, 也就是dll的path.
            string dllpath = Path.GetDirectoryName(currentExecutingAssembly.CodeBase);
            string fakeDllName = "test.dll";
            dllpath = CombineDllPath(dllpath, fakeDllName);
            dllpath = dllpath.Replace(fakeDllName, string.Empty);
            return dllpath;
        }
    }
}
