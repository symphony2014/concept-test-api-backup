﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCommon.ParaMode
{
    public class PGExternalRuleResult
    {
        public string callSampGrpId { get; set; }
        public string ruleId { get; set; }
        public string userId { get; set; }
        public string sampGrpId { get; set; }
        public string prjId { get; set; } 
    }
}
