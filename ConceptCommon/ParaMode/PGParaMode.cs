﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCommon.ParaMode
{
    public class PGParaMode
    {
        public string ExternalPjId { get; set; }
        public string GroupId { get; set; }  
    }

    public class PGAnalysisParaMode: PGParaMode
    {
        public string StartTopicId { get; set; }
        public string EndTopicId { get; set; }
        public List<string> SrvIdList { get; set; }
        public bool CloseAllTopic { get; set; }
    }
}
