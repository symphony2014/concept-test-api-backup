﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCommon.WinSerState
{
    public enum WinSerSateType
    {
        StartRunning = 0,
        IsRunning = 1,
        StopRunning = 2
    }
}
