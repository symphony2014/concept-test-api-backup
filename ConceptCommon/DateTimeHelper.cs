﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ConceptCommon
{
    public class DateTimeHelper
    {
        //数据库中  无效时间存储
        private static DateTime _emptyDateTime = new DateTime(1753, 1, 1);
        public static DateTime EmptyDateTime
        {
            get
            {
                return _emptyDateTime;
            }
        }

        private static DateTime _captureDbBeginTime = new DateTime(2015, 12, 1);
        public static DateTime CaptureDbBeginTime
        {
            get
            {
                return _captureDbBeginTime;
            }
        }

        private static IFormatProvider ifp = new CultureInfo("zh-CN", true);

        public static bool TryParseDateTimeWithPattern(string raw, string pattern, out DateTime result)
        {
            if (string.IsNullOrEmpty(raw))
            {
                throw new ArgumentNullException("raw");
            }

            return DateTime.TryParseExact(raw, pattern, ifp, DateTimeStyles.None, out result);
        }

        public static DateTime GetNextTime(DateTime dateTime, int interval, int intervalUnit)
        {
            DateTime nextTime = new DateTime();
            switch (intervalUnit)
            {
                case 0:
                    nextTime = dateTime.AddHours(interval);
                    break;
                case 1:
                    nextTime = dateTime.AddDays(interval);
                    break;
                case 2:
                    nextTime = dateTime.AddDays(interval * 7);
                    break;
                case 3:
                    nextTime = dateTime.AddMonths(interval);
                    break;
                default: break;
            }
            return nextTime;
        }
    }
}
