﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCommon.Interface 
{
    public interface IServiceRun
    {
        void Process();
    }

    public interface IServiceParameterRun
    {
        void ProcessWithParas(params object[] paramets);
    }
}
