﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCommon.Interface
{
    public interface IConvertJson
    {
        string SerializeObject(object expectedConvertObject);
        T DeserializeObject<T>(string jsonStr);
    }
}
