﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCommon.Interface 
{
    public class ConvertJsonProvider : IConvertJson
    {
        #region 单例设定

        private static object lockObject = new object();
        protected ConvertJsonProvider()
        {

        }

        private static volatile ConvertJsonProvider _instance;
        public static ConvertJsonProvider Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        if (_instance == null)
                        {
                            _instance = new ConvertJsonProvider();
                            return _instance;
                        }
                    }
                }

                return _instance;
            }
        }

        #endregion 单例设定

        public virtual string SerializeObject(object expectedConvertObject)
        {
            if (expectedConvertObject == null)
            {
                throw new ArgumentNullException("expectedConvertObject");
            }

            string jsonStr = JsonConvert.SerializeObject(expectedConvertObject);
            return jsonStr;
        }

        public virtual T DeserializeObject<T>(string jsonStr)
        {
            if (string.IsNullOrEmpty(jsonStr))
            {
                throw new ArgumentNullException("jsonStr");
            }

            T convertedObj = JsonConvert.DeserializeObject<T>(jsonStr);
            return convertedObj;
        }
    }
}
