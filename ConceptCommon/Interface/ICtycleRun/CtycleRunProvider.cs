﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ConceptCommon.Interface 
{
    public class CtycleRunProvider : ICtycleRun
    {
        public void CtycleRun(IServiceRun serviceProvider, TimeSpan ctyleinterval, int? times)
        {
            if (!times.HasValue)
            {
                times = -1;
            }

            CtycleRunProviderParameter parameter = new CtycleRunProviderParameter()
            {
                ServiceProvider = serviceProvider,
                Ctyleinterval = ctyleinterval,
                Times = times.HasValue ? times.Value : -1
            };

            this.CtycleRunLogic(parameter);
        }

        protected void CtycleRunLogic(CtycleRunProviderParameter ctycleRunParameter)
        {
            if (ctycleRunParameter == null)
            {
                throw new ArgumentNullException("ctycleRunParameter");
            }

            if (ctycleRunParameter.ServiceProvider == null)
            {
                throw new ArgumentException("ctycleRunParameter should contains IServiceRun instance");
            }

            if (ctycleRunParameter.Ctyleinterval.TotalSeconds <= 0)
            {
                return;
            }

            Thread ctycleThread = new Thread(new ParameterizedThreadStart(CtycleRunThreadMethod));
            ctycleThread.IsBackground = false;
            ctycleThread.Start(ctycleRunParameter);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="ctyleinterval"></param>
        /// <param name="times">-1 means no times limited</param>
        protected virtual void CtycleRunThreadMethod(object ctycleRunParameter)
        {
            Type currentType = ctycleRunParameter.GetType();
            if (!currentType.Equals(typeof(CtycleRunProviderParameter)))
            {
                return;
            }

            CtycleRunProviderParameter currentParameter = ctycleRunParameter as CtycleRunProviderParameter;
            bool isLoop = true;


            int timesValue = currentParameter.Times;
            while (isLoop)
            {
                DateTime performStart = DateTime.Now;

                try
                {
                    currentParameter.ServiceProvider.Process();
                }
                catch (Exception ex)
                {
                    throw ex; 
                    //this.WriteLogProvider.WritreLogExption(ex, string.Format(@"CtycleRunProvider/Calling[{0}]IServiceRun.", currentParameter.ServiceProvider.GetType().FullName));
                }

                if (timesValue > 0)
                {
                    timesValue--;
                }

                if (timesValue == 0)
                {
                    isLoop = false;
                }

                DateTime perfortEndTime = DateTime.Now;
                TimeSpan duration = perfortEndTime - performStart;

                // 如果执行时间小于最小间隔， 计算出剩余间隔时间。
                // 如果执行时间大于最小间隔， 则直接运行下一次。
                if (duration < currentParameter.Ctyleinterval)
                {
                    TimeSpan actualCtyleinterval = currentParameter.Ctyleinterval - duration;

                    // Sleep until the elapsed time reach the specified interval.
                    Thread.Sleep(actualCtyleinterval);
                }
            }
        }
    }

    public class CtycleRunProviderParameter
    {
        public IServiceRun ServiceProvider { get; set; }

        public TimeSpan Ctyleinterval { get; set; }

        public int Times { get; set; }
    }
}
