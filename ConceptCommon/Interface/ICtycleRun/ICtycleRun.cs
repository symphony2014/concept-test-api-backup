﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCommon.Interface 
{
    public interface ICtycleRun
    {
        /// <summary>
        /// Perform IServiceRun by specified ctyleinterval
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="ctyleinterval">the interval value, Mintues as units.</param>
        /// <param name="times">How many times execute. If set to null, there are no times limited. </param>
        void CtycleRun(IServiceRun serviceProvider, TimeSpan ctyleinterval, int? times);
    }
}
