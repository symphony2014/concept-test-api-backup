﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ConceptCommon.Interface.ServiceRunner
{
    public abstract class ServiceRunner<T>
    {
        //protected IWriteLogMess WriteLogProvider
        //{
        //    get
        //    {
        //        return WriteLogMess.Instance;
        //    }
        //}

        private ISettingRun _settingRunProvider = null;
        protected ISettingRun SettingRunProvider
        {
            get
            {
                if (this._settingRunProvider == null)
                {
                    this._settingRunProvider = this.GetSettingRunProvider();
                }

                return this._settingRunProvider;
            }
        }

        public static string strConfigName = string.Empty;

        public void Run()
        {
            // Load all enable config items.
            // 按照配置表的条数开启线程。目前是一个thirdparty 开启一个线程。
            // 每个thirdparty 内的并发线程数以及间隔时间由ZxReport_ThirdPartyTaskConfig 各自的配置决定。
            List<T> thirdPartyProcessorConfigItems = this.LoadExpectedRunConfigurationItems();
            if (thirdPartyProcessorConfigItems == null || thirdPartyProcessorConfigItems.Count == 0)
            {
                return;
            }

            // 根据Expected run 的config items，启动ISer
            foreach (var item in thirdPartyProcessorConfigItems)
            {
                // 获得实现名字
                string implementationName = this.GetProcessImplementationInfo(item);

                // 获得实现的显示名
                string processorName = this.GetProcessorDisplayName(item);
                if (string.IsNullOrEmpty(implementationName))
                {
                    continue;
                }

                try
                {
                    IServiceRun currentProcessor = this.LoadServiceRunInstance(implementationName);
                    if (currentProcessor == null)
                    {
                        continue;
                    }

                    // 启动处理器，
                    this.LaunchProcessByConfigItem(item, currentProcessor);

                    //int interval = item.RunInterval;
                    //if (interval <= 0)
                    //{
                    //    interval = 1000; //1000毫秒=1秒
                    //}

                    //// 开启一个间隔run 的线程。
                    //this.SettingRunProvider.CtycleRun(currentProcessor, new TimeSpan(0, 0, 0, 0, interval), null);
                }
                catch (Exception ex)
                {
                    throw ex;
                   // this.WriteLogProvider.WritreLogExption(ex, string.Format(@"ServiceRunner: 开启[{0}]processor出错", string.IsNullOrEmpty(processorName) ? "Unknown" : processorName));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceItems"></param>
        /// <returns>如果配置成ALL, 则返回true,如果配置成分散列表，返回false</returns>
        protected bool VerifyDeployServiceItemsIsAll(out string[] serviceItems)
        {
            string deployConfigValue = ConfigurationManager.AppSettings[this.RunItemSettingConfigName];

            if (string.IsNullOrEmpty(deployConfigValue))
            {
                strConfigName = string.Empty;
            }
            else
            {
                strConfigName = deployConfigValue;
            }
            if (string.IsNullOrEmpty(deployConfigValue) || "All".Equals(deployConfigValue, StringComparison.OrdinalIgnoreCase))
            {
                serviceItems = new string[] { };
                return true;
            }

            serviceItems = deployConfigValue.Split(new string[] { @";" }, StringSplitOptions.RemoveEmptyEntries);
            return false; ;
        }

        protected virtual ISettingRun GetSettingRunProvider()
        {
            // 每次new一个对象， 每个new的对象都对该对象线程安全。
            return new SettingRunProvider();
        }

        protected virtual IServiceRun LoadServiceRunInstance(string implementationName)
        {
            if (string.IsNullOrEmpty(implementationName))
            {
                throw new ArgumentNullException("implementationName");
            }

            string iserviceRunImplementDll = this.IServiceRunImplementDllName;
            IServiceRun result = ReflectionHelper.GetInstanceByTypeName<IServiceRun>(implementationName, iserviceRunImplementDll);
            return result;
        }

        protected abstract List<T> LoadExpectedRunConfigurationItems();

        /// <summary>
        /// 根据配置，选择不同的Run方式， 去跑IServiceRun 的实现。
        /// </summary>
        /// <param name="configItem"></param>
        /// <param name="serviceRunImplementItem"></param>
        protected abstract void LaunchProcessByConfigItem(T configItem, IServiceRun serviceRunImplementItem);

        protected abstract string GetProcessImplementationInfo(T configItem);

        protected abstract string GetProcessorDisplayName(T configItem);

        protected abstract string IServiceRunImplementDllName { get; }

        /// <summary>
        /// The name of the config which indicate what item need to launch.
        /// 哪些Service Item 需要跑起来
        /// </summary>
        protected abstract string RunItemSettingConfigName { get; }
    }
}
