﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCommon.Interface
{
    public interface ISettingRun : ICtycleRun
    {
        void SettingRun(IServiceRun serviceProvider);

        void SettingRun(IServiceRun serviceProvider, DateTime specifiedTime);
    }
}
