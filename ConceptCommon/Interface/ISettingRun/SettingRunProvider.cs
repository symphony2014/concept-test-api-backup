﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCommon.Interface 
{
    using ConceptCommon.Interface;
    using Timers = System.Timers;

    /// <summary>
    /// 线程安全针对某个实例有效。
    /// </summary>
    public class SettingRunProvider : CtycleRunProvider, ISettingRun
    {
        protected object lockerOfVerifyRunning = new object();

        protected bool _isRunning = false;

        protected bool IsRunning
        {
            get
            {
                lock (lockerOfVerifyRunning)
                {
                    return _isRunning;
                }
            }

            set
            {
                lock (lockerOfVerifyRunning)
                {
                    _isRunning = value;
                }
            }

        }

        private DateTime _serviceRunStart;
        protected DateTime ServiceRunStart
        {
            get
            {
                if (this._serviceRunStart.Equals(default(DateTime)) || this._serviceRunStart.Equals(DateTime.MaxValue))
                {
                    // 默认取得Time的方法。
                    this._serviceRunStart = this.GetConfigStartTime();
                }

                return this._serviceRunStart;
                //return this.GetConfigStartTime();
            }
        }

        protected IServiceRun ServiceProvider { get; set; }

        protected void SettingStartTime(DateTime expectedStartTime)
        {
            if (expectedStartTime.Equals(default(DateTime)) || expectedStartTime.Equals(DateTime.MaxValue))
            {
                throw new ArgumentNullException("expectedStartTime");
            }

            this._serviceRunStart = expectedStartTime;
        }

        /// <summary>
        /// 从配置文件获取start时间
        /// </summary>
        /// <param name="serviceProvider"></param>
        public void SettingRun(IServiceRun serviceProvider)
        {
            if (serviceProvider == null)
            {
                throw new ArgumentNullException("serviceProvider");
            }

            this.ServiceProvider = serviceProvider;
            DateTime expectedStart = this.GetConfigStartTime();

            // 设置开始时间
            this.SettingStartTime(expectedStart);

            // Setting timer.
            Timers.Timer timer = this.SettingTimer();
            timer.Elapsed += serviceTimer_Elapsed;
        }

        /// <summary>
        /// 从输入参数中获取start时间
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="specifiedTime">指定Service start的时刻， 计算中只只用其中的Hour and minutes设置。 建议用"HH:mm" 字符转换成DateTime.</param>
        public void SettingRun(IServiceRun serviceProvider, DateTime specifiedTime)
        {
            if (serviceProvider == null)
            {
                throw new ArgumentNullException("serviceProvider");
            }

            this.ServiceProvider = serviceProvider;

            // 设置开始时间
            this.SettingStartTime(specifiedTime);

            // Setting timer.
            Timers.Timer timer = this.SettingTimer();
            timer.Elapsed += serviceTimer_Elapsed;
        }

        /// <summary>
        /// Timer异步线程的执行入口。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void serviceTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (this.IsRunning)
            {
                return;
            }

            if (this.IsRunProcess(this.ServiceRunStart, e.SignalTime))
            {
                this.IsRunning = true;
                try
                {
                    this.ServiceProvider.Process();
                }
                catch (Exception ex)
                {
                    //this.WriteLogProvider.WritreLogExption(ex, string.Format(@"SettingRunProvider/Calling[{0}]IServiceRun.", this.ServiceProvider.GetType().FullName));
                }

                this.IsRunning = false;
            }
        }

        protected virtual DateTime GetConfigStartTime()
        {
            string settingValue = ConfigHelper.GetConfigValueByName("ServiceStartTime");
            if (string.IsNullOrEmpty(settingValue))
            {
                return default(DateTime);
            }

            DateTime result;
            if (!DateTimeHelper.TryParseDateTimeWithPattern(settingValue, "HH:mm", out result))
            {
                return default(DateTime);
            }

            return result;
        }

        protected virtual bool IsRunProcess(DateTime settingTime, DateTime currentTime)
        {

            if (settingTime.Equals(default(DateTime)))
            {
                return false;
            }

            int hour = settingTime.Hour;
            int minutes = settingTime.Minute;

            // 设置值的精度为分钟。如果需要改变采样率，请确保采样率的精度高于或者等价于设置值的精度。
            return currentTime.Hour == hour && currentTime.Minute == minutes;
        }

        protected virtual Timers.Timer SettingTimer()
        {
            Timers.Timer timer = new Timers.Timer()
            {

                AutoReset = true,
                Enabled = true,
                Interval = 60000 //一分钟一次
            };

            return timer;
        }
    }
}
