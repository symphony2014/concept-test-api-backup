﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCommon
{
    public class MapGetUtil
    {
        /// <summary>
        /// get value from "?openid=333&groupId=333"
        /// </summary>
        /// <param name="appendData"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public static string MapGet(string appendData, string v)
        {
            var pair = appendData.Split('&');
            foreach (var item in pair)
            {
                if (item.ToLower().Trim().IndexOf(v.ToLower().Trim()) > -1)
                {
                    var value = item.Split('=');
                    return value[1];
                }
            }
            return string.Empty;
        }

    }
}
