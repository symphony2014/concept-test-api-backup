﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCommon.Model
{
    public enum ServiceRunningType
    {
        Default = -1,

        /// <summary>
        /// 指定在某个时刻跑
        /// </summary>
        SettingRun = 1,

        /// <summary>
        /// 间隔的循环跑，按照指定的间隔TimeSpan
        /// </summary>
        CtyleRun = 2
    }
}
