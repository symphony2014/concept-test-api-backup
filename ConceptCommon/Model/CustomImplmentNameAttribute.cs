﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCommon.Model
{
    public class CustomImplmentNameAttribute : Attribute
    {
        public string Implement { get; set; }
        public bool IsEnable { get; set; }
    }
}
