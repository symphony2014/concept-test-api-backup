﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCommon.ReturnMode
{
    public class PGReturn
    {
        //"200",#响应码
        public int statusCode { get; set; }
        //#错误信息
        public string errMsg { get; set; }
        //#返回数据
        public string data { get; set; }
    }
}
