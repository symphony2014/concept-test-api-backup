﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCommon.ReturnMode
{
    public class VoidReturn
    {
        public int Result { get; set; }
        public string Message { get; set; }
    }

    public class NormalReturn<T> : VoidReturn where T : class
    {
        public T data { get; set; }
    }

    public class ValueTypeNormalReturn<T> : VoidReturn where T : struct
    {
        public T data { get; set; }
    }

    public class NormalListReturn<T> : VoidReturn where T : class, new()
    {
        public List<T> data { get; set; }
        public int TotalCount { get; set; }
    }

    public class NormalListReturnType<T> : VoidReturn where T : class, new()
    {
        public List<T> data { get; set; }
        public int nState { get; set; }
        public string ErrMessage { get; set; }
    }


    public class NormalReturnType<T> : VoidReturn where T : class, new()
    {
        public T data { get; set; }
        public int nState { get; set; }
        public string ErrMessage { get; set; }
    }

    public class NormalListWithHeaderReturn<T> : NormalListReturn<T> where T : class, new()
    {
        public List<string> header { get; set; }
    }
}
