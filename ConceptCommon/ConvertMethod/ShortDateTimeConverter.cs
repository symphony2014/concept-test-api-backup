﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace ConceptCommon.ConvertMethod
{
    public class ShortDateTimeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var value = reader.Value;
            if (value == null)
            {
                return null;
            }
            else
            {
                return System.Convert.ToDateTime(value);
            }

        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var item = (DateTime)value;
            writer.WriteValue(item.ToString("yyyy-MM-dd"));
            writer.Flush();
        }
    } 
}
