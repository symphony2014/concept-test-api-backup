﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace ConceptCommon.ConvertMethod
{
    public class EnumConverter : JsonConverter
    {

        public override bool CanConvert(Type objectType)
        {
            return typeof(Enum).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
                return null;
            return Enum.Parse(objectType, reader.Value.ToString());
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var item = (Enum)value;
            writer.WriteValue(item.ToString());
            writer.Flush();
        }
    }
}
