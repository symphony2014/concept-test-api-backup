﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCommon.ConvertMethod
{
    public class JsonStrConverter
    {
        public static string GetJsonStr(object any)
        {
            return JsonConvert.SerializeObject(any);
        }
    }
}
