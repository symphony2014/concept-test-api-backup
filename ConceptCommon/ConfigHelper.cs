﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ConceptCommon
{
    public class ConfigHelper
    {
        public static string GetConfigValueByName(string configName)
        {
            if (string.IsNullOrEmpty(configName))
            {
                throw new ArgumentNullException("configName");
            }

            string result = ConfigurationManager.AppSettings.Get(configName);
            return string.IsNullOrEmpty(result) ? "" : result;
        }

    }
}
