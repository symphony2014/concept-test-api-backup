﻿
using Concept.Entity.BaseDB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


using System.Web;
using Newtonsoft.Json.Linq;
using Concept.Entity.External;
using ConceptCommon;
using Concept.BLL.BaseBD;
using Concept.BLL.BaseBD.Log;

namespace ExternalAPI
{
 
    /// <summary>
    /// 处理外部接口方法
    /// </summary>
    public class ImportMock
    {
        static HttpClient  client = new HttpClient();
        static List<Message> messages = new List<Message>()
        {
            new Message
            {
                  
                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "1",
                tpcId = "1",
                msgCntnt = "昨晚华硕品牌日下的单，分推荐，很不错，感觉是那种制作果干技术，香脆可口，和一般的红枣比，别有一番风味，很是独特，也很是好吃！ 这个脆枣我也十分推荐，很不错，感觉是那种制作果干技术，香脆可口，和一般的红枣比，别有一番风味，很是独特，也很是好吃！ 这个香瓜子的表现也是中规中矩，奶油味的，和市面上的大多数包头装的香瓜子差不多，没有什么突出的地方，和现炒的比起来个头略小，而且有点硬。 这个香瓜子的表现也是中规中矩，奶油味的，和市面上的大多数包头装的香瓜子差不多，没有什么突出的地方，和现炒的比起来个头略小，而且有点硬。 共7张图片 收起详情 ︽ 今天早上收到货，快递速度就不用说了，电脑?质量的话有两点我感觉不太喜欢，一是风扇的声音感觉稍微有点大，声音大说明风扇转速快，可是散热感觉也是呵呵呵！！！！！其二是屏幕分辨率，电脑配置显示是1920*1080，" 
            },
              new Message
            {

                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "1",
                tpcId = "1",
                msgCntnt = "为最近人气极高的一个品牌，我总能在各大购物网站和影视剧中看到它的产品，自己也是它的一个客户，经常会去买一些他家的肉类和糖果零食，这次参加了试用，中到了一个大礼包，让我看到了它作为零食企业的雄厚实力！感觉不太喜欢，一是风扇的声音感觉稍微有点大，声音大说明风扇转速快，可是散热感觉也是呵呵呵！！！！！其二是屏幕分辨率，电脑配置显示是1920*1080，"

            },
                new Message
            {

                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "1",
                tpcId = "2",
                msgCntnt = "昨晚华硕品牌日下的单，今天早上收到货，快递速度就不用说了，电脑?质量的话有两点我感觉不太喜欢，一是风扇的声音感觉稍微有点大，声音大说明风扇转速快，可是散热感觉也是呵呵呵！！！！！其二是屏幕分辨率，电脑配置显示是1920*1080，"

            },
                  new Message
            {

                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "1",
                tpcId = "2",
                msgCntnt = "昨晚华硕品牌日下的 商品亮点及建议： 良品铺子 阖家欢年货礼盒2171g 14袋，太惊喜了！！！这个礼盒真的是太厉害了，收到打开真的是感动的泪流满面，各种吃的啊，简直是太完美了，坚果瓜子点心饼干果冻果干薯片等等等等~~~简直是超值，会回购会回购！！！良品铺子简直是良心之作啊，每样都超好吃，真的是吃了一大半才来写报告的！单，今天早上收到货，快递速度就不用说了，电脑?质量的话有两点我感觉不太喜欢，一是风扇的声音感觉稍微有点大，声音大说明风扇转速快，可是散热感觉也是呵呵呵！！！！！其二是屏幕分辨率，电脑配置显示是1920*1080，" 
            },
                    new Message
            {

                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "1",
                tpcId = "3",
                msgCntnt = "昨很喜欢这种包装，都是小份小份的，因为我女儿比较挑食，款式多总能让她挑上几件吧！晚华硕品牌日下的单，今天早上收到货，快递速度就不用说了，电脑?质量的话有两点我感觉不太喜欢，一是风扇的声音感觉稍微有点大，声音大说明风扇转速快，可是散热感觉也是呵呵呵！！！！！其二是屏幕分辨率，电脑配置显示是1920*1080，"

            },
                      new Message
            {

                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "1",
                tpcId = "3",
                msgCntnt = "一直在京东上面买东西，有活动的时候还是很实惠的，之前都是买京东自营的，这次买的这种店铺的，很好的体验，好大一箱送人很体面不太喜欢，一是风扇的声音感觉稍微有点大，声音大说明风扇转速快，可是散热感觉也是呵呵呵！！！！！其二是屏幕分辨率，电脑配置显示是1920*1080，"

            },
                        new Message
            {

                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "11",
                tpcId = "11",
                msgCntnt = "昨晚华硕品牌日下的单，今天早上收到货，快递速度就不用说了，电脑?质量的话有两点我感觉不太喜欢，一是风扇的声音感觉稍微有点大，声音大说明风扇转速快，可是散热感觉也是呵呵呵！！！！！其二是屏幕分辨率，电脑配置显示是1920*1080，"

            },
                          new Message
            {

                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "11",
                tpcId = "11",
                msgCntnt = "给你点赞 太实惠了 里面好多好吃的哦 很喜欢 而且京东物流太赞了 为你打call 而且我很幸运 抢到了 第二件一元 超级开心，"

            },
           new Message
            {

                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "11",
                tpcId = "12",
                msgCntnt = "外盒不错 满满一箱 看起来很实惠 送人也不错 自己吃味道也不错 但是 但是原价买真的不是特别值 都是小小一包 开包吃不了几口就没了 想吃还是买散装的吧，"

            },
              new Message
            {

                crtTm = DateTime.Now.ToString(),
                crtPsn = Guid.NewGuid().ToString(),
                ExternalPjId = "1",
                sampGrpId = "11",
                tpcId = "12",
                msgCntnt = "22日上午，全国个体劳动者第五次代表大会在京开幕。国务委员王勇在会上宣读了习近平的贺信和李克强的批示并讲话。他强调，要深入学习贯彻党的十九大精神和习近平总书记贺信的要求，以习近平新时代中国特色社会主义思想为指导，坚持新发展理念，加快推动个体私营经济转变发展方式、优化经济结构、转换增长动的声音感觉稍微有点大，声音大说明风扇转速快，可是散热感觉也是呵呵呵！！！！！其二是屏幕分辨率，电脑配置显示是1920*1080，"

            }
        };
         static ImportMock()
        {
            var externalAPI = ConfigurationManager.AppSettings["ExternalAPI"];
            client.BaseAddress = new Uri(externalAPI);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }
        /// <summary>
        /// 获取Group
        /// </summary>
        /// <param name="userInfos"></param>
        /// <returns></returns>
        public static ExternalMessage<eGroup> GetGroupInfo(ExternalParam<ExternalGroupParam> topicParam)
        {
            ExternalMessage<eGroup> data = new ExternalMessage<eGroup> {
                data=new data<eGroup> { rows=new List<eGroup>() { new eGroup { dscInf="test",id="1",sampNm="group1"} } }
            };
            //HttpResponseMessage response = client.PostAsJsonAsync(
            //    "sampgrp/get", topicParam).Result;
            //if (response.IsSuccessStatusCode)
            //{
            //    data = response.Content.ReadAsAsync<ExternalMessage<eGroup>>().Result;
            //}

            // return URI of the created resource.
            return data;
        }
        public static ExternalMessage<eTopic> GeteTopicInfo(ExternalParam<ExternaleTopicParam> externalParam)
        {
            ExternalMessage<eTopic> data = new ExternalMessage<eTopic>
            {
                data = new data<eTopic> { rows = new List<eTopic>() { new eTopic {dscInf="test",id="1",tpcNm="topic1"  } } }
            };
            //HttpResponseMessage response = client.PostAsJsonAsync(
            //    "sampgrp/get", topicParam).Result;
            //if (response.IsSuccessStatusCode)
            //{
            //    data = response.Content.ReadAsAsync<ExternalMessage<eGroup>>().Result;
            //}

            // return URI of the created resource.
            return data;
        }
        /// <summary>
        /// 获取聊天信息
        /// </summary>
        /// <param name="userInfos"></param>
        /// <returns></returns>
        public async static Task<ExternalMessage<Message>> GetTopicInfo(ExternalTopicParam topicParam)
        {
            ExternalMessage<Message> data = new ExternalMessage<Message> { data = new Concept.Entity.External.data<Message> { rows=new List<Message>()} };
            HttpResponseMessage response = await client.PostAsJsonAsync(
                "msg/get", topicParam);
            if (response.IsSuccessStatusCode)
            {
                data = await response.Content.ReadAsAsync<ExternalMessage<Message>>();
            }

            // return URI of the created resource.
             data = new ExternalMessage<Message> { data = new Concept.Entity.External.data<Message> { rows = new List<Message>() } };
            data.data.rows = messages.Where(m => m.ExternalPjId == topicParam.parms.prjId && m.sampGrpId == topicParam.parms.sampGrpId).ToList();
            data.errMsg = "测试数据";
            data.statusCode = "1";
            return data;
        }
        /// <summary>
        /// 推送被访者信息到，老宋接口
        /// </summary>
        /// <param name="userInfos"></param>
        /// <returns></returns>
        private async static Task<ExternalStatus> PostUserInfo(List<ExternalUserInfo> userInfos)
        {
            ExternalStatus data = null;
            
            HttpResponseMessage response = await client.PostAsJsonAsync(
                "interviwer/add", new ExternalUser { parms=userInfos});
            if (response.IsSuccessStatusCode)
            {
                data = await response.Content.ReadAsAsync<ExternalStatus>();
            }

            // return URI of the created resource.
            return data;
        }
        /// <summary>
        /// 更新图文消息结果
        /// </summary>
        /// <param name="userInfos"></param>
        /// <returns></returns>
        public  static ExternalStatus Postqnttvmtrlresult(ExternalQnt qnt)
        {
            ExternalStatus data = null;
            HttpResponseMessage response =  client.PostAsJsonAsync(
                "qnttvmtrlresult/add", qnt).Result;
            if (response.IsSuccessStatusCode)
            {
                data =  response.Content.ReadAsAsync<ExternalStatus>().Result;
            }

            // return URI of the created resource.
            return data;
        }
        /// <summary>
        /// 
        /// 获取老宋那里的项目列表
        /// </summary>
        /// <param name="userInfos"></param>
        /// <returns></returns>
        public async static Task<ExternalProject> GetExternalProjects( )
        {
            ExternalProject data = null;
            HttpResponseMessage response = await client.PostAsJsonAsync(
                "proj/get",new ExternalProjectParam { nowpage=1,pagesize=10000});
            if (response.IsSuccessStatusCode)
            {
                data = await response.Content.ReadAsAsync<ExternalProject>();
            }

            // return URI of the created resource.
            return data;
        }
        public static void ImportRadar(string fileName) {
            string strConn = GetConn(fileName);
            using (OleDbConnection connection = new OleDbConnection(strConn))
            {
                try
                {
                    DataSet ds1 = new DataSet();
                    OleDbDataAdapter oada1 = new OleDbDataAdapter("select * from [Sheet1$]", connection);

                    oada1.Fill(ds1);
                    string conn = ConfigurationManager.ConnectionStrings["ConceptOwner"].ConnectionString; 
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
                    {
                        bulkCopy.DestinationTableName =
                            "dbo.RadarLink";

                        try
                        {
                            // Write from the source to the destination.
                            bulkCopy.WriteToServer(ds1.Tables[0]);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
                catch(Exception e)
                {
                   throw e;
                }
            }

        }
        public static List<QStruct> ImportVariables(string fileName,string externalPjId)
        {
            VariableBLL.RemoveByPjId(externalPjId);
            List<QStruct> variables = new List<QStruct>();
            string strConn = GetConn(fileName);
            using (OleDbConnection connection = new OleDbConnection(strConn))
            {
                string[][] mappings = new string[][] {
                    new string[] { "列名","ColumnName" },
                    new string[] { "题干编号" , "Index2" },
                    new string[] { "题干内容" ,"Text"},
                    new string[]{ "题的编码","Code" },
                    new string[] {"题型", "QuesTypeId" } ,
                    new string[] {"该题的答案值类型" ,"ValueType"}
                    };
                try
                {
                    DataSet ds1 = new DataSet();
                    OleDbDataAdapter oada1 = new OleDbDataAdapter("select * from [码表$]", connection);

                    oada1.Fill(ds1);
                    QStruct lastVar = null;
                    for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
                    {
                    QStruct qStruct = new QStruct();
                        DataRow row = ds1.Tables[0].Rows[i];
                        long rowId = DateTime.Now.Ticks;
                        #region 处理选项信息
                        var isVarValue = ds1.Tables[0].Rows[i][0] == System.DBNull.Value;
                        if (isVarValue)
                        {
                            VariableValue2 varValue = new VariableValue2();
                            varValue.VarValueId = DateTime.Now.Ticks;
                            varValue.VarId = lastVar.Variable.VarId;
                            varValue.Text = row["选项的文本内容"].ToString();
                            varValue.Code = row["选项code"].ToString();
                             
                             //lastVar.VariableValues.Add(varValue);
                            VariableValue2BLL.AddItem(varValue);
                            continue;
                        }
                        else
                        {
                            qStruct.Variable = new Variable();
                            qStruct.VariableValues = new List<VariableValue2>();

                            lastVar = qStruct;
                            qStruct.Variable.VarId = DateTime.Now.Ticks;
                            qStruct.Variable.ExternalVarId = externalPjId; 
                            qStruct.Variable.CreateDate = DateTime.Now;
                            if (row["选项的文本内容"] != System.DBNull.Value)
                            {
                                VariableValue2 varValue = new VariableValue2();
                                varValue.VarValueId = DateTime.Now.Ticks;
                                varValue.VarId = qStruct.Variable.VarId;
                                varValue.Text = row["选项的文本内容"].ToString();
                                varValue.Code = row["选项code"].ToString();
                                //qStruct.VariableValues.Add(varValue);
                                VariableValue2BLL.AddItem(varValue);
                            }

                        }
                        #endregion
                        #region 处理属性信息
                        for (int j = 0; j < ds1.Tables[0].Columns.Count - 2; j++)
                        {
                            var column = ds1.Tables[0].Columns[j];

                            PropertyInfo prop = qStruct.Variable.GetType().GetProperty(mappings[j][1], BindingFlags.Public | BindingFlags.Instance);
                            if (null != prop && prop.CanWrite)
                            {
                                if (row[column].GetType() == typeof(Double))
                                {
                                    if (prop.PropertyType == typeof(int))
                                    {
                                        var val = int.Parse(row[column].ToString());
                                        prop.SetValue(qStruct.Variable, val, null);
                                    }
                                }
                                else if (prop.PropertyType == typeof(QuestionType2))
                                {
                                    var val = Enum.Parse(typeof(QuestionType2), row[column].ToString());
                                    prop.SetValue(qStruct.Variable, val, null);
                                }
                                else if (prop.PropertyType == typeof(Concept.Entity.BaseDB.ValueType))
                                {
                                    var val = Enum.Parse(typeof(Concept.Entity.BaseDB.ValueType), row[column].ToString());
                                    prop.SetValue(qStruct.Variable, val, null);
                                }
                                else
                                {
                                    prop.SetValue(qStruct.Variable, row[column], null);
                                }
                            }
                        }
                        #endregion
                        VariableBLL.AddItem(qStruct.Variable);
                        variables.Add(qStruct);
                    }

                    //#region 添加选项值到数据库
                    //foreach (var item in variables)
                    //{
                    //    foreach (var sub in item.VariableValues)
                    //    {
                    //        VariableValue2BLL.AddItem(sub);
                    //    }
                    //}
                    //#endregion

                    ImportSample(fileName, variables,externalPjId);
                  
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                // The connection is automatically closed when the
                // code exits the using block.
            }
            return variables;
        }

        public static void GenreateExcel(string path,string groupId, string externalPjId, string topicId)
        {
            List<Message> list = MessageBLL.GetMessages(groupId,externalPjId,topicId);
           
            var fileName = path + "_"+groupId+"_"+externalPjId+"_"+topicId+".xlsx";
            CreateExcelFile.CreateExcelDocument(list, fileName);
        }

        internal static void GenreateExcel()
        {
            throw new NotImplementedException();
        }

        private static string GetConn(string fileName)
        {
            string SuffixName = Path.GetExtension(fileName);
            string strConn = "";
            if (SuffixName == ".xls")
            {
                //Offic2003连接字符串
                strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'";

            }
            else if (SuffixName == ".xlsx")
            {
                //Offic2010/2007连接字符串
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1';";
            }

            return strConn;
        }

        /// <summary>
        /// 添加被访者名单
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="variables"></param>
        /// <returns></returns>
        async public static void ImportSample(string fileName,List<QStruct> variables,string externalPjId)
        {
            string strConn = GetConn(fileName);

            using (OleDbConnection connection = new OleDbConnection(strConn))
            {
                    List<ExternalUserInfo> users = new List<ExternalUserInfo>(); 
                try
                {
                    DataSet ds1 = new DataSet();
                    OleDbDataAdapter oada1 = new OleDbDataAdapter("select * from [被访者名单$]", connection);
                    oada1.Fill(ds1);
                    for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
                        {
                        DataRow row = ds1.Tables[0].Rows[i];
                        ExternalUserInfo externalUserInfo = new ExternalUserInfo();
                        externalUserInfo.jsonData = new List<Other>();
                        externalUserInfo.prjId = externalPjId;
                        if (row[0] == DBNull.Value && row[1]==DBNull.Value)
                        {
                            break;
                        }
                        for (int j = 0; j < variables.Count; j++)
                        {
                            var answer = new VariableAnswer();
                            answer.Value = row[j].ToString().Trim();
                            answer.varId = variables[j].Variable.VarId;
                            bool isOther = true;
                         
                                if (!string.IsNullOrWhiteSpace(row[0].ToString()))
                                {
                              answer.OpenId = row[0].ToString().Trim();
                                }
                                else  
                                {
                                    answer.OpenId = row["手机号"].ToString().Trim();
                                }
                            
                   
                            VariableAnswerBLL.AddItem(answer);
                            
                            //set externalUser property.
                            for (int x = 0; x < ExternalUserInfo.convertMapping.Length; x++)
                            {
                                if (variables[j].Variable.ColumnName == ExternalUserInfo.convertMapping[x][1])
                                {
                                    isOther = false;
                                    PropertyInfo prop = externalUserInfo.GetType().GetProperty(ExternalUserInfo.convertMapping[x][0], BindingFlags.Public | BindingFlags.Instance);
                                      prop.SetValue(externalUserInfo, answer.Value.Trim(), null);
                                }
                            }
                            if (isOther)
                            {
                                externalUserInfo.jsonData.Add(new Other { key = variables[j].Variable.ColumnName, value = answer.Value.Trim() });
                            }
                        }
                        
                        users.Add(externalUserInfo);
                    }

                    //推送到老宋接口
                  //  Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept.Entity.BaseDB.Log.Concept_LogException { AddTime = DateTime.Now, MessDescript = "PostUserInfo:userInfo:" + JObject.FromObject(users).ToString()   });
                    var status =  await PostUserInfo(users);
                    Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept.Entity.BaseDB.Log.Concept_LogException { AddTime = DateTime.Now, MessDescript = "PostUserInfo:"+status.errMsg + ":" + status.statusCode });
                }
                catch (Exception ex)
                {
                    Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept.Entity.BaseDB.Log.Concept_LogException { AddTime = DateTime.Now, MessDescript = "PostUserInfo:exception" +ex.ToString() });
                }
                // The connection is automatically closed when the
                // code exits the using block.
            }
        }
    }
}
