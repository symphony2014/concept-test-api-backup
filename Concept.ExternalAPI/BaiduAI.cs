﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Baidu.Aip.Nlp;
using System.Web;
using Concept.Entity.BaseDB;
using Concept.Entity.External;
using System.Linq;
using Concept.BLL.BaseBD.Log;
using Newtonsoft.Json;
using Concept.Entity;
using Concept.BLL.BaseBD;

namespace ExternalAPI
{
    public class cluster
    {
        public int sentiment { get; set; }
        public string prop { get; set; }
        public string abstracts { get; set; }
    }

    public class BaiduAI
    {
        static HttpClient client = new HttpClient();



        // 百度云中开通对应服务应用的 API Key 建议开通应用的时候多选服务
        private static String clientId = "LGiXxIvvniYt0Vn5MNczUIBV";
        // 百度云中开通对应服务应用的 Secret Key
        private static String clientSecret = "k4fsS0CHpCA2r8e2xmb5gQGPVtF7B1MN";


        static Nlp nlp;


        static BaiduAI()
        {

            nlp = new Nlp(clientId, clientSecret);
        }
        /// <summary>
        /// 生成cluster数据
        /// </summary>
        /// <param name="messages"></param>
        /// <returns></returns>
        static public List<ExternalCluster> GenerateClusters(List<AIData> list)
        {
            // 将数据筛选为选择个数多的type ==》将数据递归的分组进行计算


            return list.GroupBy(x => x.externalPjId)
               .Select(x => new
               {
                   key = x.Key,
                   Rows = x.GroupBy(y => y.sampGrpId).Select(y => new
                   {
                       key = y.Key,
                       Rows = y.GroupBy(z => z.tpcId).Select(z => new
                       {
                           key = z.Key,
                           Rows = z.GroupBy(m => m.username).Select(m => new
                           {
                               key = m.Key,
                               Rows = m.GroupBy(n => n.typeId)
                               .Select(n => new
                               {
                                   key = n.Key,
                                   Rows = (from o in n
                                           group o by (o.prop + o.adj) into g
                                           let count = g.Count()
                                           orderby count descending
                                           select new { Value = g.Key, Count = g.Count(), forClusters = g.ToList() })
                               }).SelectMany(i => i.Rows)
                                .GroupBy(j => j.Value)
                                .Select(k => new { Value = k.Key, maxSentiment = k.OrderByDescending(j => j.Count).First() })
                           }).SelectMany(a1 => a1.Rows)
                            .SelectMany(b1 => b1.maxSentiment.forClusters)
                       }).SelectMany(a1 => a1.Rows)
                   }).SelectMany(d1 => d1.Rows)
               }).SelectMany(c1 => c1.Rows)
               .GroupBy(e1 => e1.prop).Select(f1 => new ExternalCluster
               {
                   type = f1.Key,
                   negative = f1.Where(a => a.sentiment == 0).Count() * 100 / f1.Count(),
                   positive = f1.Where(a => a.sentiment == 2).Count() * 100 / f1.Count(),
                   middle = f1.Where(a => a.sentiment == 1).Count() * 100 / f1.Count(),
               }).ToList();

        }
        /// <summary>
        /// 获取百度AI数据
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private static List<JObject> GetAIResult(string item)
        {
            var count = 0;
            List<JObject> results = new List<JObject>();
            Dictionary<int, List<JObject>> dict = new Dictionary<int, List<JObject>>();
            for (int i = 1; i <= 13; i++)
            {
                var options = new Dictionary<string, object>{
               {"type", i}
                };
                var result = nlp.CommentTag(item, options);
                results.Add(result);
                count++;
            }
            Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept.Entity.BaseDB.Log.Concept_LogException
            {
                AddTime = DateTime.Now,
                ModuleName = "BaiduAI:CommentTag:" + count.ToString(),
                ExceptMessage = "",
                MessDescript = JsonConvert.SerializeObject(results)
            });
            return results;
        }



        //private static IEnumerable<string> GroupUsers(List<AI> messages)
        //{
        //    return (from m in messages
        //            group m by m.username).Select(x =>
        //            {
        //                var newStr = "";
        //                foreach (var item in x)
        //                {
        //                    newStr += item.msgCntnt;
        //                }
        //                return newStr;
        //            });
        //}
        private static dynamic GroupUsersByName(List<Message> messages)
        {
            var data = from m in messages
                       group m by m.openId into g
                       select new
                       {
                           userName = g.Key,
                           Comments = g.Aggregate<Message, string>(string.Empty, (a, b) => a + "." + b.msgCntnt)
                       };
            var aiResult = from t in data
                           select GetAIResult(t.Comments);

            return data;
        }
        static public ExternalAIData GetcommentTags(List<Message> messages)
        {

            ExternalAIData wordcloud = new ExternalAIData();

            List<AIData> datas = SaveAIData(messages);
            
            wordcloud.ids = GenerateIds(datas);
            wordcloud.comments = GenerateComments(datas);
            wordcloud.clusters = GenerateClusters(datas);

            return wordcloud;

        }

        private static List<AIData> SaveAIData(List<Message> messages)
        {
            List<AIData> aiDatas = new List<AIData>();
            var data = from m in messages
                       group m by new { m.ExternalPjId, m.sampGrpId, m.tpcId, m.openId,m.usericon,m.username } into g
                       select new
                       {
                           MapKey = g.Key,
                           Comments = g.Aggregate<Message, string>(string.Empty, (a, b) => a + "。" + b.msgCntnt)
                       };

            foreach (var item in data)
            {
                List<JObject> list = GetAIResult(item.Comments);
                var index = 0;
                string err = string.Empty;
                foreach (var subitem in list)
                {
                    index++;
                    if (((JValue)subitem["error_code"]) == null)
                    {
                        // List<AIData> aiDatas = new List<AIData>();
                        AIData aiData = new AIData();
                        foreach (var sub in subitem["items"].Values())
                        {
                            if ((sub as JProperty).Name == "sentiment")
                            {
                                aiData = new AIData();
                                aiData.openId = item.MapKey.openId;
                                aiData.typeId = index;
                                aiData.sampGrpId = item.MapKey.sampGrpId;
                                aiData.tpcId = item.MapKey.tpcId;
                                aiData.externalPjId = item.MapKey.ExternalPjId;
                                aiData.usericon = item.MapKey.usericon;
                                aiData.username = item.MapKey.username;
                                aiData.sentiment = Convert.ToInt32(((sub as JProperty).Value as JValue).Value.ToString());
                            }
                            if ((sub as JProperty).Name == "prop")
                            {
                                aiData.prop = ((sub as JProperty).Value as JValue).Value.ToString();
                            }
                            if ((sub as JProperty).Name == "abstract")
                            {
                                aiData.abstractStr = ((sub as JProperty).Value as JValue).Value.ToString().Replace("<span>","").Replace("</span>","");
                            }
                            if ((sub as JProperty).Name == "adj")
                            {
                                aiData.adj = ((sub as JProperty).Value as JValue).Value.ToString();
                                aiDatas.Add(aiData);
                               
                                AIDataBLL.AddItem(aiData);
                            }
                        }
                    }
                    else
                    {
                        err += JsonConvert.SerializeObject((JValue)subitem["error_code"]);
                    }
                }
                Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept.Entity.BaseDB.Log.Concept_LogException
                {
                    AddTime = DateTime.Now,
                    ModuleName = "BaiduAI:CommentTag:baiduError",
                    ExceptMessage = "",
                    MessDescript = err
                });

            }

            return aiDatas;
        }
        public static string encode(string raw)
        {
            return HttpUtility.UrlEncode(raw.Replace("<span>", "").Replace("</span>", ""));
        }
        public static ExternalAIData Search(AIParam param)
        {

            ExternalAIData data = new ExternalAIData();
            List<AIData> datas = AIDataBLL.Search(param);
            data.ids = GenerateIds(datas);
            data.comments = GenerateComments(datas);
            data.clusters = GenerateClusters(datas);
            return data;
        }
        private static List<Comment> GenerateComments(List<AIData> aiDatas)
        {
            return (
             from t in aiDatas
             select new Comment { comment = t.abstractStr, userIcon = t.usericon, userName = t.username }
                    ).GroupBy(c=>c.comment).Select(g=>g.First()).ToList();
        }

        public static List<idx> GenerateIds(List<AIData> aiDatas)
        {

            //基本分组
            var data = aiDatas.GroupBy(x => x.externalPjId)
                .Select(x => new
                {
                    key = x.Key,
                    Rows = x.GroupBy(y => y.sampGrpId).Select(y => new
                    {
                        key = y.Key,
                        Rows = y.GroupBy(z => z.tpcId).Select(z => new
                        {
                            key = z.Key,
                            Rows = z.GroupBy(m => m.username).Select(m => new
                            {
                                key = m.Key,
                                Rows = m.GroupBy(n => n.typeId)
                                .Select(n => new
                                {
                                    key = n.Key,
                                    Rows = (from o in n
                                            group o by (o.prop + o.adj) into g
                                            let count = g.Count()
                                            orderby count descending
                                            select new { Value = g.Key, Count = count })
                                }).SelectMany(i => i.Rows)
                                 .GroupBy(j => j.Value)
                                 .Select(k => new { Value = k.Key, Count = k.Max(kk => kk.Count) })
                            })
                        }).SelectMany(a1 => a1.Rows)
                          .SelectMany(a2 => a2.Rows)
                          .GroupBy(a3 => a3.Value)
                          .Select(a4 => new { Value = a4.Key, Count = a4.Sum(a5 => a5.Count) })
                    }).SelectMany(b1 => b1.Rows)
                          .GroupBy(b2 => b2.Value)
                          .Select(b3 => new { Value = b3.Key, Count = b3.Sum(b4 => b4.Count) })
                }).SelectMany(c1 => c1.Rows)
                  .GroupBy(c2 => c2.Value)
                  .Select(c3 => new idx { id = encode(c3.Key), value = c3.Sum(c4 => c4.Count) });

            return data.ToList();
        }

        static public List<cluster> MergeResultForCluster(List<JObject> list)
        {
            List<cluster> segments = new List<cluster>();
            foreach (var item in list)
            {
                /// "items": [
                ///{
                ///  "sentiment": 2,
                ///  "abstract": "这个新的分布式数据库<span>看起来挺不错</span>的",
                ///  "prop": "看起来",
                /// "begin_pos": 20,
                ///  "end_pos": 32,
                ///  "adj": "不错"
                /// },

                if (((JValue)item["error_code"]) == null)
                {
                    cluster segment = null;
                    foreach (var sub in item["items"].Values())
                    {
                        if ((sub as JProperty).Name == "sentiment")
                        {
                            segment = new cluster();
                            segment.sentiment = Convert.ToInt32(((sub as JProperty).Value as JValue).Value.ToString());
                        }
                        if ((sub as JProperty).Name == "prop")
                        {
                            segment.prop = ((sub as JProperty).Value as JValue).Value.ToString();
                        }
                        if ((sub as JProperty).Name == "abstract")
                        {
                            segment.abstracts = ((sub as JProperty).Value as JValue).Value.ToString();
                        }
                        if ((sub as JProperty).Name == "adj")
                        {
                            segments.Add(segment);
                        }
                    }
                }
            }

            segments = segments.OrderBy(s => s.abstracts).ToList();
            //remove dupes
            Int32 index = 0;
            while (index < segments.Count - 1)
            {
                if (segments[index].abstracts == segments[index + 1].abstracts)
                    segments.RemoveAt(index);
                else
                    index++;
            }

            return segments;
        }
        static public List<string> MergeResultForComment(IEnumerable<JObject> list)
        {
            List<string> segments = new List<string>();
            foreach (var item in list)
            {
                /// "items": [
                ///{
                ///  "sentiment": 2,
                ///  "abstract": "这个新的分布式数据库<span>看起来挺不错</span>的",
                ///  "prop": "看起来",
                /// "begin_pos": 20,
                ///  "end_pos": 32,
                ///  "adj": "不错"
                /// },

                if (((JValue)item["error_code"]) == null)
                {
                    string segment = null;
                    foreach (var sub in item["items"].Values())
                    {
                        if ((sub as JProperty).Name == "sentiment")
                        {

                        }

                        if ((sub as JProperty).Name == "abstract")
                        {
                            segment = ((sub as JProperty).Value as JValue).Value.ToString();
                            //replace <span>
                            segment = segment.Replace("<span>", "").Replace("</span>", "");
                            segments.Add(segment);
                        }
                        if ((sub as JProperty).Name == "adj")
                        {
                            // segments.Add(segment);
                        }
                    }
                }
            }


            return segments;
        }
        //
        static public List<string> MergeResultForWordClouds(List<JObject> list)
        {
            List<string> segments = new List<string>();
            foreach (var item in list)
            {
                /// "items": [
                ///{
                ///  "sentiment": 2,
                ///  "abstract": "这个新的分布式数据库<span>看起来挺不错</span>的",
                ///  "prop": "看起来",
                /// "begin_pos": 20,
                ///  "end_pos": 32,
                ///  "adj": "不错"
                /// },
                if (((JValue)item["error_code"]) == null)
                {
                    var segment = string.Empty;
                    foreach (var sub in item["items"].Values())
                    {
                        if ((sub as JProperty).Name == "prop")
                        {
                            segment = string.Empty;
                            segment = (((sub as JProperty).Value as JValue).Value.ToString());
                        }
                        //if ((sub as JProperty).Name == "prop")
                        //{
                        //    segment.prop = ((sub as JProperty).Value as JValue).Value.ToString();
                        //}
                        //if ((sub as JProperty).Name == "abstract")
                        //{
                        //    segment.abstracts = ((sub as JProperty).Value as JValue).Value.ToString();
                        //}
                        if ((sub as JProperty).Name == "adj")
                        {
                            segment += (((sub as JProperty).Value as JValue).Value.ToString());
                            segments.Add(segment);
                        }
                    }
                }

            }

            //remove dupes
            segments.Sort();
            Int32 index = 0;
            while (index < segments.Count - 1)
            {
                if (segments[index] == segments[index + 1])
                    segments.RemoveAt(index);
                else
                    index++;
            }

            return segments;
        }

    }
}
