﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.ExternalAPI
{
    static public class ConstString
    {
              public  static readonly string[][] CodeMappings = new string[][] {
                    new string[] { "列名","ColumnName" },
                    new string[] { "题干编号" , "Index2" },
                    new string[] { "题干内容" ,"Text"},
                    new string[]{ "题的编码","Code" },
                    new string[] {"题型", "QuesTypeId" } ,
                    new string[] {"该题的答案值类型" ,"ValueType"}
                    };
    }
}
