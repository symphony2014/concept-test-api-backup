﻿
using Concept.Entity.BaseDB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


using System.Web;
using Newtonsoft.Json.Linq;
using Concept.Entity.External;
using ConceptCommon;
using Concept.BLL.BaseBD;
using Concept.BLL.BaseBD.Log;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Concept.Entity.EnumMode;
using Concept.Entity.BaseDB.Log;
using System.Threading;

namespace ExternalAPI
{
    public class pageInfo
    {
        public int nowpage { get; set; }
        public int pagesize { get; set; }
    }
    /// <summary>
    /// 处理外部接口方法
    /// </summary>
    public class Import
    {
        static HttpClient client = new HttpClient();
        static Import()
        {
            var externalAPI = ConfigurationManager.AppSettings["ExternalAPI"];
            client.BaseAddress = new Uri(externalAPI);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }
        ///// <summary>
        ///// 获取topic列表
        ///// </summary>
        ///// <param name="userInfos"></param>
        ///// <returns></returns>
        //public static async Task<ExternalMessage<eTopic>> GetTopicInfo(ExternalTopicParam topicParam)
        //{
        //    ExternalMessage<Message> data = null;
        //    HttpResponseMessage response = await client.PostAsJsonAsync(
        //        "msg/get", topicParam);
        //    if (response.IsSuccessStatusCode)
        //    {
        //        data = await response.Content.ReadAsAsync<ExternalMessage<eTopic>>();
        //    }

        //    // return URI of the created resource.
        //    return data;
        //}
        /// <summary>
        /// 获取聊天信息
        /// </summary>
        /// <param name="userInfos"></param>
        /// <returns></returns>
        public static async Task<ExternalMessage<Message>> GetMessageInfo(ExternalTopicParam topicParam)
        {
            ExternalMessage<Message> data = null;
            HttpResponseMessage response = await client.PostAsJsonAsync(
                "msg/get", topicParam);
            if (response.IsSuccessStatusCode)
            {
                data = await response.Content.ReadAsAsync<ExternalMessage<Message>>();
            }

            // return URI of the created resource.
            return data;
        }
        /// <summary>
        /// 获取concept项目列表
        /// </summary>
        /// <param name="userInfos"></param>
        /// <returns></returns>
        public static ExternalMessage<eConceptProject> GetConceptProjects(ExternalParam<ExternaleTopicParam> topicParam)
        {
            ExternalMessage<eConceptProject> data = null;
            HttpResponseMessage response = client.PostAsJsonAsync(
                "qnttvmtrl/get", topicParam).Result;
            if (response.IsSuccessStatusCode)
            {
                data = response.Content.ReadAsAsync<ExternalMessage<eConceptProject>>().Result;
            }

            // return URI of the created resource.
            return data;
        }
        /// <summary>
        /// 获取Group
        /// </summary>
        /// <param name="userInfos"></param>
        /// <returns></returns>
        public static ExternalMessage<eGroup> GetGroupInfo(ExternalParam<ExternalGroupParam> topicParam)
        {
            ExternalMessage<eGroup> data = null;
            HttpResponseMessage response = client.PostAsJsonAsync(
                "sampgrp/get", topicParam).Result;
            if (response.IsSuccessStatusCode)
            {
                data = response.Content.ReadAsAsync<ExternalMessage<eGroup>>().Result;
            }

            // return URI of the created resource.
            return data;
        }

        public static void ImportVariablesAppend(string newFileName, string externalPjId)
        {
            var variables = VariableBLL.GetByPrjId(externalPjId);
            var structs = variables.Select(v => new QStruct { Variable = v }).ToList();

            ImportSample(newFileName, structs, externalPjId, OperationType.Append);
        }

        /// <summary>
        /// 推送被访者信息到，老宋接口
        /// </summary>
        /// <param name="userInfos"></param>
        /// <returns></returns>
        private async static Task<ExternalStatus> PostUserInfo(ExternalUser userInfos)
        {
            ExternalStatus data = null;

            var logString = JsonConvert.SerializeObject(userInfos);
            Concept_OpMessageBLL.AddOpMessage(new Concept_OpMessage
            {
                AddTime = DateTime.Now,
                Msg = logString,
                ErrorContext = "PostUserInfo"
            });

            HttpResponseMessage response = await client.PostAsJsonAsync(
                 "interviwer/add", userInfos);
            if (response.IsSuccessStatusCode)
            {
                data = await response.Content.ReadAsAsync<ExternalStatus>();
            }
            // return URI of the created resource.
            return data;
        }

        public static ExternalMessage<eTopic> GeteTopicInfo(ExternalParam<ExternaleTopicParam> externalParam)
        {
            ExternalMessage<eTopic> data = null;
            HttpResponseMessage response = client.PostAsJsonAsync(
                "tpc/get", externalParam).Result;
            if (response.IsSuccessStatusCode)
            {
                data = response.Content.ReadAsAsync<ExternalMessage<eTopic>>().Result;
            }

            // return URI of the created resource.
            return data;
        }
        public static eTopic GetSpecTopic(ExternalParam<ExternaleTopicParam> parm, string tpcId)
        {
            if (tpcId == "-1")
                return new eTopic() { id = "", dscInf = "", tpcNm = "all topic" };
            var mess = GeteTopicInfo(parm);
            return mess.data.rows.Where(r => r.id == tpcId).FirstOrDefault();
        }
        public static eGroup GetSpecGroup(ExternalParam<ExternalGroupParam> parm, string groupId)


        {
            if (groupId == "-1")
                return new eGroup() { id = "", dscInf = "", sampNm = "all group" };
            var mess = GetGroupInfo(parm);
            return mess.data.rows.Where(r => r.id == groupId).FirstOrDefault();
        }
        /// <summary>
        /// 更新图文消息结果
        /// </summary>
        /// <param name="userInfos"></param>
        /// <returns></returns>
        public static ExternalStatus Postqnttvmtrlresult(ExternalQnt qnt)
        {
            ExternalStatus data = null;
            HttpResponseMessage response = client.PostAsJsonAsync(
                "qnttvmtrlresult/add", qnt).Result;
            if (response.IsSuccessStatusCode)
            {
                data = response.Content.ReadAsAsync<ExternalStatus>().Result;
            }

            // return URI of the created resource.
            return data;
        }
        /// <summary>
        /// 
        /// 获取老宋那里的项目列表
        /// </summary>
        /// <param name="userInfos"></param>
        /// <returns></returns>
        public async static Task<ExternalProject> GetExternalProjects()
        {
            ExternalProject data = null;
            HttpResponseMessage response = await client.PostAsJsonAsync(
                "proj/get", new ExternalProjectParam { nowpage = 1, pagesize = 10000 });
            if (response.IsSuccessStatusCode)
            {
                data = await response.Content.ReadAsAsync<ExternalProject>();
            }

            // return URI of the created resource.
            return data;
        }
        public static void ImportRadar(string fileName)
        {
            string strConn = GetConn(fileName);
            using (OleDbConnection connection = new OleDbConnection(strConn))
            {
                try
                {
                    DataSet ds1 = new DataSet();
                    OleDbDataAdapter oada1 = new OleDbDataAdapter("select * from [Sheet1$]", connection);

                    oada1.Fill(ds1);
                    string conn = ConfigurationManager.ConnectionStrings["ConceptOwner"].ConnectionString;
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
                    {
                        bulkCopy.DestinationTableName =
                            "dbo.RadarLink";

                        try
                        {

                            var result = (from rw in ds1.Tables[0].AsEnumerable()
                                          select new RadarLink
                                          {
                                              SurveryId = Convert.ToString(rw["SurveryId"]),
                                              Link = Convert.ToString(rw["Link"])
                                          }).ToList();
                            // Write from the source to the destination.
                            //bulkCopy.WriteToServer(newTable);
                            foreach (var item in result)
                            {
                                RadarLinkBLL.AddItem(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept.Entity.BaseDB.Log.Concept_LogException { AddTime = DateTime.Now, MessDescript = ex.ToString() });
                        }
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

        }
        public static List<QStruct> ImportVariables(string fileName, string externalPjId)
        {
            Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept.Entity.BaseDB.Log.Concept_LogException { AddTime = DateTime.Now, MessDescript = "ImportVariables:normal" });
            VariableBLL.RemoveByPjId(externalPjId);
            List<QStruct> variables = new List<QStruct>();
            string strConn = GetConn(fileName);
            using (OleDbConnection connection = new OleDbConnection(strConn))
            {
                string[][] mappings = new string[][] {
                    new string[] { "列名","ColumnName" },
                    new string[] { "题干编号" , "Index2" },
                    new string[] { "题干内容" ,"Text"},
                    new string[]{ "题的编码","Code" },
                    new string[] {"题型", "QuesTypeId" } ,
                    new string[] {"该题的答案值类型" ,"ValueType"}
                    };
                try
                {
                    DataSet ds1 = new DataSet();
                    OleDbDataAdapter oada1 = new OleDbDataAdapter("select * from [码表$]", connection);

                    oada1.Fill(ds1);
                  //  List<QStruct> lastVars = new List<QStruct>();
   
                    for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
                    {
                        Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { AddTime = DateTime.Now, MessDescript = "ImportVariables:start count" + i });
                       // Thread.Sleep(1000);
                        QStruct qStruct = new QStruct();
                        DataRow row = ds1.Tables[0].Rows[i];
                        long rowId = IdHelper.GetNewLongId();
                        #region 处理选项信息
                        var isVarValue = ds1.Tables[0].Rows[i][0] == System.DBNull.Value;
                        if (isVarValue)
                        {
                            var last = variables.Last();
                            VariableValue2 varValue = new VariableValue2();
                            varValue.VarValueId = DateTime.Now.Ticks;
                            varValue.VarId = last.Variable.VarId;
                         
                            varValue.Text = row["选项的文本内容"].ToString();
                            varValue.Code = row["选项code"].ToString();

                            last.VariableValues.Add(varValue);
                           // VariableValue2BLL.AddItem(varValue);
                            continue;
                        }
                        else
                        {
                            qStruct.Variable = new Variable();
                            qStruct.VariableValues = new List<VariableValue2>();

                        
                            qStruct.Variable.VarId = DateTime.Now.Ticks;
                        

                            qStruct.Variable.ExternalVarId = externalPjId;
                            qStruct.Variable.CreateDate = DateTime.Now;
                            if (row["选项的文本内容"] != System.DBNull.Value)
                            {
                                VariableValue2 varValue = new VariableValue2();
                                varValue.VarValueId = DateTime.Now.Ticks;
                                varValue.VarId = qStruct.Variable.VarId;
                                varValue.Text = row["选项的文本内容"].ToString();
                                varValue.Code = row["选项code"].ToString();

                                qStruct.VariableValues.Add(varValue);

                               // VariableValue2BLL.AddItem(varValue);
                             
                            }
                           // lastVars.Add(qStruct);

                        }
                        #endregion
                        #region 处理属性信息
                        for (int j = 0; j < ds1.Tables[0].Columns.Count - 2; j++)
                        {
                            var column = ds1.Tables[0].Columns[j];

                            PropertyInfo prop = qStruct.Variable.GetType().GetProperty(mappings[j][1], BindingFlags.Public | BindingFlags.Instance);
                            if (null != prop && prop.CanWrite)
                            {
                                if (row[column].GetType() == typeof(Double))
                                {
                                    if (prop.PropertyType == typeof(int))
                                    {
                                        var val = int.Parse(row[column].ToString());
                                        prop.SetValue(qStruct.Variable, val, null);
                                    }
                                }
                                else if (prop.PropertyType == typeof(QuestionType2))
                                {
                                    var val = Enum.Parse(typeof(QuestionType2), row[column].ToString());
                                    prop.SetValue(qStruct.Variable, val, null);
                                }
                                else if (prop.PropertyType == typeof(Concept.Entity.BaseDB.ValueType))
                                {
                                    var val = Enum.Parse(typeof(Concept.Entity.BaseDB.ValueType), row[column].ToString());
                                    prop.SetValue(qStruct.Variable, val, null);
                                }
                                else
                                {
                                    prop.SetValue(qStruct.Variable, row[column], null);
                                }
                            }
                        }
                        #endregion
                        // 开始插入数据库
                        Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { AddTime = DateTime.Now, MessDescript = "ImportVariables:start 开始插入数据库:" + ds1.Tables[0].Rows[i][0] });

                        //  VariableBLL.AddItem(qStruct.Variable);
                        //var value=VariableBLL.GetById(qStruct.Variable.VarId);
                        // while (value==null)
                        //  {
                        //      Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { AddTime = DateTime.Now, MessDescript = "ImportVariables:sync error" });
                        //      value = VariableBLL.GetById(qStruct.Variable.VarId);
                        //  }
                     
                        variables.Add(qStruct);
                    }

                    //#region 添加选项值到数据库
                    foreach (var item in variables)
                    {
                       var result= VariableBLL.AddItem(item.Variable);
                        Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept.Entity.BaseDB.Log.Concept_LogException { AddTime = DateTime.Now, MessDescript = "ImportSample:columnName:"+item.Variable.ColumnName +result+":varId:"+item.Variable.VarId});
                        foreach (var sub in item.VariableValues)
                        {
                            VariableValue2BLL.AddItem(sub);
                        }
                    }
                    //#endregion
                    Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept.Entity.BaseDB.Log.Concept_LogException { AddTime = DateTime.Now, MessDescript = "ImportSample:normal2" });
                    ImportSample(fileName, variables, externalPjId);

                }
                catch (Exception ex)
                {
                    Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept.Entity.BaseDB.Log.Concept_LogException { AddTime = DateTime.Now, MessDescript = "ImportSample:ex" + ex.ToString() });
                }
                // The connection is automatically closed when the
                // code exits the using block.
            }
            return variables;
        }

        public static string GenerateExcelFromDataUrl(string dataUrl, string directory)
        {
            var base64Data = Regex.Match(dataUrl, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
            var binData = Convert.FromBase64String(base64Data);
            var fileName = Path.Combine(directory, "wordcloud.jpg");
            File.WriteAllBytes(fileName, binData);
            return fileName;
        }
        /// <summary>
        /// generate chat data excel and return file name.
        /// </summary>
        /// <param name="topicPath"></param>
        /// <param name="externalPjId"></param>
        /// <param name="groupId"></param>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public static string GetTopicInfoExcel(string directory, string externalPjId, string groupId, string topicId)
        {

            try
            {
                var group = Import.GetSpecGroup(new ExternalParam<ExternalGroupParam>
                {
                    parms = new ExternalGroupParam
                    {
                        prjId = externalPjId
                    }
                }, groupId);
                var topic = Import.GetSpecTopic(new ExternalParam<ExternaleTopicParam>
                {
                    parms = new ExternaleTopicParam
                    {
                        level = "1",
                        prjId = externalPjId
                    }
                }, topicId);
                List<Message> list = MessageBLL.GetMessages(externalPjId, groupId, topicId);

                var result = list.Select(l => new ExternalMessageLite
                {
                    groupName = group.sampNm,
                    TopicName = topic.tpcNm,
                    SendUser = l.username,
                    Content = l.msgCntnt,
                    ContentUrl = l.url,
                    Date = l.crtTm
                }).ToList();
                var fileName = directory + "\\chatdata_" + Guid.NewGuid().ToString() + ".xlsx";
                CreateExcelFile.CreateExcelDocument(result, fileName);
                return fileName;
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }
        public static void GenreateExcel(string path, string externalPjId, string groupId, string topicId)
        {
            List<Message> list = MessageBLL.GetMessages(externalPjId, groupId, topicId);

            var fileName = path + "\\" + externalPjId + "_" + groupId + "_" + topicId + ".xlsx";
            CreateExcelFile.CreateExcelDocument(list, fileName);
        }

        internal static void GenreateExcel()
        {
            throw new NotImplementedException();
        }

        private static string GetConn(string fileName)
        {
            string SuffixName = Path.GetExtension(fileName);
            string strConn = "";
            if (SuffixName == ".xls")
            {
                //Offic2003连接字符串
                strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'";

            }
            else if (SuffixName == ".xlsx")
            {
                //Offic2010/2007连接字符串
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1';";
            }

            return strConn;
        }

        /// <summary>
        /// 添加被访者名单
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="variables"></param>
        /// <returns></returns>
        async public static void ImportSample(string fileName, List<QStruct> variables, string externalPjId, OperationType addType = OperationType.Add)
        {
            string strConn = GetConn(fileName);
            Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { AddTime = DateTime.Now, MessDescript = "ImportSample:userInfo:" });
            using (OleDbConnection connection = new OleDbConnection(strConn))
            {
                List<ExternalUserInfo> users = new List<ExternalUserInfo>();
                try
                {
                    DataSet ds1 = new DataSet();
                    OleDbDataAdapter oada1 = new OleDbDataAdapter("select * from [被访者名单$]", connection);
                    oada1.Fill(ds1);
                    for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
                    {
                        DataRow row = ds1.Tables[0].Rows[i];
                        ExternalUserInfo externalUserInfo = new ExternalUserInfo();
                        externalUserInfo.jsonData = new List<Other>();
                        externalUserInfo.prjId = externalPjId;
                        if (row[0] == DBNull.Value && row[1] == DBNull.Value)
                        {
                            break;
                        }
                        for (int j = 0; j < variables.Count; j++)
                        {
                            var answer = new VariableAnswer();
                            try
                            {
                                answer.Value = row[j].ToString().Trim();
                            }
                            catch (Exception e)
                            {

                                Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { AddTime = DateTime.Now, MessDescript = "appendUserInfo:exception" + e.ToString() });
                            }

                            answer.varId = variables[j].Variable.VarId;
                            bool isOther = true;

                            if (!string.IsNullOrWhiteSpace(row[0].ToString()))
                            {
                                answer.OpenId = row[0].ToString().Trim();
                            }
                            else
                            {
                                answer.OpenId = row["手机号"].ToString().Trim();
                            }


                            VariableAnswerBLL.AddItem(answer);

                            //set externalUser property.
                            for (int x = 0; x < ExternalUserInfo.convertMapping.Length; x++)
                            {
                                if (variables[j].Variable.Code == ExternalUserInfo.convertMapping[x][1])
                                {
                                    isOther = false;
                                    PropertyInfo prop = externalUserInfo.GetType().GetProperty(ExternalUserInfo.convertMapping[x][0], BindingFlags.Public | BindingFlags.Instance);
                                    if (prop != null)
                                        prop.SetValue(externalUserInfo, answer.Value.Trim(), null);
                                }
                            }
                            if (isOther)
                            {
                                externalUserInfo.jsonData.Add(new Other { key = variables[j].Variable.ColumnName, value = answer.Value.Trim() });
                            }
                        }

                        users.Add(externalUserInfo);
                    }

                    //推送到老宋接口
                    Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { AddTime = DateTime.Now, ExceptStackTrace = "PostUserInfo:userInfo:" + JsonConvert.SerializeObject(users) });
                    var status = await PostUserInfo(new ExternalUser { parms = users, optCode = addType });
                    Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { AddTime = DateTime.Now, MessDescript = "PostUserInfo:" + status.errMsg + ":" + status.statusCode });
                }
                catch (Exception ex)
                {
                    Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { AddTime = DateTime.Now, MessDescript = "PostUserInfo:exception" + ex.ToString() });
                }
                // The connection is automatically closed when the
                // code exits the using block.
            }
        }
    }
}
