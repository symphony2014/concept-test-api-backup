﻿using Concept.BLL.BaseBD;
using Concept.Entity.BaseDB;
using Concept.Entity.External;
using ConceptApiController;

using ConceptApiController.SyncReport;
using ConceptWebApi.Controllers;
using IpsosReport.ModelBiz.External;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    class Program
    {
        static  void Main(string[] args)
        {
            //  Task<ReturnData> data=Sync();
            //int x = 0;
            //data.Wait();
            //var result = data.Result;
            //VariableBLL.RemoveByPjId("10");
            ConceptController controller = new ConceptController();
            ExternalUserInfo user = new ExternalUserInfo();
            user.phone = "13917637141";
            user.openId = "GmSJw2_Xl9CQCNqKMzwZxQeYdPk";
            user.prjId = "19";
            user.usrAge = "1999";
            user.sex = "男性";
            
            controller.UpdateUserInfo(user);
            var userUpdated = controller.GetUserInfo(user.openId);

            int radarId = 26;
            string csq = "c";
            controller.UpdateRadarStatus(radarId, csq);

            //中间跳转和提交答案
            ConceptWebApi.Models.Param param = new ConceptWebApi.Models.Param
            {
                SuvId="1",
                 ExternalPjId="1",
                 GroupId="1",
                 ExternalUserId="1",
                 LinkType=1,
                 SuvType=ConceptWebApi.Models.SuvType.Conceptest,
                 OpenId="99999",
                 returnUrl="http://www.baidu.com",
                 MessageId="1"
            };
            controller.Redirect(param);

            MobileController mobile = new MobileController();
            mobile.SubmitAnswer(new List<DeviceAnswer> {
                new DeviceAnswer{
                    AppendData="suvId=1&externalpjid=1&groupid=1&externaluserid=1",
                    DeviceId="99999",
                }
            },true);
        }
         
        static async Task<ReturnData> Sync()
        {
            var sync = new Sync(60);
            var result =await sync.SyncToReport();
            return result;
        }
    }
}
