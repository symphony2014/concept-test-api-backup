﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConceptWebApi.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.Entity.External;
using Newtonsoft.Json;
using ConceptWebApi.Models;

namespace ConceptWebApi.Controllers.Tests
{
    [TestClass()]
    public class ConceptControllerTests
    {
        [TestMethod()]
        public void RedirectTest()
        {
            ConceptController controller = new ConceptController();
            Param param = new Param();
            //PjId	Link	OpenId	Mobile	Id	GroupId	TopicId	ExternalPjId	Status	SurveryId	IsUsed	LinkType	ReturnUrl	externalUserId	messageId
            //NULL              	NULL	3482	71	NULL	10	c	180112173559840512	1	0	http://mobilefgdtest.ipsos.com.cn/chat/login?prjId=10&sampGrpId=71&roomId=40661833285633&userId=31	31	8
            param.SuvType = SuvType.Radar;
            param.OpenId = "oGmSJw0TdT840QZeMDv0oEOvlm-Y";
            param.ExternalPjId = "10";
            param.SuvId = "180112173559840512";
            param.LinkType = 0;
            param.GroupId = "71";
            controller.Redirect(param);
        }
        [TestMethod()]
        public void UpdateRadarStatusTest()
        {
            ConceptController controller = new ConceptController();
            int radarId = 26;
            string csq = "c";
            controller.UpdateRadarStatus(radarId, csq);
        }

     

        [TestMethod()]
        public void DownloadZipTest()
        {

        }

        [TestMethod()]
        public void AppendUserInfoTest()
        {
            ConceptController controller = new ConceptController();

            //{ "prjId":"42","userName":null,"openId":"owrJMwsLJ3zuvU28ElfHK7CCMaAY","phone":null,"idcardNo":null,"sex":null,"usrAge":null,"urbnLvlCd":null,"city":null,"eddgrCd":null,"idyTpcd":null,"wrkUnitNm":null,"pstnNm":null,"idvMoIncmam":null,"famMoIncmam":null,"famPpnNum":null,"famPercptaIncmam":null,"lclRsdncTm":null,"marSttnCd":null,"usrAgeRange":null,"jsonData":[{"key":"昵称","value":"何慢慢"},{"key":"微信头像","value":"http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaEJhEjWZ8gC2MrmXTAg0Qic64zaRIufmLcruBm1Kv6xDYHOtZMkLNaltaU5byKIpfsia2QhnGEL7hmgA/132
            var userstr = "{\"prjId\":\"42\",\"userName\":null,\"openId\":\"owrJMwsLJ3zuvU28ElfHK7CCMaAY\",\"phone\":null,\"idcardNo\":null,\"sex\":null,\"usrAge\":null,\"urbnLvlCd\":null,\"city\":null,\"eddgrCd\":null,\"idyTpcd\":null,\"wrkUnitNm\":null,\"pstnNm\":null,\"idvMoIncmam\":null,\"famMoIncmam\":null,\"famPpnNum\":null,\"famPercptaIncmam\":null,\"lclRsdncTm\":null,\"marSttnCd\":null,\"usrAgeRange\":null,\"jsonData\":[{\"key\":\"昵称\",\"value\":\"何慢慢\"},{\"key\":\"微信头像\",\"value\":\"http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaEJhEjWZ8gC2MrmXTAg0Qic64zaRIufmLcruBm1Kv6xDYHOtZMkLNaltaU5byKIpfsia2QhnGEL7hmgA/132\"}]}";
            var user = JsonConvert.DeserializeObject<ExternalUserInfo>(userstr);

           // controller.AppendUserInfo(user);
        }

       
    }
}