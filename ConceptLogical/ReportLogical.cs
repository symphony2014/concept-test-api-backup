﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text; 

namespace ConceptLogical
{
    public class ReportLogical
    {
        #region 单例定义
        private static object lockObject = new object();
        protected ReportLogical()
        {
        }

        private static volatile ReportLogical _instance;
        public static ReportLogical Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new ReportLogical());
                    }
                }
                return _instance;
            }
        }

        #endregion 单例定义 

        public List<List<string>> GetGradingEmotionData(int projectAutoId)
        {
            List<List<string>> resultData = new List<List<string>>();
            List<string> header1 = new List<string>();
            header1.Add("");
            header1.Add("");
            List<string> header2 = new List<string>();
            header2.Add("组名");
            header2.Add("设备ID");

            List<Group> groupList = GroupBLL.GetGroupList(projectAutoId);
            List<ConceptCard> conceptCardList = ConceptCardBLL.GetConceptCardList(projectAutoId);
            List<GradingQuestion> gradingQList = GradingQuestionBLL.GetGradingQuestionList(projectAutoId).Where(gq => gq.QuestionText != "").ToList();
            GradingEmoticonQuestion geq = GradingEmoticonQuestionBLL.GetGradingEmoticonQuestion(projectAutoId);
            List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();


            if (conceptCardList != null && conceptCardList.Count > 0)
            {
                foreach (var card in conceptCardList)
                {
                    if (gradingQList.Count > 0)
                    {
                        for (int j = 1; j <= gradingQList.Count; j++)
                        {
                            if (j == 1)
                            {
                                header1.Add("概念卡" + card.CardOrder + "("+card.ConceptCardName+")");
                            }
                            else
                            {
                                header1.Add("");
                            }
                            header2.Add("维度" + j + "打分");
                        }
                    }
                    if (geq.IsShowOpenQuestion == 1)
                    {
                        header1.Add("");
                        header2.Add("开放题答案");
                    }
                    if (geq.IsShowEmoticon == 1)
                    {
                        header1.Add("");
                        header2.Add("表情包选择");
                    }
                }
            }
            resultData.Add(header1);
            resultData.Add(header2);
            if (groupList != null && groupList.Count > 0)
            {
                foreach (var groupItem in groupList)
                {
                    var groupByDeviceAnswers = AllAnswerList.Where(a => a.GroupId == groupItem.GroupId).GroupBy(pet => pet.DeviceId).Select(g => (new { DeviceId = g.Key, Answers = g.ToList() }));
                    foreach (var device in groupByDeviceAnswers)
                    {
                        List<string> rowItem = new List<string>();
                        rowItem.Add(groupItem.GroupName);
                        rowItem.Add(device.DeviceId);

                        if (conceptCardList != null && conceptCardList.Count > 0)
                        {
                            foreach (var card in conceptCardList)
                            {
                                if (gradingQList.Count > 0)
                                {
                                    foreach (var gradingQItem in gradingQList)
                                    {
                                        var thisGradingQAnswer = device.Answers.Where(a => a.CardId == card.CardId && a.QuestionId == gradingQItem.GradingQuestionId).FirstOrDefault();
                                        if (thisGradingQAnswer != null)
                                        {
                                            rowItem.Add(thisGradingQAnswer.QuestionAnswer);
                                        }
                                        else
                                        {
                                            rowItem.Add("");
                                        }
                                    }
                                }
                                if (geq.IsShowOpenQuestion == 1)
                                {
                                    var thisOpenQAnswer = device.Answers.Where(a => a.CardId == card.CardId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.OpenText).FirstOrDefault();
                                    if (thisOpenQAnswer != null)
                                    {
                                        rowItem.Add(thisOpenQAnswer.QuestionAnswer);
                                    }
                                    else
                                    {
                                        rowItem.Add("");
                                    }
                                }
                                if (geq.IsShowEmoticon == 1)
                                {
                                    var thisIconQAnswer = device.Answers.Where(a => a.CardId == card.CardId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.Icon).FirstOrDefault();
                                    if (thisIconQAnswer != null)
                                    {
                                        rowItem.Add(thisIconQAnswer.QuestionAnswer);
                                    }
                                    else
                                    {
                                        rowItem.Add("");
                                    }
                                }
                            }
                        }
                        resultData.Add(rowItem);
                    }
                }
            }
            return resultData;
        }

        public List<List<string>> GetOrderData(int projectAutoId)
        {
            List<List<string>> resultData = new List<List<string>>();
            List<string> header2 = new List<string>();
            header2.Add("组名");
            header2.Add("设备ID");

            List<Group> groupList = GroupBLL.GetGroupList(projectAutoId);
            List<OrderQuestion> orderQList = OrderQuestionBLL.GetOrderQuestionList(projectAutoId).Where(o => o.OrderQuestionText != "").ToList();
            List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();


            if (orderQList != null && orderQList.Count > 0)
            {
                for (int i = 1; i <= orderQList.Count; i++)
                {
                    header2.Add("维度" + i + "排序(" + orderQList[i - 1].OrderQuestionText + ")");
                }
            }
            resultData.Add(header2);
            if (groupList != null && groupList.Count > 0)
            {
                foreach (var groupItem in groupList)
                {
                    var groupByDeviceAnswers = AllAnswerList.Where(a => a.GroupId == groupItem.GroupId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.OrderQuestion).GroupBy(pet => pet.DeviceId).Select(g => (new { DeviceId = g.Key, Answers = g.ToList() }));
                    foreach (var device in groupByDeviceAnswers)
                    {
                        List<string> rowItem = new List<string>();
                        rowItem.Add(groupItem.GroupName);
                        rowItem.Add(device.DeviceId);

                        if (orderQList.Count > 0)
                        {
                            foreach (var orderQItem in orderQList)
                            {
                                var thisOrderQAnswer = device.Answers.Where(an => an.QuestionId == orderQItem.OrderQuestionId).FirstOrDefault();
                                if (thisOrderQAnswer != null)
                                {
                                    rowItem.Add(thisOrderQAnswer.QuestionAnswer);
                                }
                                else
                                {
                                    rowItem.Add("");
                                }
                            }
                        }
                        resultData.Add(rowItem);
                    }
                }
            }
            return resultData;
        }

        public List<List<string>> GetClassifyData(int projectAutoId)
        {
            List<List<string>> resultData = new List<List<string>>();
            List<string> header2 = new List<string>();
            header2.Add("组名");
            header2.Add("设备ID");

            List<Group> groupList = GroupBLL.GetGroupList(projectAutoId);
            List<ClassifyQuestion> classifyQList = ClassifyQuestionBLL.GetClassifyQuestionList(projectAutoId).Where(o => o.ClassifyQuestionText != "").ToList();
            List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();


            if (classifyQList != null && classifyQList.Count > 0)
            {
                for (int i = 1; i <= classifyQList.Count; i++)
                {
                    header2.Add("分堆" + i + "(" + classifyQList[i - 1].ClassifyQuestionText + ")");
                }
            }
            resultData.Add(header2);
            if (groupList != null && groupList.Count > 0)
            {
                foreach (var groupItem in groupList)
                {
                    var groupByDeviceAnswers = AllAnswerList.Where(a => a.GroupId == groupItem.GroupId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.CategoryQuestion).GroupBy(pet => pet.DeviceId).Select(g => (new { DeviceId = g.Key, Answers = g.ToList() }));
                    foreach (var device in groupByDeviceAnswers)
                    {
                        List<string> rowItem = new List<string>();
                        rowItem.Add(groupItem.GroupName);
                        rowItem.Add(device.DeviceId);

                        if (classifyQList.Count > 0)
                        {
                            foreach (var classifyQItem in classifyQList)
                            {
                                var thisClassifyQAnswer = device.Answers.Where(an => an.QuestionId == classifyQItem.ClassifyQuestionId).FirstOrDefault();
                                if (thisClassifyQAnswer != null)
                                {
                                    rowItem.Add(thisClassifyQAnswer.QuestionAnswer);
                                }
                                else
                                {
                                    rowItem.Add("");
                                }
                            }
                        }
                        resultData.Add(rowItem);
                    }
                }
            }
            return resultData;
        }

        public List<List<string>> GetPaintingData(int projectAutoId)
        {
            List<List<string>> resultData = new List<List<string>>();
            List<string> header2 = new List<string>();
            header2.Add("组名");
            header2.Add("设备ID"); 
            

            List<Group> groupList = GroupBLL.GetGroupList(projectAutoId);
            List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();
            List<ConceptCard> conceptCardList = ConceptCardBLL.GetConceptCardList(projectAutoId);

            if(conceptCardList!=null && conceptCardList.Count>0)
            {
                foreach(var card in conceptCardList)
                {
                    header2.Add("概念卡" + card.CardOrder + "(" + card.ConceptCardName + ")");
                }
            }
            resultData.Add(header2);

            if (groupList != null && groupList.Count > 0)
            {
                foreach (var groupItem in groupList)
                {
                    var groupByDeviceAnswers = AllAnswerList.Where(a => a.GroupId == groupItem.GroupId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.Painting).GroupBy(pet => pet.DeviceId).Select(g => (new { DeviceId = g.Key, Answers = g.ToList() }));
                    foreach (var device in groupByDeviceAnswers)
                    {
                        List<string> rowItem = new List<string>();
                        rowItem.Add(groupItem.GroupName);
                        rowItem.Add(device.DeviceId);
                        //to be continue 
                        if (conceptCardList != null && conceptCardList.Count > 0)
                        {
                            foreach (var card in conceptCardList)
                            {
                                var thisCardAnswer = device.Answers.Where(a => a.CardId == card.CardId).FirstOrDefault();
                                if (thisCardAnswer != null)
                                {
                                    string DrawingimagePath = ConfigurationManager.AppSettings["DisplayPaintingCardImage"] + thisCardAnswer.QuestionAnswer;
                                    rowItem.Add(DrawingimagePath);
                                }
                                else
                                {
                                    rowItem.Add("");
                                }
                            }
                        } 
                        resultData.Add(rowItem);
                    }
                }
            }
            return resultData;
        }
    }
}
