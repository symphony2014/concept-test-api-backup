﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.SysMode
{
    public class CardSum
    {
        public string cardName { get; set; }
        public int allCount { get; set; }
        public int recyCount { get; set; }
        public int notInvolved { get; set; }
        public List<QuestionAnswer> answers { get; set; }

    }
    public class QuestionAnswer
    {
        public string Name { get; set; }
        public int SumCount { get; set; }
        public string Average { get; set; }
        public List<ScoreData> Scores { get; set; }

    }
    public class ScoreData
    {
        public string Score { get; set; }
        public int Count { get; set; }
        public float Percent { get; set; }
        
    }
}
