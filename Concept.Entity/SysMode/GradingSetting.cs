﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Concept.Entity.BaseDB;

namespace Concept.Entity.SysMode
{
    public class GradingSetting
    {
        public int ProjectAutoId { get; set; }
        public bool IsShowGrading { get; set; }
        public bool IsShowOpenQuestion { get; set; }
        public string OpenQuestion { get; set; }
        public List<GradingQuestion> GradingQuestionList { get; set; }
    }
}
