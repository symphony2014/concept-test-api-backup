﻿using Concept.Entity.BaseDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.SysMode
{
    public class OrderReportReturn
    {
        public List<string> header { get; set; }
        public List<OrderGroup> groups { get; set; }
    }

    public class OrderGroup
    {
        public string name { get; set; }
        public List<OrderDeviceInfo> device { get; set; }
    }

    public class OrderDeviceInfo
    {
        public string deviceId { get; set; }
        public List<UserDetail> details { get; set; }
        public List<string> orders { get; set; }
    }
}
