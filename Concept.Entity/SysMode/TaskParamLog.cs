﻿using ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.SysMode
{
     public  class TaskParamLog 
    {
        [DbField("Id", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int Id { get; set; }
        public string prjId { get; set; }
        public string sampGrpId { get; set; }
        public string Data { get; set; }
        /// <summary>
        /// 0:get external messages,1:Baidu AI task,3:generate excel.
        /// </summary>
        public int paramType { get; set; }
        public int status { get; set; }
        public string topicId { get; set; }
        [DbField("AddDate","DATETIME")]
        public DateTime AddDate { get; set; }
    }
}
