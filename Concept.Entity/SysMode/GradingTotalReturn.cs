﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.SysMode
{
    public class GradingTotalReturn
    {
        public List<string> Header { get; set; } 
        public List<List<string>> Contents { get; set; }
    }
}
