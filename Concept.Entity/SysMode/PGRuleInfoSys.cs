﻿using Concept.Entity.BaseDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.SysMode
{
    public class PGRuleInfoSys : PGRuleInfo
    {
        public List<string> TopicIdList { get; set; }
    }
}
