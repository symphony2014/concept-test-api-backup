﻿using Concept.Entity.BaseDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.SysMode
{
    public class ClassifyReportReturn
    {
        public List<string> header { get; set; }
        public List<ClassfiyGroup> groups { get; set; }
    }

    public class ClassfiyGroup
    {
        public string name { get; set; }
        public List<ClassfiyDeviceInfo> device { get; set; }
    }

    public class ClassfiyDeviceInfo
    {
        public string deviceId { get; set; }
        public List<UserDetail> details { get; set; }
        public List<string> duis { get; set; }
    }
}
