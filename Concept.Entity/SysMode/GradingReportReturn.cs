﻿using Concept.Entity.BaseDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.SysMode
{
    public class GradingReportReturn
    {
        public List<string> cards { get; set; }
        public List<string> header { get; set; }
        public List<GradingGroup> groups { get; set; }
    }

    public class GradingGroup
    {
        public string name { get; set; }
        public List<GradingDeviceInfo> device { get; set; }
    }

    public class GradingDeviceInfo
    {
        public string deviceId { get; set; }
        public string OpenText { get; set; }
        public List<UserDetail> details { get; set; }
        public List<string> angles { get; set; }
    }
     
}
