﻿using Concept.Entity.BaseDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.SysMode
{
    public class PaintingReportReturn
    {
        public List<string> header { get; set; }
        public List<PaintingGroup> groups { get; set; }
    }

    public class PaintingGroup
    {
        public string name { get; set; }
        public List<PaintingDeviceInfo> device { get; set; }
    }

    public class PaintingDeviceInfo
    {
        public string deviceId { get; set; }
        public List<UserDetail> details { get; set; }
        public List<PaintingCard> cards { get; set; }
    }

    public class PaintingCard
    {
        public string name { get; set; }
        public string url { get; set; }
        public string detailName { get; set; }
    }
}
