﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity
{
    public class AIParam
    {
        public string ExternalPjId { get; set; }
        public string SampGrpId { get; set; }
        public string TpcId { get; set; }
        public string OpenId { get; set; }
    }
}
