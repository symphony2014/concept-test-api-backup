﻿using ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.BaseDB
{
    [DbTable("AIData")]
    public class AIData
    {
        [DbField("id", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int id { get; set; }

        [DbField("openId", "NVARCHAR", 100)]
        public string openId { get; set; }

        [DbField("username", "NVARCHAR", 50)]
        public string username{ get; set; }

        [DbField("typeId", "INT")]
        public int typeId{ get; set; }


        [DbField("log_id", "BIGINT")]
        public long log_id{ get; set; }
        

        [DbField("prop", "NVARCHAR", 500)]
        public string prop { get; set; }

        [DbField("adj", "NVARCHAR", 100)]
        public string adj { get; set; }

        [DbField("tpcId", "NVARCHAR", 50)]
        public string tpcId{ get; set; }

        [DbField("sampGrpId", "NVARCHAR", 50)]
        public string sampGrpId{ get; set; }

        [DbField("sentiment", "INT")]
        public int sentiment{ get; set; }
        
       
        [DbField("abstract", "NVARCHAR", 200)]
        public string abstractStr{ get; set; }
        [DbField("externalPjId", "NVARCHAR", 200)]
        public string externalPjId{ get; set; }
        [DbField("usericon", "NVARCHAR", 500)]
        public string usericon{ get; set; }
     
    }
}

