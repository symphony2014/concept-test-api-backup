﻿using ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.BaseDB
{
    [Serializable]
    [DbTable("Mapping_ServiceConfig")]
    public class Mapping_ServiceConfig
    {
        public Mapping_ServiceConfig() { }
        #region 字段属性
        protected int _id = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("id", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        protected string _cqModelName = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("cqModelName", "NVARCHAR", 100)]
        public string CqModelName
        {
            get { return _cqModelName; }
            set { _cqModelName = value; }
        }
        protected int _cqModelId = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("cqModelId", "INT")]
        public int CqModelId
        {
            get { return _cqModelId; }
            set { _cqModelId = value; }
        }
        protected string _runningTypeName = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("runningTypeName", "NVARCHAR", 100)]
        public string RunningTypeName
        {
            get { return _runningTypeName; }
            set { _runningTypeName = value; }
        }
        protected int _runningTypeId = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("runningTypeId", "INT")]
        public int RunningTypeId
        {
            get { return _runningTypeId; }
            set { _runningTypeId = value; }
        }
        protected DateTime _addTime;
        ///<summary>
        ///
        ///</summary>
        [DbField("addTime", "DATETIME")]
        public DateTime AddTime
        {
            get { return _addTime; }
            set { _addTime = value; }
        }
        protected int _isDel = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("isDel", "INT")]
        public int IsDel
        {
            get { return _isDel; }
            set { _isDel = value; }
        }
        protected string _implementation = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("implementation", "NVARCHAR", 500)]
        public string Implementation
        {
            get { return _implementation; }
            set { _implementation = value; }
        }
        protected int _runInterval = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("runInterval", "INT")]
        public int RunInterval
        {
            get { return _runInterval; }
            set { _runInterval = value; }
        }
        protected int _threadNumber = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("threadNumber", "INT")]
        public int ThreadNumber
        {
            get { return _threadNumber; }
            set { _threadNumber = value; }
        }
        protected int _isEnable = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("isEnable", "INT")]
        public int IsEnable
        {
            get { return _isEnable; }
            set { _isEnable = value; }
        }
        protected string _expectedStartTime = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("expectedStartTime", "NVARCHAR", 50)]
        public string ExpectedStartTime
        {
            get { return _expectedStartTime; }
            set { _expectedStartTime = value; }
        }
        #endregion
    }
}
