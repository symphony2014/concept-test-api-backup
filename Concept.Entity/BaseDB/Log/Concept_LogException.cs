﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORM;

namespace Concept.Entity.BaseDB.Log
{
    [Serializable]
    [DbTable("Concept_LogException")]
    public class Concept_LogException
    {
        public Concept_LogException() { }			
		#region 字段属性
        protected int _id = 0;
		///<summary>
        ///
        ///</summary>
		[DbField("id", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        protected string _moduleName = string.Empty;
		///<summary>
        ///
        ///</summary>
		[DbField("moduleName", "NVARCHAR", 200)]
        public string ModuleName
        {
            get { return _moduleName; }
            set { _moduleName = value; }
        }
        protected string _exceptMessage = string.Empty;
		///<summary>
        ///
        ///</summary>
		[DbField("exceptMessage", "NVARCHAR", -1)]
        public string ExceptMessage
        {
            get { return _exceptMessage; }
            set { _exceptMessage = value; }
        }
        protected string _exceptStackTrace = string.Empty;
		///<summary>
        ///
        ///</summary>
		[DbField("exceptStackTrace", "NVARCHAR", -1)]
        public string ExceptStackTrace
        {
            get { return _exceptStackTrace; }
            set { _exceptStackTrace = value; }
        }
        protected string _messDescript = string.Empty;
		///<summary>
        ///
        ///</summary>
		[DbField("messDescript", "NVARCHAR", 500)]
        public string MessDescript
        {
            get { return _messDescript; }
            set { _messDescript = value; }
        }
        protected DateTime _addTime;
		///<summary>
        ///
        ///</summary>
		[DbField("addTime", "DATETIME")]
        public DateTime AddTime
        {
            get { return _addTime; }
            set { _addTime = value; }
        }
		#endregion
    }
}
