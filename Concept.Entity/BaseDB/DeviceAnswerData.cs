﻿using System;
using System.Collections.Generic;
using ConceptCommon.ConvertMethod;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ORM;
using Concept.Entity.EnumMode;

namespace Concept.Entity.BaseDB
{
    public class DeviceAnswerData
    {
        public List<DeviceAnswer> answers { get; set; }
    }
}
