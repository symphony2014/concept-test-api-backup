﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCommon.ConvertMethod;
using Newtonsoft.Json;
using ORM;

namespace Concept.Entity.BaseDB
{
    [DbTable("Project")]
    public class Project
    {
        public Project() { }
        #region 字段属性
        protected int _projectAutoId = 0;
        ///<summary>
        ///
        ///</summary> 
        [DbField("projectAutoId", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int ProjectAutoId
        {
            get { return _projectAutoId; }
            set { _projectAutoId = value; }
        }

        protected string _projectId = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("projectId", "VARCHAR", 50)]
        public string ProjectId
        {
            get { return _projectId; }
            set { _projectId = value; }
        }

        protected string _projectName = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("projectName", "NVARCHAR", 50)]
        public string ProjectName
        {
            get { return _projectName; }
            set { _projectName = value; }
        }

        protected string _projectDesc = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("projectDesc", "NVARCHAR", 250)]
        public string ProjectDesc
        {
            get { return _projectDesc; }
            set { _projectDesc = value; }
        }
         
        protected DateTime _createDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("createDate", "DATETIME")]
        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }

        protected int _ownerId = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("ownerId", "INT")]
        public int OwnerId
        {
            get { return _ownerId; }
            set { _ownerId = value; }
        }
        protected string _moduleOrder = "";
        [DbField("ModuleOrder", "NVARCHAR",50)]
        public string ModuleOrder
        {
            get { return _moduleOrder; }
            set { _moduleOrder = value; }
        }


        protected int _isNeedDoodled =0;
        [DbField("IsNeedDoodled", "INT")]
        public int IsNeedDoodled
        {
            get { return _isNeedDoodled; }
            set { _isNeedDoodled = value; }
        }
        #endregion
    }
}
