﻿using ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.BaseDB
{
    [DbTable("PGQualiRuleTopic")]
    public class PGQualiRuleTopic
    {
        public PGQualiRuleTopic() { }
        #region 字段属性
        protected int _autoId = 0;
        ///<summary>
        ///
        ///</summary> 
        [DbField("autoId", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int AutoId
        {
            get { return _autoId; }
            set { _autoId = value; }
        }
         
        protected string _ruleId = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("ruleId", "NVARCHAR", 50)]
        public string RuleId
        {
            get { return _ruleId; }
            set { _ruleId = value; }
        }

        protected string _topicId = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("topicId", "NVARCHAR", 100)]
        public string TopicId
        {
            get { return _topicId; }
            set { _topicId = value; }
        }
         
        #endregion
    }
}
