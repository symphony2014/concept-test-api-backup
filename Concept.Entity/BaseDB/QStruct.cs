﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Concept.Entity.BaseDB
{
   public  class QStruct
    {
        public Variable Variable { get; set; }
        public List<VariableValue2> VariableValues { get; set; }
    }
}