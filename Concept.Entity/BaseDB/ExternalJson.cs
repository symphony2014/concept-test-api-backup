﻿using ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.BaseDB
{
    [DbTable("ExternalJson")]
    public  class ExternalJson
    {
        public ExternalJson() { }
        [DbField("ID", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int ID { get; set; }
        [DbField("JSON", "NVARCHAR", int.MaxValue)]
        public string JSON { get; set; }
        [DbField("ExternalPjId", "NVARCHAR",50)]
        public string ExternalPjId { get; set; }
        [DbField("TopicId", "NVARCHAR", 50)]
        public string TopicId { get; set; }
        [DbField("GroupId", "NVARCHAR", 50)]
        public string GroupId { get; set; }

        [DbField("UpdateTime", "DATETIME")]
        public DateTime UpdateTime { get; set; }
        [DbField("type", "INT")]
        public int type { get; set; }
    }
}
