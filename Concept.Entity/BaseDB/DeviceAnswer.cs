﻿using System;
using System.Collections.Generic;
using ConceptCommon.ConvertMethod;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ORM;
using Concept.Entity.EnumMode;

namespace Concept.Entity.BaseDB
{
    [DbTable("DeviceAnswer")]
    public class DeviceAnswer
    {
        public DeviceAnswer() { }
        #region 字段属性
        protected int _aId = 0;
        ///<summary>
        ///
        ///</summary> 
        [DbField("aId", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int AId
        {
            get { return _aId; }
            set { _aId = value; }
        }
        protected int _projectAutoId = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("projectAutoId", "INT")]
        public int ProjectAutoId
        {
            get { return _projectAutoId; }
            set { _projectAutoId = value; }
        }
        protected int _groupId = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("groupId", "INT")]
        public int GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }

        protected int _cardId = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("cardId", "INT")]
        public int CardId
        {
            get { return _cardId; }
            set { _cardId = value; }
        }

        protected string _deviceId = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("deviceId", "NVARCHAR", 50)]
        public string DeviceId
        {
            get { return _deviceId; }
            set { _deviceId = value; }
        }

        protected DeviceAnswerQuestionTypeEnum _questionType = DeviceAnswerQuestionTypeEnum.Default;
        ///<summary>
        ///
        ///</summary>
        [JsonConverter(typeof(EnumConverter))]
        [DbField("questionType", "INT")]
        public DeviceAnswerQuestionTypeEnum QuestionType
        {
            get { return _questionType; }
            set { _questionType = value; }
        }

        protected int _questionId = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("questionId", "INT")]
        public int QuestionId
        {
            get { return _questionId; }
            set { _questionId = value; }
        }

        protected string _questionAnswer = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("questionAnswer", "NVARCHAR", 350)]
        public string QuestionAnswer
        {
            get { return _questionAnswer; }
            set { _questionAnswer = value; }
        }

        

        protected DateTime _createDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("createDate", "DATETIME")]
        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }

        protected string _appendData = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("appendData", "NVARCHAR", 350)]
        public string AppendData
        {
            get { return _appendData; }
            set { _appendData = value; }
        }
        [DbField("OpenText", "NVARCHAR", 550)]
        public string OpenText { get; set; }

        [DbField("ExternalMessageId", "NVARCHAR", 100)]
        public string ExternalMessageId { get; set; }


        #endregion
    }
}
