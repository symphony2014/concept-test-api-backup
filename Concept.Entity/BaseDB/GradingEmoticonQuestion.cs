﻿using System;
using System.Collections.Generic;
using ConceptCommon.ConvertMethod;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ORM;

namespace Concept.Entity.BaseDB
{
    [DbTable("GradingEmoticonQuestion")]
    public class GradingEmoticonQuestion
    {
        public GradingEmoticonQuestion() { }
        #region 字段属性
        protected int _gradingEmoticonId = 0;
        ///<summary>
        ///
        ///</summary> 
        [DbField("gradingEmoticonId", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int GradingEmoticonId
        {
            get { return _gradingEmoticonId; }
            set { _gradingEmoticonId = value; }
        }
        protected int _projectAutoId = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("projectAutoId", "INT")]
        public int ProjectAutoId
        {
            get { return _projectAutoId; }
            set { _projectAutoId = value; }
        }
        protected int _isShowEmoticon = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("isShowEmoticon", "INT")]
        public int IsShowEmoticon
        {
            get { return _isShowEmoticon; }
            set { _isShowEmoticon = value; }
        }

        protected int _isShowGrading = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("isShowGrading", "INT")]
        public int IsShowGrading
        {
            get { return _isShowGrading; }
            set { _isShowGrading = value; }
        }

        protected int _isShowOpenQuestion = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("isShowOpenQuestion", "INT")]
        public int IsShowOpenQuestion
        {
            get { return _isShowOpenQuestion; }
            set { _isShowOpenQuestion = value; }
        }


        protected string _emoticonDesc = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("emoticonDesc", "NVARCHAR", 150)]
        public string EmoticonDesc
        {
            get { return _emoticonDesc; }
            set { _emoticonDesc = value; }
        }

        protected string _openQuestion = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("openQuestion", "NVARCHAR", 150)]
        public string OpenQuestion
        {
            get { return _openQuestion; }
            set { _openQuestion = value; }
        }
           
        protected DateTime _createDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("createDate", "DATETIME")]
        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }
        protected DateTime _updateDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("updateDate", "DATETIME")]
        public DateTime UpdateDate
        {
            get { return _updateDate; }
            set { _updateDate = value; }
        } 
         
        #endregion
    }
}
