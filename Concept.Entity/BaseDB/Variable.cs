﻿using Newtonsoft.Json;
using ORM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.BaseDB
{
    public enum VarType : byte
    {
        Choice = 1,
        Text = 2
    }
    public enum QuestionType2 : byte
    {
        Unknown = 0,
        Single = 1,
        Multi = 2,
        Opentext = 3,
        MultiOpentext = 4,
        Rating = 5,
        Numbering = 6,
        Ranking = 7,
        Info = 8,
        MatrixSingle = 9,
        MatrixMulti = 10,
        MixGrid = 11,
        Loop = 12,
        Page = 13,
        Picture = 14,
        Position = 15,
        Audio = 16,
        Video = 17,
        Condition = 18,
        Stop = 19,
        Timer = 20,
        Directive = 21,
        Block = 22,
        Assignment = 23,
        Progress = 24,
        End = 25,
        Encode = 26
    }
    public enum ValueType : byte
    {
        Code = 0,
        Char = 1,
        Numerical = 2,
        Date = 3
    }
    [DbTable("Variable")]
    public class Variable
    {
        #region 字段属性
        protected long _varId = 0;
        ///<summary>
        ///
        ///</summary> 
        [DbField("VarId", "BIGINT", ORMType = ORMFieldType.Key | ORMFieldType.Unique)]
        public long VarId
        {
            get { return _varId; }
            set { _varId = value; }
        }

        protected int _index2 = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("Index2", "INT")]
        public int Index2
        {
            get { return _index2; }
            set { _index2 = value; }
        }
        protected string _ID = "";
        ///<summary>
        ///
        ///</summary>
        [DbField("ID", "NVARCHAR", 64)]
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        protected string _externalVarId = "";
        ///<summary>
        ///
        ///</summary>
        [DbField("ExternalVarId", "NVARCHAR", 64)]
        public string ExternalVarId
        {
            get { return _externalVarId; }
            set { _externalVarId = value; }
        }

        protected string columnName = "";
        ///<summary>
        ///
        ///</summary>
        [DbField("ColumnName", "NVARCHAR", 64)]
        public string ColumnName
        {
            get { return columnName; }
            set { columnName = value; }
        }

        protected String code = "";
        ///<summary>
        ///
        ///</summary>
        [DbField("Code", "VARCHAR", 256)]
        public String Code
        {
            get { return code; }
            set { code = value; }
        }
     
        protected VarType _varType = VarType.Choice;
        ///<summary>
        ///
        ///</summary>
        [DbField("VarType", "TINYINT")]
        public VarType VarType
        {
            get { return _varType; }
            set { _varType = value; }
        }

     
        protected ValueType _valueType = ValueType.Code;
        ///<summary>
        ///
        ///</summary>
        [DbField("ValueType", "TINYINT")]
        public ValueType ValueType
        {
            get { return _valueType; }
            set { _valueType = value; }
        }

        protected long _pjId = 0;
        ///<summary>
        ///
        ///</summary>

        [DbField("PjId", "BIGINT")]
        public long PjId
        {
            get { return _pjId; }
            set { _pjId = value; }
        }

        protected String _text = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("Text", "NVARCHAR", 2048)]
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        protected QuestionType2 quesTypeId = QuestionType2.Multi;
        ///<summary>
        ///
        ///</summary>
        [DbField("QuesTypeId", "TINYINT")]
        public QuestionType2 QuesTypeId
        {
            get { return quesTypeId; }
            set { quesTypeId = value; }
        }



        protected DateTime _createDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("CreateDate", "DATETIME")]
        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }

        protected long _rowId = 0;
        [DbField("RowId", "BIGINT")]
        public long RowId
        {
            get { return _rowId; }
            set { _rowId = value; }
        }


        #endregion
    }
}
