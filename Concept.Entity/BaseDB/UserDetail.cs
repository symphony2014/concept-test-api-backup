﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.BaseDB
{

    public class UserDetail
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    public class MapData
    {
        public string userIcon { get; set; }
        public string NickName { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string City { get; set; }
        public string Age { get; set; }
    }
    public class UserMap
    {
        public List<UserDetail> pairs { get; set; }
        public MapData map { get {
                if (pairs == null || pairs.Count == 0) { return new MapData(); }
                Func<string, UserDetail> f = key =>
                 {
                     var def = pairs.Where(p => p.Key == key).FirstOrDefault();
                     if (def == null)
                     {
                         return new UserDetail { Key = "", Value = "" };
                     }
                     return def;
                 };
                MapData mapData = new MapData();
                mapData.userIcon = f("微信头像").Value;
                mapData.NickName = f("昵称").Value;
                mapData.Name = f("姓名").Value;
                mapData.Gender = f("性别").Value;
                mapData.City = f("城市").Value;
                mapData.Age = f("年龄").Value;
                return mapData;
            } }
    }
}
