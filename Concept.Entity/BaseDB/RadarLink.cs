﻿using ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.BaseDB
{
    [DbTable("RadarLink")]
    public class RadarLink
    {

    

        [DbField("Link", "NVARCHAR", 500)]
        public string Link { get; set; }

        [DbField("OpenId", "NVARCHAR", 100)]
        public string OpenId { get; set; }

        [DbField("Mobile", "NVARCHAR", 50)]
        public string Mobile { get; set; }

        [DbField("Id", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int Id { get; set; }

        [DbField("IsUsed", "INT")]
        public int IsUsed { get; set; }

        [DbField("Status","NVARCHAR",50)]
        public string Status { get; set; }

        [DbField("ExternalPjId", "NVARCHAR", 50)]
        public string ExternalPjId { get; set; }

        [DbField("GroupId", "NVARCHAR", 50)]
        public string GroupId { get; set; }

        [DbField("SurveryId", "NVARCHAR", 50)]
        public string SurveryId { get; set; }

        [DbField("LinkType", "INT")]
        public int LinkType { get; set; }

        [DbField("ReturnUrl", "NVARCHAR", 200)]
        public string ReturnUrl { get; set; }

        [DbField("externalUserId", "NVARCHAR", 50)]
        public string externalUserId { get; set; }

        [DbField("messageId", "NVARCHAR", 50)]
        public string messageId { get; set; }

        public string PjId { get; set; }
    }
}
