﻿
using ORM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Concept.Entity.BaseDB
{
    [DbTable("AdoptAnswer")]
    public class AdoptAnswer
    {
        public AdoptAnswer() { }

        protected int _ID = 0;
        [DbField("Id", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int Id
        {
            get { return _ID; }
            set { _ID = value; }
        }

        protected long _pjId = 0;
        [DbField("PjId", "BIGINT")]
        public long PjId
        {
            get { return _pjId; }
            set { _pjId = value; }
        }

        protected long _cardId = 0;
        [DbField("CardId", "INT")]
        public long CardId
        {
            get { return _cardId; }
            set { _cardId = value; }
        }

        protected long _questionId = 0;
        [DbField("QuestionId", "INT")]
        public long QuestionId
        {
            get { return _questionId; }
            set { _questionId = value; }
        }
        protected string _Value = string.Empty;
        [DbField("Value", "NVARCHAR", 500)]
        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
        [DbField("OpenId", "NVARCHAR", 50)]
        public string OpenId { get; set; }

        [DbField("ExternalPjId", "NVARCHAR", 50)]
        public string ExternalPjId { get; set; }

        [DbField("GroupId", "NVARCHAR", 50)]
        public string GroupId { get; set; }

        [DbField("SuvId", "NVARCHAR", 50)]
        public string SuvId { get; set; }


        [DbField("SubType", "INT")]
        public int SubType { get; set; }
        [DbField("ExternalMessageId", "NVARCHAR", 100)]
        public string ExternalMessageId { get; set; }
    }
}