﻿using System;
using System.Collections.Generic;
using ConceptCommon.ConvertMethod;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ORM;

namespace Concept.Entity.BaseDB
{
    [DbTable("EmoticonQuestionOption")]
    public class EmoticonQuestionOption
    {
        public EmoticonQuestionOption() { }
        #region 字段属性
        protected int _emoticonOptionId = 0;
        ///<summary>
        ///
        ///</summary> 
        [DbField("emoticonOptionId", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int EmoticonOptionId
        {
            get { return _emoticonOptionId; }
            set { _emoticonOptionId = value; }
        }
        protected int _projectAutoId = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("projectAutoId", "INT")]
        public int ProjectAutoId
        {
            get { return _projectAutoId; }
            set { _projectAutoId = value; }
        }
        protected int _orderId = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("orderId", "INT")]
        public int OrderId
        {
            get { return _orderId; }
            set { _orderId = value; }
        }

        protected int _isShow = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("isShow", "INT")]
        public int IsShow
        {
            get { return _isShow; }
            set { _isShow = value; }
        }

        protected int _score = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("score", "INT")]
        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }

        protected int _iconId = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("iconId", "INT")]
        public int IconId
        {
            get { return _iconId; }
            set { _iconId = value; }
        }

        protected string _iconDesc = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("iconDesc", "NVARCHAR", 150)]
        public string IconDesc
        {
            get { return _iconDesc; }
            set { _iconDesc = value; }
        }
           
        protected DateTime _createDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("createDate", "DATETIME")]
        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }
        protected DateTime _updateDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("updateDate", "DATETIME")]
        public DateTime UpdateDate
        {
            get { return _updateDate; }
            set { _updateDate = value; }
        } 
         
        #endregion
    }
}
