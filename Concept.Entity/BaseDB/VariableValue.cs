﻿using Newtonsoft.Json;
using ORM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.BaseDB
{

    [DbTable("VariableValue")]
    public class VariableValue2
    {
        public VariableValue2() { }

        #region 字段属性
        protected long _varValueId = 0;
        ///<summary>
        ///
        ///</summary> 
        [DbField("VarValueId", "BIGINT", ORMType = ORMFieldType.Key | ORMFieldType.Unique)]
        public long VarValueId
        {
            get { return _varValueId; }
            set { _varValueId = value; }
        }

        protected String code = "";
        ///<summary>
        ///
        ///</summary>
        [DbField("Code", "VARCHAR", 256)]
        public String Code
        {
            get { return code; }
            set { code = value; }
        }



        protected long _varId = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("VarId", "BIGINT")]
        public long VarId
        {
            get { return _varId; }
            set { _varId = value; }
        }

        protected long _pjId = 0;
        ///<summary>
        ///
        ///</summary>

        [DbField("PjId", "BIGINT")]
        public long PjId
        {
            get { return _pjId; }
            set { _pjId = value; }
        }

        protected String _text = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("Text", "NVARCHAR", 2048)]
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }





        protected DateTime _createDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("CreateDate", "DATETIME")]
        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }
        #endregion 
    }
}
