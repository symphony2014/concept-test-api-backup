﻿using ConceptCommon.ConvertMethod;
using Newtonsoft.Json;
using ORM;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.BaseDB
{
    [DbTable("PGRuleInfo")]
    public class PGRuleInfo
    {
        public PGRuleInfo() { }
        #region 字段属性
        protected int _pGRuleAutoId = 0;
        ///<summary>
        ///
        ///</summary> 
        [DbField("pGRuleAutoId", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int PGRuleAutoId
        {
            get { return _pGRuleAutoId; }
            set { _pGRuleAutoId = value; }
        }

        protected string _externalPjId = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("externalPjId", "NVARCHAR", 50)]
        public string ExternalPjId
        {
            get { return _externalPjId; }
            set { _externalPjId = value; }
        }

        protected string _groupId = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("groupId", "NVARCHAR", 50)]
        public string GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }

        protected string _ruleId = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("ruleId", "NVARCHAR", 50)]
        public string RuleId
        {
            get { return _ruleId; }
            set { _ruleId = value; }
        }

        protected string _srvId = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("srvId", "NVARCHAR", 100)]
        public string SrvId
        {
            get { return _srvId; }
            set { _srvId = value; }
        }

        protected int _ruleType = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("ruleType", "INT")]
        public int RuleType
        {
            get { return _ruleType; }
            set { _ruleType = value; }
        }
        protected DateTime _createDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("createDate", "DATETIME")]
        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }

        protected int _isUse = 1;
        ///<summary>
        ///
        ///</summary>
        [DbField("isUse", "INT")]
        public int IsUse
        {
            get { return _isUse; }
            set { _isUse = value; }
        }
        #endregion
    }
}
