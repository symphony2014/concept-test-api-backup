﻿using System;
using System.Collections.Generic;
using ConceptCommon.ConvertMethod;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ORM;

namespace Concept.Entity.BaseDB
{
    [DbTable("OrderQuestion")]
    public class OrderQuestion
    {
        public OrderQuestion() { }
        #region 字段属性
        protected int _orderQuestionId = 0;
        ///<summary>
        ///
        ///</summary> 
        [DbField("orderQuestionId", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int OrderQuestionId
        {
            get { return _orderQuestionId; }
            set { _orderQuestionId = value; }
        }
        protected int _projectAutoId = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("projectAutoId", "INT")]
        public int ProjectAutoId
        {
            get { return _projectAutoId; }
            set { _projectAutoId = value; }
        }
        protected int _orderId = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("orderId", "INT")]
        public int OrderId
        {
            get { return _orderId; }
            set { _orderId = value; }
        }


        protected string _orderQuestionText = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("orderQuestionText", "NVARCHAR", 150)]
        public string OrderQuestionText
        {
            get { return _orderQuestionText; }
            set { _orderQuestionText = value; }
        }
         
        protected DateTime _createDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("createDate", "DATETIME")]
        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }
        protected DateTime _updateDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("updateDate", "DATETIME")]
        public DateTime UpdateDate
        {
            get { return _updateDate; }
            set { _updateDate = value; }
        }
        protected string _openText = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("OpenText", "NVARCHAR", 550)]
        public string OpenText
        {
            get { return _openText; }
            set { _openText = value; }
        }
        #endregion
    }
}
