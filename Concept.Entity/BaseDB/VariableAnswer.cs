﻿
using ORM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Concept.Entity.BaseDB
{
    [DbTable("VariableAnswer")]
    public  class VariableAnswer
    {
        public VariableAnswer() { }

        protected int _ID = 0;
        [DbField("ID", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int ID {
            get { return _ID; }
            set { _ID = value; }
        }

        protected long _VarId = 0;
        [DbField("VarId", "BIGINT")]
        public long varId
        {
            get { return _VarId; }
            set { _VarId = value; }
        }

        protected string _Value = string.Empty;
        [DbField("Value", "NVARCHAR",500)]
        public string Value {
            get { return _Value; }
            set { _Value = value; }
        }
        [DbField("OpenId", "NVARCHAR", 500)]
        public string OpenId { get; set; }
    }
}