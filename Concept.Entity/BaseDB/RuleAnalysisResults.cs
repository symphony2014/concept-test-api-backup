﻿using ConceptCommon.ConvertMethod;
using Newtonsoft.Json;
using ORM;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.BaseDB
{
    [DbTable("RuleAnalysisResults")]
    public class RuleAnalysisResults
    {
        public RuleAnalysisResults() { }
        #region 字段属性
        protected int _analysisAutoId = 0;
        ///<summary>
        ///
        ///</summary> 
        [DbField("analysisAutoId", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int AnalysisAutoId
        {
            get { return _analysisAutoId; }
            set { _analysisAutoId = value; }
        }
         
        protected string _externalPjId = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("externalPjId", "NVARCHAR", 50)]
        public string ExternalPjId
        {
            get { return _externalPjId; }
            set { _externalPjId = value; }
        }

        protected string _originalGroupId = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("originalGroupId", "NVARCHAR", 50)]
        public string OriginalGroupId
        {
            get { return _originalGroupId; }
            set { _originalGroupId = value; }
        }

        protected string _ruleId = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("ruleId", "NVARCHAR", 50)]
        public string RuleId
        {
            get { return _ruleId; }
            set { _ruleId = value; }
        }

        protected string _openId = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("openId", "NVARCHAR", 100)]
        public string OpenId
        {
            get { return _openId; }
            set { _openId = value; }
        }

        protected string _gotoGroupId = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("gotoGroupId", "NVARCHAR", 50)]
        public string GotoGroupId
        {
            get { return _gotoGroupId; }
            set { _gotoGroupId = value; }
        }
        protected DateTime _createDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("createDate", "DATETIME")]
        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        } 
        #endregion
    }
}
