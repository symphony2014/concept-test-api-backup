﻿using Newtonsoft.Json;
using ORM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.BaseDB
{

    [DbTable("Message")]
    public class Message
    {
        public Message() { }

        #region 字段属性

        ///<summary>
        ///
        ///</summary> 
        [DbField("id", "NVARCHAR", 50)]
        public string id { get; set; }
        [DbField("crtTm", "DATETIME")]
        public string crtTm { get; set; }
        [DbField("crtPsn", "NVARCHAR", 50)]
        public string crtPsn { get; set; }
        [DbField("udtTm", "NVARCHAR", 50)]
        public string udtTm { get; set; }
        [DbField("delInd", "NVARCHAR", 50)]
        public string delInd { get; set; }
        [DbField("sampGrpId", "NVARCHAR", 50)]
        public string sampGrpId { get; set; }
        [DbField("ctTp", "NVARCHAR", 50)]
        public string ctTp { get; set; }
        [DbField("tpcId", "NVARCHAR", 50)]
        public string tpcId { get; set; }
        [DbField("sndId", "NVARCHAR", 50)]
        public string sndId { get; set; }
        [DbField("rcvId", "NVARCHAR", 50)]
        public string rcvId { get; set; }
        [DbField("msgCntnt", "NVARCHAR", int.MaxValue)]
        public string msgCntnt { get; set; }
        [DbField("msgTsCntnt", "NVARCHAR", int.MaxValue)]
        public string msgTsCntnt { get; set; }
        [DbField("msgTp", "NVARCHAR", 50)]
        public string msgTp { get; set; }
        [DbField("url", "NVARCHAR", 50)]
        public string url { get; set; }
        [DbField("ExternalPjId", "NVARCHAR", 50)]
        public string ExternalPjId { get; set; }
        [DbField("MessageId", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int MessageId { get; set; }

        [DbField("openId","NVARCHAR",200)]
        public string openId { get; set; }

        public string username { get; set; }

        [DbField("usericon", "NVARCHAR", 500)]
        public string usericon { get; set; }

        [DbField("nckNm", "NVARCHAR", 50)]
        public string nckNm { get; set; }

        public string phone { get; set; }
        #endregion 
    }
}
