﻿using Concept.Entity.BaseDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.External
{
  
    public class data<T> {
        public List<T> rows { get; set; }
        public int total { get; set; }
    }
}
