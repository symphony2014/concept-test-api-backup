﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.External
{
   public class eTopic
    {
        public string id { get; set; }
        public string tpcNm { get; set; }
        public string dscInf { get; set; }
        public string sampGrpId { get; set; }
    }
}
