﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.External
{
      public class ZipParam
    {
        public string ExternalPjId { get; set; }
        public string GroupId { get; set; }
        public string TopicId { get; set; }
        public string DataUrl { get; set; }
    }
}
