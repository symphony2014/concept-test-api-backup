﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.External
{
   public class ExternalMessageLite
    {
        public string groupName { get; set; }
        public string TopicName { get; set; }
        public string SendUser { get; set; }
        public string Content { get; set; }
        public string ContentUrl { get; set; }
        public string Date { get; set; }
    }
}
