﻿namespace Concept.Entity.External
{
    public  class ExternalStatus
    {
        public string statusCode { get; set; }
        public string errMsg { get; set; }
        public string data { get; set; }
    }
}