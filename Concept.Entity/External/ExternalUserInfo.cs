﻿using System.Collections.Generic;

namespace Concept.Entity.External
{
    public class ExternalUserInfo
    {
      public  static string[][] convertMapping = new string[][] {
            new string[] { "userName", "name" },
            new string[] { "openId","ID" },
            new string[] { "phone", "mobile" }, 
             new string[] { "sex", "gender" },
             new string[]{ "usrAge", "age"},
             new string[]{"usrAgeRange","agescope"},
             new string[]{ "urbnLvlCd", "ctier" },
             new string[]{ "city", "city" },
             new string[]{ "eddgrCd", "degree" },
             new string[]{ "idyTpcd", "industry" },
             //new string[]{ "wrkUnitNm", "company" },
             //new string[]{ "pstnNm", "position" },
             //new string[]{ "idvMoIncmam", "pincome" },
             //new string[]{ "famMoIncmam", "fincome" },
             //new string[]{ "famPpnNum", "fcount" },
             //new string[]{ "famPercptaIncmam", "faincome" },
             //new string[]{ "lclRsdncTm", "livetime" },
             new string[] { "marSttnCd", "marriage" },
        };
        public string prjId { get; set; }
        public string userName { get; set; }
        public string openId { get; set; }
        public string phone { get; set; }
        public string idcardNo { get; set; }
        public string sex { get; set; }
        public string usrAge { get; set; }
        public string urbnLvlCd { get; set; }

        public string city { get; set; }

        public string eddgrCd { get; set; }

        public string idyTpcd { get; set; }

        public string wrkUnitNm { get; set; }

        public string pstnNm { get; set; }

        public string idvMoIncmam { get; set; }

        public string famMoIncmam { get; set; }

        public string famPpnNum { get; set; }

        public string famPercptaIncmam { get; set; }

        public string lclRsdncTm { get; set; }

        public string marSttnCd { get; set; }
        public string usrAgeRange{get;set;}

        public List<Other> jsonData { get; set; }
        
    }

    public class Other
    {
        public string key { get; set; }
        public string value { get; set; }

    }
}