﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.External
{
   public class  eConceptProject 
    {
        public string id { get; set; }
        public string  mtrlSrccd{ get; set; }
        public string qstnrId { get; set; }
        public string ttlCntnt { get; set; }
    }
}
