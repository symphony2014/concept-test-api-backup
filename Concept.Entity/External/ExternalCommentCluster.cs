﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.External
{
 
    public class ExternalCluster
    {
        public int positive { get; set; }
        public int negative { get; set; }
        public int middle { get; set; }
        public string type { get; set; }
        public int allCount { get; set; }

    }
}
