﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.External
{
    public class row
    {
        public string id { get; set; }
        public string crtTm { get; set; }
        public string crtPsn { get; set; }
        public string udtTm { get; set; }
        public string delInd { get; set; }
        public string prjNo { get; set; }
        public string prjNm { get; set; }
        public string sttm { get; set; }
        public string edtm { get; set; }
        public string lvShwInd { get; set; }
        public string lvShwAdr { get; set; }
        public string  prjDsc{ get; set; }
    }
    public class ProjectData
    {
        public int total { get; set; }
        public List<row> rows { get; set; }
    }
    public  class ExternalProject
    {
        public int statuscode { get; set; }
        public string errMsg { get; set; }

        public ProjectData data { get; set; }
    }
   public class ExternalProjectParam
    {
        public int nowpage { get; set; }
        public int pagesize { get; set; }
    }
}
