﻿using Concept.Entity.BaseDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.External
{
   public class ExternalMessage<T>
    {
        public string statusCode { get; set; }
        public string errMsg { get; set; }
        public data<T>  data { get; set; }
    }
}
