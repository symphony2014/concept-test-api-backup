﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.External
{
    public class idx
    {
        public string id { get; set; }
        public int value { get; set; }
    }
    public class Comment
    {
        public string userName { get; set; }
        public string comment { get; set; }
        public string userIcon { get; set; }
    }
    /// <summary>
    /// 词云数据类
    /// </summary>
    public class ExternalAIData
    {
        public List<idx> ids { get; set; }
        public List<Comment> comments { get; set; }
        public IEnumerable<ExternalCluster> clusters { get; set; }

        public bool HasData {
            get {
                if (ids != null && comments != null && clusters != null) {
                    if(ids.Count>0 || comments.Where(c=>!string.IsNullOrWhiteSpace(c.comment)).Count()>0 ||  clusters.Count() > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
        }
    }
}
