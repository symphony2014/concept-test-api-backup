﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.BLL.OrmCommon
{
    public class GetDbTimeHelper
    {
        private static TimeSpan _offsetWithDB = default(TimeSpan);

        protected static TimeSpan OffsetWithDB
        {
            get
            {
                return _offsetWithDB;
            }
        }

        public static void InitialDBTimeOffset()
        {
            if (_offsetWithDB.Equals(default(TimeSpan)))
            {
                //DateTime dbTime = FunLayer.Transform.Time(CaptureTask_CaptureConfigBLL.Instance.GetColumn("GETDATE()"));
                //_offsetWithDB = DateTime.Now - dbTime;
                //if (_offsetWithDB.Equals(default(TimeSpan)))
                //{
                //    //  如果毫无时差，默认当前时间比数据库时间慢1毫秒误差。
                //    _offsetWithDB = new TimeSpan(0, 0, 0, 0, 1);
                //}
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public static DateTime GetTime()
        {
            DateTime time = DateTime.Now.Add(OffsetWithDB);
            return time;
        }
    }
}
