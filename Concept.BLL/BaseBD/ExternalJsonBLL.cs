﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using Concept.Entity.EnumMode;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class ExternalJsonBLL: BLL<ExternalJson>
    {
        #region 定义
		private static object lockObject = new object();
        protected ExternalJsonBLL()
            : base(new ExternalJsonDAL())
        {
        }
        private static volatile ExternalJsonBLL _instance;
        public static ExternalJsonBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new ExternalJsonBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(ExternalJson item)
        {
            return Transform.Int(Instance.Add(item));
        }

        public static int UpdateItem(ExternalJson Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }

        public static ExternalJson GetVarByText(string columnName, string externalPjId)
        {
            var item = Instance.Where("ExternalVarId=@externalPjId and columnName=@columnName").Parms(externalPjId, columnName).Get();
            return item;
        
        }

        public static ExternalJson GetById(long varId)
        {
          return   Instance.Where("varId=@varId").Parms(varId).Get();
        }
        /// <summary>
        /// 删除存在的message(根据groupId,externalPjId,topicId)
        /// </summary>
        /// <param name="externalPjId"></param>
        /// <param name="groupId"></param>
        /// <param name="topicId"></param>
        public static void RemoveBy(string externalPjId,string groupId,string topicId)
        {
            var items = Instance.Where("externalPjId=@externalPjId and groupId=@groupId and topicId=@topicId").Parms(externalPjId,groupId,topicId);
            var query = SqlQuery.CreateByQuery(items);
            var list = items.ListAll();
            foreach (var item in list)
            {
                Instance.Del(item);
            }
          
        }
        /// <summary>
        /// 
        /// 当groupid==-1, 或者topic==-1 返回的数据为全局数据。
        /// </summary>
        /// <param name="pjId"></param>
        /// <param name="groupId"></param>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public static ExternalJson GetWordCloudData(string pjId, string groupId="-1", string topicId="-1")
        {
           string where = "externalPjId=@externalPjId and groupId=@groupId ";

            //if (topicId != "-1")
            //{
                where += "and topicId=@topicId";
                var result = Instance.Where(where).Parms(pjId, groupId, topicId).ListAll();
            return result.OrderByDescending(json => json.UpdateTime).FirstOrDefault();
            //}else 
            //{
            //    where += " and type=@type ";
            //    return Instance.Where(where).Parms(pjId, groupId, Convert.ToInt32(type)).ListAll();
            //}
          
        }


        #endregion
    }
}
