﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class AIDataBLL: BLL<AIData>
    {
        #region 定义
		private static object lockObject = new object();
        protected AIDataBLL()
            : base(new AIDataDAL())
        {
        }
        private static volatile AIDataBLL _instance;
        public static AIDataBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new AIDataBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(AIData item)
        {
            return Transform.Int(Instance.Add(item));
        }
        public static bool Remove(string externalPjId,string sampGrpId,string tpcId)
        {
            string param = "externalPjId=@externalPjId and sampGrpId=@sampGrpId";
            if (tpcId != "-1")
            {
                param += " and tpcId=@tpcId ";
            }
            var list=Instance.Where(param).Parms(externalPjId,sampGrpId,tpcId).ListAll();
            return   Instance.BatchDel(list);
            //return Transform.Int();
        }
        
        public static List<AIData> Search(AIParam param)
        {
            string where = "1=1 ";
            if (param.ExternalPjId != "-1")
                where += " and  externalpjid="+param.ExternalPjId;
            if(param.SampGrpId!="-1")
                where += " and  sampgrpId="+param.SampGrpId;

            if (param.TpcId != "-1")
                where += " and  tpcId="+param.TpcId;
            if (param.OpenId != "-1")
                where += " and openId=" + param.OpenId ;

            return Instance.Where(where).ListAll();
        }
     
        #endregion
    }
}
