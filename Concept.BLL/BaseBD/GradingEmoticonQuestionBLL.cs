﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class GradingEmoticonQuestionBLL: BLL<GradingEmoticonQuestion>
    {
        #region 定义
		private static object lockObject = new object();
        protected GradingEmoticonQuestionBLL()
            : base(new GradingEmoticonQuestionDAL())
        {
        }
        private static volatile GradingEmoticonQuestionBLL _instance;
        public static GradingEmoticonQuestionBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new GradingEmoticonQuestionBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(GradingEmoticonQuestion item)
        {
            return Transform.Int(Instance.Add(item));
        }

        public static int UpdateItem(GradingEmoticonQuestion Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }

        public static GradingEmoticonQuestion GetGradingEmoticonQuestion(int projectAutoId)
        {
            var list = new GradingEmoticonQuestion();
            list = Instance.Where("ProjectAutoId=@projectAutoId").Parms(projectAutoId).Get();
            return list;
        } 
        #endregion
    }
}
