﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.BLL.OrmCommon;
using Concept.DAL.BaseDB.Log;
using Concept.Entity.BaseDB.Log;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD.Log
{
    public class Concept_LogExceptionBLL: BLL<Concept_LogException>
    {
        #region 定义
        private static object lockObject = new object();
        protected Concept_LogExceptionBLL()
            : base(new Concept_LogExceptionDAL())
        {
        }
        private static volatile Concept_LogExceptionBLL _instance;
        public static Concept_LogExceptionBLL Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new Concept_LogExceptionBLL());
                    }
                }
                return _instance;
            }
        }
        #endregion

        #region 方法
        /// <summary>
        /// 插入数据库
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static int InsertLogExceptionInfo(Concept_LogException model, string[] cols = null)
        {
            model.AddTime = DateTime.Now;
            return Transform.Int(Instance.Add(model, cols));
        }

        /// <summary>
        /// 数据库插入数据返回ID
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Concept_LogException InsertLogLogExceptionAndGetModel(Concept_LogException model)
        {
            if (model.AddTime.Equals(default(DateTime)))
            {
                model.AddTime = GetDbTimeHelper.GetTime();
            }
            int idValue = Transform.Int(Instance.Add(model));
            model.ID = idValue;
            return model;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="num"></param>
        /// <param name="start"></param>
        /// <returns></returns>
        public static List<Concept_LogException> GetLogExceptionListByDate(string startTime, int PageSize, int PageNum, out int totalCount)
        {
            if (PageNum < 0 || PageSize < 0)
            {
                throw new ArgumentException("The num and start parameter should be larger than zero.");
            }

            var q = GenerateGetLogExceptionListByDateQuery(startTime);
             
            totalCount = q.Count();
            if (totalCount == 0)
            {
                return new List<Concept_LogException>();
            }

            List<Concept_LogException> result = q.List(PageSize, (PageNum - 1) * PageSize);
            return result;
        }

        public static Query GenerateGetLogExceptionListByDateQuery(string startTime)
        {
            DateTime time;
            DateTime.TryParse(startTime, out time);
            if (startTime.Equals(default(DateTime)))
            {
                throw new ArgumentNullException("startTime");
            }

            var q = Instance.Where("AddTime>=@startTime").Parms(time).OrderBy("AddTime desc");
            return q;
        }

        #endregion
    }
}
