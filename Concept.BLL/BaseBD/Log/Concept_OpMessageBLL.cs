﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.BLL.OrmCommon;
using Concept.DAL.BaseDB.Log;
using Concept.Entity.BaseDB.Log;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD.Log
{
    public class Concept_OpMessageBLL: BLL<Concept_OpMessage>
    {
		#region 定义
		private static object lockObject = new object();
        protected Concept_OpMessageBLL()
            : base(new Concept_OpMessageDAL())
        {
        }
        private static volatile Concept_OpMessageBLL _instance;
        public static Concept_OpMessageBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new Concept_OpMessageBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法

        public static int AddOpMessage(Concept_OpMessage model, string[] cols=null)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            model.UpdateTime = GetDbTimeHelper.GetTime();
            if (cols != null && cols.Length > 0)
            {
                List<string> colsTemp = new List<string>(cols);
                if (!colsTemp.Any(item => "UpdateTime".Equals(item, StringComparison.OrdinalIgnoreCase)))
                {
                    colsTemp.Add("UpdateTime");
                    cols = colsTemp.ToArray<string>();
                }
            }

            int idValue = Transform.Int(Instance.Add(model, cols));
            return idValue;
        }

        #endregion
	}
}
