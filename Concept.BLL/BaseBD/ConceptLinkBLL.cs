﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using ConceptCommon;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class ConceptLinkBLL : BLL<ConceptLink>
    {
        #region 定义
		private static object lockObject = new object();
        protected ConceptLinkBLL()
            : base(new ConceptLinkDAL())
        {
        }
        private static volatile ConceptLinkBLL _instance;
        public static ConceptLinkBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new ConceptLinkBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(ConceptLink item)
        {
            return Transform.Int(Instance.Add(item));
        }

        public static int UpdateItem(ConceptLink Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }
    
     
  


        public static ConceptLink GetConceptLinkByGroupOpenIdOrMobile(string suvId, string externalpjId, string groupId, string openId)
        {
            var list = new List<ConceptLink>();
            var result = Instance.Where("SurveryId=@suvId and externalpjId=@externalpjId and GroupId=@groupId and OpenId=@OpenId").Parms(suvId, externalpjId, groupId, openId);
            if (result.ListAll().Count > 0)
                return result.ListAll().FirstOrDefault();
            else
            {
                return null;
            }
        }

        public static string GetConceptLinkByGroupOpenIdOrMobileByAppend(string appendData)
        {

            var concept = GetConceptLinkByGroupOpenIdOrMobile(MapGetUtil.MapGet(appendData,"suvId"), MapGetUtil.MapGet(appendData, "externalpjId"), MapGetUtil.MapGet(appendData, "groupId"), MapGetUtil.MapGet(appendData, "openId"));
            if (concept != null)
            {
                return concept.ReturnUrl;
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion
    }
}
