﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class GroupBLL: BLL<Group>
    {
        #region 定义
		private static object lockObject = new object();
        protected GroupBLL()
            : base(new GroupDAL())
        {
        }
        private static volatile GroupBLL _instance;
        public static GroupBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new GroupBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(Group item)
        {
            return Transform.Int(Instance.Add(item));
        }

        public static int UpdateItem(Group Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }

        public static bool DelItemByGroupId(int groupId)
        {
            var item = Instance.Where("GroupId=@groupId").Parms(groupId).Get();
            if (item != null)
            {
                return Instance.Del(item);
            }
            return false;
        }
          
        public static List<Group> GetGroupList(int projectAutoId)
        {
            var list = new List<Group>();
            list = Instance.Where("ProjectAutoId=@projectAutoId").Parms(projectAutoId).ListAll();
            return list;
        }
         
        #endregion
    }
}
