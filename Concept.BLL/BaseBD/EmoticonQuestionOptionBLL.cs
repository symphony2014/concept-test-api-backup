﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class EmoticonQuestionOptionBLL : BLL<EmoticonQuestionOption>
    {
        #region 定义
		private static object lockObject = new object();
        protected EmoticonQuestionOptionBLL()
            : base(new EmoticonQuestionOptionDAL())
        {
        }
        private static volatile EmoticonQuestionOptionBLL _instance;
        public static EmoticonQuestionOptionBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new EmoticonQuestionOptionBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static EmoticonQuestionOption get(string id)
        {
            return Instance.Where("EmoticonOptionId=@id").Parms(id).Get();
        }
        public static bool addItemList(List<EmoticonQuestionOption> Model)
        {
            return Instance.BatchAdd(Model);
        }    
        public static int UpdateItem(EmoticonQuestionOption Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }
        public static bool UpdateItemList(List<EmoticonQuestionOption> Model, string[] Cols = null)
        {
            return Instance.BatchUpdate(Model, Cols);
        }

        public static List<EmoticonQuestionOption> GetEmoticonQuestionOptionList(int projectAutoId)
        {
            var list = new List<EmoticonQuestionOption>();
            list = Instance.Where("ProjectAutoId=@projectAutoId").Parms(projectAutoId).OrderBy("CreateDate").ListAll();
            return list;
        }
        #endregion
    }
}
