﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using ConceptCommon;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class AdoptAnswerBLL : BLL<AdoptAnswer>
    {
        #region 定义
		private static object lockObject = new object();
        protected AdoptAnswerBLL()
            : base(new AdoptAnswerDAL())
        {
        }
        private static volatile AdoptAnswerBLL _instance;
        public static AdoptAnswerBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new AdoptAnswerBLL());
                    }
                }
                return _instance;
            }
        }
        #endregion
        public class AdoptEqual : IEqualityComparer<AdoptAnswer>
        {
            public bool Equals(AdoptAnswer x, AdoptAnswer y)
            {
                return x.OpenId == y.OpenId;
            }

            public int GetHashCode(AdoptAnswer obj)
            {
                return obj.ToString().ToLower().GetHashCode();
            }
        }
        #region 方法 
        public static int AddItem(AdoptAnswer item)
        {
            return Transform.Int(Instance.Add(item));
        }
        public static List<AdoptAnswer> GetUserData(string openId,string mobile)
        {
            List<AdoptAnswer> answers = Instance.Where("openId=@openId or openId=@mobile").Parms(openId,mobile).ListAll();
            return answers;
        }
        public static int UpdateItem(AdoptAnswer Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }
        public static int SaveFromDeviceAnswer(DeviceAnswer r)
        {
            AdoptAnswer adoptAnswer = new AdoptAnswer();
            adoptAnswer.PjId = r.ProjectAutoId;
            adoptAnswer.CardId = r.CardId;
            adoptAnswer.SubType = (int)(r.QuestionType);
            adoptAnswer.QuestionId = r.QuestionId;
            adoptAnswer.GroupId = MapGetUtil.MapGet(r.AppendData, "GroupId");
            adoptAnswer.OpenId = MapGetUtil.MapGet(r.AppendData, "OpenId");
            adoptAnswer.ExternalPjId = MapGetUtil.MapGet(r.AppendData, "ExternalPjId");
            adoptAnswer.SuvId = MapGetUtil.MapGet(r.AppendData, "SuvId");
            adoptAnswer.Value = r.QuestionAnswer;
            adoptAnswer.ExternalMessageId = r.ExternalMessageId;
            if(r.QuestionAnswer.IndexOf(',')>-1)
            {
                string[] values = r.QuestionAnswer.Split(',');
                foreach (var item in values)
                {
                    adoptAnswer.Value = item;
                   int result= Transform.Int(Instance.Add(adoptAnswer));
                    if (result < 0)
                    {
                        return -1;
                    } 
                }
            }
            else
            {
                return Transform.Int(Instance.Add(adoptAnswer));
            }
                return -1;
        }
        public static int GetAnswerCount(int cardId,string externalPjId)
        {
            var count= Instance.Where("cardId=@cardId and externalPjId=@externalPjId").Parms(cardId,externalPjId).ListAll().GroupBy(c=>c.OpenId).Select(g=>g.Key).Count();
            return count;
        }
        public static int GetCompleteCount(string externalPjId, string groupId, string suvId)
        {
            return Instance
                .Where("ExternalPjId=@externalPjId and groupId=@groupId and suvId=@surveryId")
                .Parms(externalPjId, groupId, suvId)
                .ListAll()
                .Distinct(new AdoptEqual())
                .Count();
        }

       public static List<AdoptAnswer> Search(int cardId,string externalPjId)
        {
            return Instance.Where("cardId=@cardId and externalPjId=@externalPjId").Parms(cardId,externalPjId).ListAll();
        }

        public static List<AdoptAnswer> GetStatusByPidGidOpenId(string suvId,string externalPjId, string groupId,string openId,string externalMessageId)
        {
           return Instance.Where("ExternalPjId=@externalPjId and groupId=@groupId and openId=@openId and SuvId=@SuvId and  ExternalMessageId=@ExternalMessageId ").Parms(externalPjId, groupId,openId,suvId,externalMessageId).ListAll();
        }
        #endregion
    }
}
