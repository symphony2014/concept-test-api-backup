﻿using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.BLL.BaseBD
{
    public class RuleAnalysisResultsBLL: BLL<RuleAnalysisResults>
    {
        #region 定义
		private static object lockObject = new object();
        protected RuleAnalysisResultsBLL()
            : base(new RuleAnalysisResultsDAL())
        {
        }
        private static volatile RuleAnalysisResultsBLL _instance;
        public static RuleAnalysisResultsBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new RuleAnalysisResultsBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(RuleAnalysisResults item)
        {
            return Transform.Int(Instance.Add(item));
        }

        public static int UpdateItem(RuleAnalysisResults Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }

        public static List<RuleAnalysisResults> GetItemList(string groupId,string externalPjId)
        {
            var list = new List<RuleAnalysisResults>();
            list = Instance.Where("OriginalGroupId=@groupId").Parms(groupId).And("ExternalPjId=@externalPjId").Parms(externalPjId).OrderBy("CreateDate desc").ListAll();
             
            return list;
        }

        public static RuleAnalysisResults GetItem(string groupId, string ruleId, string openId)
        {
            return Instance.Where("OriginalGroupId=@groupId").Parms(groupId).And("RuleId=@ruleId").Parms(ruleId).And("OpenId=@openId").Parms(openId).Get();
        } 
        #endregion
    }
}
