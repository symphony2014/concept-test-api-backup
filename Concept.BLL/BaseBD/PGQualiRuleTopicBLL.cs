﻿using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.BLL.BaseBD
{
    public class PGQualiRuleTopicBLL : BLL<PGQualiRuleTopic>
    {
        #region 定义
        private static object lockObject = new object();
        protected PGQualiRuleTopicBLL()
            : base(new PGQualiRuleTopicDAL())
        {
        }
        private static volatile PGQualiRuleTopicBLL _instance;
        public static PGQualiRuleTopicBLL Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new PGQualiRuleTopicBLL());
                    }
                }
                return _instance;
            }
        }
        #endregion

        #region 方法 
        public static int AddItem(PGQualiRuleTopic item)
        {
            return Transform.Int(Instance.Add(item));
        }
        public static int AddItems(List<PGQualiRuleTopic> Model, string[] Cols = null)
        {
            return Transform.Int(Instance.BatchAdd(Model, Cols));
        }
        public static int UpdateItem(PGQualiRuleTopic Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }
        public static int UpdateItems(List<PGQualiRuleTopic> Model, string[] Cols = null)
        {
            return Transform.Int(Instance.BatchUpdate(Model, Cols));
        }

        #endregion
    }
}
