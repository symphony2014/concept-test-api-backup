﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class VariableValue2BLL: BLL<VariableValue2>
    {
        #region 定义
		private static object lockObject = new object();
        protected VariableValue2BLL()
            : base(new VariableValue2DAL())
        {
        }
        private static volatile VariableValue2BLL _instance;
        public static VariableValue2BLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new VariableValue2BLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(VariableValue2 item)
        {
            return Transform.Int(Instance.Add(item));
        }

        public static int UpdateItem(VariableValue2 Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }

        public static VariableValue2 GetItemCode(long varId,string text)
        {
            return Instance.Where("text=@text and varId=@varid").Parms(text, varId).Get();
        }
        //public static bool DelItemByPjId(int PjId)
        //{
        //    var item = Instance.Where("ProjectAutoId=@PjId").Parms(PjId).Get();
        //    if(item!=null)
        //    {
        //        return Instance.Del(item);
        //    }
        //    return false;
        //}

        //public static List<Project> GetProjectList(int page, int size, int ownerId, out int totalCount)
        //{
        //    var list = new List<Project>();
        //    list = Instance.Where("OwnerId=@ownerId").Parms(ownerId).OrderBy("CreateDate desc").ListAll();
        //    totalCount = list.Count;
        //    if (size == -1)
        //    {
        //        return list;
        //    }
        //    return list.GetRange((page - 1) * size, (page * size) < totalCount ? size : totalCount - ((page - 1) * size));
        //}

        //public static Project GetProjectById(string pjid)
        //{
        //    return Instance.Where("ProjectId=@pjid").Parms(pjid).Get();
        //}
        //public static Project GetProjectByAutoId(int pjid)
        //{
        //    return Instance.Where("projectAutoId=@pjid").Parms(pjid).Get();
        //}
        public static VariableValue2 GetById(long varValueId)
        {
            return Instance.Where("VarValueId=@VarValueId").Parms(varValueId).Get();
        }
        #endregion
    }
}
