﻿using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.BLL.BaseBD
{
    public class PGRuleInfoBLL : BLL<PGRuleInfo>
    {
        #region 定义
        private static object lockObject = new object();
        protected PGRuleInfoBLL()
            : base(new PGRuleInfoDAL())
        {
        }
        private static volatile PGRuleInfoBLL _instance;
        public static PGRuleInfoBLL Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new PGRuleInfoBLL());
                    }
                }
                return _instance;
            }
        }
        #endregion

        #region 方法 
        public static int AddItem(PGRuleInfo item)
        {
            return Transform.Int(Instance.Add(item));
        }

        public static int UpdateItem(PGRuleInfo Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }

        public static List<PGRuleInfo> GetItemListByGroup(string groupId, string externalPjId)
        {
            var list = new List<PGRuleInfo>();
            list = Instance.Where("GroupId=@groupId").Parms(groupId).And("ExternalPjId=@externalPjId").Parms(externalPjId).And("IsUse=1").OrderBy("CreateDate desc").ListAll();

            return list;
        }
        public static PGRuleInfo GetItem(string groupId, string externalPjId,string ruleId)
        {
            return Instance.Where("ExternalPjId=@externalPjId").Parms(externalPjId).And("RuleId=@ruleId").Parms(ruleId).And("GroupId=@groupId").Parms(groupId).Get();
        }
        public static PGRuleInfo GetItemByRuleId(string ruleId)
        {
            return Instance.Where("RuleId=@ruleId").Parms(ruleId).Get();
        }
        #endregion
    }
}
