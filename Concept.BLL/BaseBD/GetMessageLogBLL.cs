﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using Concept.Entity.EnumMode;
using Concept.Entity.SysMode;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class TaskParamLogBLL : BLL<TaskParamLog>
    {
        #region 定义
		private static object lockObject = new object();
        protected TaskParamLogBLL()
            : base(new TaskParamLogDLL())
        {
        }
        private static volatile TaskParamLogBLL _instance;

        public static TaskParamLogBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new TaskParamLogBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(TaskParamLog item)
        {
            return Transform.Int(Instance.Add(item));
        }

        public static int UpdateItem(TaskParamLog Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }

        public static List<TaskParamLog> LoadNeedProcessItem(int dataSize,ParamType type)
        {
            return Instance.Where(" paramType=@type and status=0 ").Parms(Convert.ToInt32(type)).ListAll().Take(dataSize).ToList();
        }

        public static int BulkUpdateItems(List<TaskParamLog> hasProcessingItems, string[] v)
        {
           
                return Transform.Int(Instance.BatchUpdate(hasProcessingItems, v));
            
        }

        public static SearchStatusEnum   GetStatus(string prjId,string sampGrpId,string topicId)
        {
            var list = Instance.Where(" prjId=@prjId and sampGrpId=@sampGrpId and topicId=@topicId ").Parms(prjId, sampGrpId, topicId).ListAll();
            var latest=list.OrderByDescending(l => l.AddDate).FirstOrDefault();

            if (latest == null)
            {
                return SearchStatusEnum.topicNotEnded;
            }
            return (SearchStatusEnum)latest.status;
        }


        #endregion
    }
}
