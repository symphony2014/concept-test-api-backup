﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class ConceptCardBLL: BLL<ConceptCard>
    {
        #region 定义
		private static object lockObject = new object();
        protected ConceptCardBLL()
            : base(new ConceptCardDAL())
        {
        }
        private static volatile ConceptCardBLL _instance;
        public static ConceptCardBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new ConceptCardBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(ConceptCard item)
        {
            return Transform.Int(Instance.Add(item));
        }

        public static int UpdateItem(ConceptCard Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }

        public static bool DelItemByCardId(int cardId)
        {
            var item = Instance.Where("CardId=@cardId").Parms(cardId).Get();
            if (item != null)
            {
                return Instance.Del(item);
            }
            return false;
        }

        public static int GetOrderCardByProject(int projectAutoId)
        {
            int cardorder = 1;
            var cardList = Instance.Where("ProjectAutoId=@projectAutoId").Parms(projectAutoId).OrderBy("CardOrder desc").ListAll();
            if(cardList.Count>0)
            {
                cardorder = cardList[0].CardOrder + 1;
            }
            return cardorder;
        }

        public static List<ConceptCard> GetConceptCardList(int projectAutoId)
        {
            var list = new List<ConceptCard>();
            list = Instance.Where("ProjectAutoId=@projectAutoId").Parms(projectAutoId).OrderBy("CardOrder").ListAll();
            return list;
        }

        public static ConceptCard GetConceptCardByCardId(int cardId)
        {
            var card = Instance.Where("CardId=@cardId").Parms(cardId).Get();
            return card;
        }

        #endregion
    }
}
