﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class VariableBLL: BLL<Variable>
    {
        #region 定义
		private static object lockObject = new object();
        protected VariableBLL()
            : base(new VariableDAL())
        {
        }
        private static volatile VariableBLL _instance;
        public static VariableBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new VariableBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(Variable item)
        {
            return Transform.Int(Instance.Add(item));
        }

        public static int UpdateItem(Variable Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }

        public static Variable GetVarByText(string columnName, string externalPjId)
        {
            var item = Instance.Where("ExternalVarId=@externalPjId and columnName=@columnName").Parms(externalPjId, columnName).Get();
            return item;
        
        }

        public static Variable GetById(long varId)
        {
          return   Instance.Where("varId=@varId").Parms(varId).Get();
        }

        public static bool RemoveByPjId(string externalPjId)
        {
            var items = Instance.Where("externalVarid=@externalPjId").Parms(externalPjId);
            var query=  SqlQuery.CreateByQuery(items);
            var list = items.ListAll();
            foreach (var item in list)
            {
                var listvar = VariableValue2BLL.Instance.Where("varId=@VarId").Parms(item.VarId).ListAll();
                VariableValue2BLL.Instance.BatchDel(listvar);
                var answers = VariableAnswerBLL.Instance.Where("varId=@VarId").Parms(item.VarId).ListAll();
                VariableAnswerBLL.Instance.BatchDel(answers);
            }
            return Instance.Del(query);
            // Instance
        }

        public static List<Variable> GetByPrjId(string externalPjId)
        {
             return Instance.Where("externalVarid=@externalPjId").Parms(externalPjId).ListAll();

        }
      
        #endregion
    }
}
