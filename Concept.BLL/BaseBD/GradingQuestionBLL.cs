﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class GradingQuestionBLL : BLL<GradingQuestion>
    {
        #region 定义
		private static object lockObject = new object();
        protected GradingQuestionBLL()
            : base(new GradingQuestionDAL())
        {
        }
        private static volatile GradingQuestionBLL _instance;
        public static GradingQuestionBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new GradingQuestionBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static bool addItemList(List<GradingQuestion> Model)
        {
            return Instance.BatchAdd(Model);
        }  
        public static int UpdateItem(GradingQuestion Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }
        public static bool UpdateItemList(List<GradingQuestion> Model, string[] Cols = null)
        {
            return Instance.BatchUpdate(Model, Cols);
        }
        
        public static List<GradingQuestion> GetGradingQuestionList(int projectAutoId)
        {
            var list = new List<GradingQuestion>();
            list = Instance.Where("ProjectAutoId=@projectAutoId").Parms(projectAutoId).OrderBy("CreateDate").ListAll();
            return list;
        }
        #endregion
    }
}
