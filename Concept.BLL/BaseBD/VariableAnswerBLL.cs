﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.BLL.BaseBD.Log;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using Concept.Entity.BaseDB.Log;
using ConceptCommon;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class VariableAnswerBLL: BLL<VariableAnswer>
    {
        #region 定义
		private static object lockObject = new object();
        protected VariableAnswerBLL()
            : base(new VariableAnswerDAL())
        {
        }
        private static volatile VariableAnswerBLL _instance;
        public static VariableAnswerBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new VariableAnswerBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(VariableAnswer item)
        {
            return Transform.Int(Instance.Add(item));
        }
        public static VariableAnswer GetByOpenIdVariable(string openId,long varId)
        {
            return  Instance.Where("openId=@openId and varId=@varId").Parms(openId,varId).ListAll().FirstOrDefault();
        }
        public static string GetUserIconByOpenId(string openId,string externalPjId="")
        {
            // var details = GetUserDetail(openId, appendData);
            var anwsers=VariableAnswerBLL.GetUserData(openId, "xxx");
            foreach (var item in anwsers)
            {
                var varable = VariableBLL.GetById(item.varId);
                if (varable != null && varable.ColumnName == "微信头像" && (varable.ExternalVarId==externalPjId || externalPjId==""))
                {
                    return item.Value;
                }
            }
            return string.Empty;

        }
        public static string GetUserNameByOpenId(string openId)
        {
            // var details = GetUserDetail(openId, appendData);
            var anwsers=VariableAnswerBLL.GetUserData(openId, "xxx");
            foreach (var item in anwsers)
            {
                var varable = VariableBLL.GetById(item.varId);
                if (varable != null && varable.ColumnName == "姓名" && item.Value!="")
                {
                    return item.Value;
                }
                if (varable != null && varable.ColumnName == "昵称" && item.Value != "")
                {
                    return item.Value;
                }
            }
            return string.Empty;

        }
        static public List<UserDetail> GetUserDetail(string openId,string externalPjId="")
        {

            Func<string,string, List<VariableAnswer>> getAnswers = (string pjId,string openIdR)=> {
            List<VariableAnswer> answers2 = VariableAnswerBLL.GetUserData(openIdR, "xxxx");
            string pjIdForSearch = pjId == "" ? VariableBLL.GetById(answers2.FirstOrDefault().varId).ExternalVarId :pjId;
            if (!string.IsNullOrEmpty(pjIdForSearch))
            {
                answers2= answers2.Where(a=> {
                    var varable = VariableBLL.GetById(a.varId);
                    if(varable == null)
                    {
                        Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException {
                            AddTime = DateTime.Now,
                            ModuleName = "GetUserDetail",
                            ExceptMessage = "",
                            MessDescript = "variable ID "+a.varId +"null"
                        });
                        return false;
                    }
                    return varable.ExternalVarId == pjIdForSearch;
                }).ToList();
            }
                return answers2;
            };
            var answers = getAnswers(externalPjId,openId);
            //List<VariableAnswer> answers = VariableAnswerBLL.GetUserData(openId, "xxxx");
            //if (!string.IsNullOrEmpty(externalPjId))
            //{
            //    answers = answers.Where(a=> {
            //        var varable = VariableBLL.GetById(a.varId);
            //        if(varable == null)
            //        {
            //            Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException {
            //                AddTime = DateTime.Now,
            //                ModuleName = "GetUserDetail",
            //                ExceptMessage = "",
            //                MessDescript = "variable ID "+a.varId +"null"
            //            });
            //            return false;
            //        }
            //        return varable.ExternalVarId == externalPjId;
            //    }).ToList();
            //}
            //else
            //{
            //    var firstExternalPjId = VariableBLL.GetById(answers.FirstOrDefault().varId).ExternalVarId;
                
            //}
            List<UserDetail> details = new List<UserDetail>();
            
            foreach (var item in answers)
            {
                UserDetail detail = new UserDetail();
                detail.Key = VariableBLL.GetById(item.varId).ColumnName;
                detail.Value = item.Value;
                details.Add(detail);
            }
            return details;
        }
        static public List<UserMap>  GetUserDetailByPjId(string pjId)
        {
            List<Variable> variables = VariableBLL.GetByPrjId(pjId);
            List<VariableAnswer> answers =new List<VariableAnswer>();
            foreach (var item in variables)
            {
                answers.AddRange(Instance.Where("varid=@varid").Parms(item.VarId).ListAll());
            } 
            List<UserMap> details = new List<UserMap>();
            var data = from ans in answers
                       group ans by ans.OpenId into g
                       select new { key = g.Key, list = g.ToList() };
            foreach (var item in data)
            {
                var detail = new UserMap();
                detail.pairs = new List<UserDetail>();
                foreach (var sub in item.list)
                {
                    UserDetail subDetail = new UserDetail();
                    subDetail.Key = VariableBLL.GetById(sub.varId).ColumnName;
                    subDetail.Value = sub.Value;
                    detail.pairs.Add(subDetail);
                }
                details.Add(detail);
            }
            return details;
             
        }
        public static List<VariableAnswer> GetUserData(string openId,string mobile)
        {
            List<VariableAnswer> answers = Instance.Where("openId=@openId or openId=@mobile").Parms(openId,mobile).ListAll();

            //var newAnswers = new List<VariableAnswer>();
            //foreach (var item in answers)
            //{
            //    Variable variable = VariableBLL.GetById(item.varId);
            //    if (variable.ExternalVarId == externalPjId)
            //    {
            //        newAnswers.Add(item);
            //    }
            //}

            return answers;
        }
    
        public static int UpdateItem(VariableAnswer Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }

        public static List<UserMap> GetUserDetailByGroupId(string externalPjId,string groupId)
        {
            List<UserMap> list = new List<UserMap>();
            var openids=MessageBLL.GetMessagesByGroupId(groupId);

            foreach (var item in openids)
            {
                if (!string.IsNullOrWhiteSpace(item))
                {
                    var map = new UserMap();
                   map.pairs = GetUserDetail(item, externalPjId);
                     list.Add(map);
                }
            }
            return list;
        }

        //public static bool DelItemByPjId(int PjId)
        //{
        //    var item = Instance.Where("ProjectAutoId=@PjId").Parms(PjId).Get();
        //    if(item!=null)
        //    {
        //        return Instance.Del(item);
        //    }
        //    return false;
        //}

        //public static List<Project> GetProjectList(int page, int size, int ownerId, out int totalCount)
        //{
        //    var list = new List<Project>();
        //    list = Instance.Where("OwnerId=@ownerId").Parms(ownerId).OrderBy("CreateDate desc").ListAll();
        //    totalCount = list.Count;
        //    if (size == -1)
        //    {
        //        return list;
        //    }
        //    return list.GetRange((page - 1) * size, (page * size) < totalCount ? size : totalCount - ((page - 1) * size));
        //}

        //public static Project GetProjectById(string pjid)
        //{
        //    return Instance.Where("ProjectId=@pjid").Parms(pjid).Get();
        //}
        //public static Project GetProjectByAutoId(int pjid)
        //{
        //    return Instance.Where("projectAutoId=@pjid").Parms(pjid).Get();
        //}
        public static int GetProjectUserCount(string prjId)
        {
            var variables=VariableBLL.GetByPrjId(prjId);
            var codVar = variables.First(v=>v.Code=="ID");
            var list=Instance.Where("VarId=@VarId").Parms(codVar.VarId).ListAll();

            return list.GroupBy(l => l.OpenId).Select(g => g.Key).Count();
        }
        #endregion
    }
}
