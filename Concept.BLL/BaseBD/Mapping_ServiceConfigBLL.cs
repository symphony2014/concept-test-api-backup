﻿using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.BLL.BaseBD
{
    public class Mapping_ServiceConfigBLL : BLL<Mapping_ServiceConfig>
    {
        #region 定义
        private static object lockObject = new object();
        protected Mapping_ServiceConfigBLL()
            : base(new Mapping_ServiceConfigDAL())
        {
        }
        private static volatile Mapping_ServiceConfigBLL _instance;
        public static Mapping_ServiceConfigBLL Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new Mapping_ServiceConfigBLL());
                    }
                }
                return _instance;
            }
        }
        #endregion

        #region 方法
        /// <summary>
        /// 返回100条央行上报服务配置表记录
        /// </summary>
        /// <returns></returns>
        public static List<Mapping_ServiceConfig> LoadAllConfigItems()
        {
            var q = Instance.Where("isDel = 0").And("IsEnable = 1").OrderBy("AddTime desc");

            List<Mapping_ServiceConfig> result = q.List(100, 0);
            return result;
        }

        public static List<Mapping_ServiceConfig> LoadSpecifiedEnableItems(string[] specifiedItems)
        {
            if (specifiedItems == null || specifiedItems.Length == 0)
            {
                throw new ArgumentNullException("specifiedItems");
            }

            var q = Instance.Where("isDel = 0").And("IsEnable = 1").OrderBy("AddTime desc");
            q = q.In("CqModelName", specifiedItems);

            List<Mapping_ServiceConfig> result = q.List(100, 0);
            return result;
        }

        #endregion
    }
}
