﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class MessageBLL: BLL<Message>
    {
        #region 定义
		private static object lockObject = new object();
        protected MessageBLL()
            : base(new MessageDAL())
        {
        }
        private static volatile MessageBLL _instance;
        public static MessageBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new MessageBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(Message item)
        {
            return Transform.Int(Instance.Add(item));
        }

        public static int UpdateItem(Message Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }

        public static string GetLastUpdate(string externalPjId,string groupId,string topicId)
        {
           var first= Instance.Where("externalPjid=@externalPjid and sampGrpId=@groupid and tpcId=@topicId order by crtTm desc").Parms(externalPjId, groupId,topicId).Get();
            if(first!=null)
            return first.crtTm;
            else
            {
                return "1973-01-01";
            }
        }
        /// <summary>
        /// groupId,或者topicId 为-1时，则忽略对应的参数
        /// </summary>
        /// <param name="externalPjId"></param>
        /// <param name="groupId"></param>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public static List<Message> GetMessages(string externalPjId, string groupId,  string topicId)
        {
            string param = " openId is not NULL and  externalPjid=@externalPjid  ";
            object[] obj = new object[] { null, null, null };
            obj[0] = externalPjId;
            if (groupId != "-1") {
                param += " and sampGrpId=@groupId ";
                obj[1] = groupId;
            }
            if (topicId != "-1")
            {
                param += " and tpcId=@tpcId ";
                obj[2] = topicId;
            }
            return Instance.Where(param).Parms(obj).OrderBy("crtTm desc").ListAll();
        }

        /// <summary>
        /// groupId,或者topicId 为-1时，则忽略对应的参数
        /// </summary>
        /// <param name="externalPjId"></param>
        /// <param name="groupId"></param>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public static List<Message> GetMessagesWithOpenId(string externalPjId, string groupId, string topicId)
        {
            string param = "  externalPjid=@externalPjid  ";
            object[] obj = new object[] { null, null, null };
            obj[0] = externalPjId;
            if (groupId != "-1")
            {
                param += " and sampGrpId=@groupId ";
                obj[1] = groupId;
            }
            if (topicId != "-1")
            {
                param += " and tpcId=@tpcId ";
                obj[2] = topicId;
            }
            return Instance.Where(param).Parms(obj).OrderBy("crtTm desc").ListAll();
        }

        public static void RemoveBy(string externalPjId, string groupId, string topicId)
        {
            string param = "externalPjid=@externalPjid  ";
            object[] obj = new object[] {null,null,null };
            obj[0] = externalPjId;
            if (groupId != "-1")
            {
                param += " and sampGrpId=@groupId ";
                obj[1] = groupId;
            }
            if (topicId != "-1")
            {
                param += " and tpcId=@tpcId ";
                obj[2] = topicId;
            }
            var items = Instance.Where(param).Parms(obj);
            var query = SqlQuery.CreateByQuery(items);
            var list = items.ListAll();
            Instance.BatchDel(list);
            foreach (var item in list)
            {
                Instance.Del(item);
            }
        }

        public static IEnumerable<string> GetMessagesByGroupId(string groupId)
        {
            string where = "1=1";
            if (groupId != "-1")
            {
                where = "sampGrpId=@groupId";
            }
            var items=Instance.Where(where).Parms(groupId).ListAll();
            var data = (from t in items
                        select t.openId).Distinct();
                      
            return data; 
        }

        //public static List<Message> GetAllMessages(string externalPjId)
        //{
        //    return Instance.Where("externalPjId=@externalPjId").Parms(externalPjId).ListAll();
        //}
        //public static bool DelItemByPjId(int PjId)
        //{
        //    var item = Instance.Where("ProjectAutoId=@PjId").Parms(PjId).Get();
        //    if(item!=null)
        //    {
        //        return Instance.Del(item);
        //    }
        //    return false;
        //}

        //public static List<Project> GetProjectList(int page, int size, int ownerId, out int totalCount)
        //{
        //    var list = new List<Project>();
        //    list = Instance.Where("OwnerId=@ownerId").Parms(ownerId).OrderBy("CreateDate desc").ListAll();
        //    totalCount = list.Count;
        //    if (size == -1)
        //    {
        //        return list;
        //    }
        //    return list.GetRange((page - 1) * size, (page * size) < totalCount ? size : totalCount - ((page - 1) * size));
        //}

        //public static Project GetProjectById(string pjid)
        //{
        //    return Instance.Where("ProjectId=@pjid").Parms(pjid).Get();
        //}
        //public static Project GetProjectByAutoId(int pjid)
        //{
        //    return Instance.Where("projectAutoId=@pjid").Parms(pjid).Get();
        //}
        #endregion
    }
}
