﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class RadarLinkBLL: BLL<RadarLink>
    {
        #region 定义
		private static object lockObject = new object();
        protected RadarLinkBLL()
            : base(new RadarLinkDAL())
        {
        }
        private static volatile RadarLinkBLL _instance;
        public static RadarLinkBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new RadarLinkBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(RadarLink item)
        {
            return Transform.Int(Instance.Add(item));
        }

        public static int UpdateItem(RadarLink Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }
        public static RadarLink UpdateItemStatus(int id,string status)
        {
            var item = Instance.Where("Id=@Id").Parms(id,status).Get();
            item.Status = status;
            int error= Transform.Int(Instance.Update(item));
            return item;
        }
        public static int GetCompleteCount(string externalPjId,string groupId,string surveryId)
        {
            return Instance.Where("ExternalPjId=@externalPjId and groupId=@groupId and surveryId=@surveryId  and Status='c'").Parms(externalPjId, groupId, surveryId).Count();
        }
        //public static List<RadarLink> GetRadarLink(string pjId)
        //{
        //    var list = new List<RadarLink>();
        //    list = Instance.Where("pjId=@pjId").Parms(pjId).ListAll();
        //    return list;
        //}
        public static RadarLink GetRadarLinkDefault(string SurveryId)
        {
            var link = Instance.Where("SurveryId=@SurveryId and IsUsed=0").Parms(SurveryId).Get();
            return link;
        }

        public static RadarLink GetRadarLinkByOpenIdOrMobile(string openId,string mobile)
        {
            var list = new List<RadarLink>();
            var result = Instance.Where(" OpenId=@OpenId or OpenId=@mobile").Parms( openId, mobile);
            if(result.ListAll().Count>0) 
            return result.ListAll().FirstOrDefault();
            else
            {
                return null;
            }
        }
        public static RadarLink GetRadarLinkByGroupOpenIdOrMobile(string SurveryId, string externalpjId, string openId, string mobile)
        {
            var list = new List<RadarLink>();
          
            var result = Instance.Where("SurveryId=@SurveryId and externalpjId=@externalpjId  and ( OpenId=@OpenId )").Parms(SurveryId, externalpjId, openId);
            //var result3 = Instance.Where("SurveryId=@suvId and externalpjId=@externalpjId and GroupId=@groupId and ( OpenId=@OpenId )").Parms(suvId, externalpjId, groupId, openId);
            if (result.ListAll().Count > 0)
                return result.ListAll().FirstOrDefault();
            else
            {
                return null;
            }
        }
        //public static bool DelItemByPjId(int PjId)
        //{
        //    var item = Instance.Where("ProjectAutoId=@PjId").Parms(PjId).Get();
        //    if(item!=null)
        //    {
        //        return Instance.Del(item);
        //    }
        //    return false;
        //}

        //public static List<Project> GetProjectList(int page, int size, int ownerId, out int totalCount)
        //{
        //    var list = new List<Project>();
        //    list = Instance.Where("OwnerId=@ownerId").Parms(ownerId).OrderBy("CreateDate desc").ListAll();
        //    totalCount = list.Count;
        //    if (size == -1)
        //    {
        //        return list;
        //    }
        //    return list.GetRange((page - 1) * size, (page * size) < totalCount ? size : totalCount - ((page - 1) * size));
        //}

        //public static Project GetProjectById(string pjid)
        //{
        //    return Instance.Where("ProjectId=@pjid").Parms(pjid).Get();
        //}
        //public static Project GetProjectByAutoId(int pjid)
        //{
        //    return Instance.Where("projectAutoId=@pjid").Parms(pjid).Get();
        //}
        #endregion
    }
}
