﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.BLL.BaseBD.Log;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using ConceptCommon;
using FunLayer;
using Newtonsoft.Json;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class DeviceAnswerBLL: BLL<DeviceAnswer>
    {
        #region 定义
		private static object lockObject = new object();
        protected DeviceAnswerBLL()
            : base(new DeviceAnswerDAL())
        {
        }
        private static volatile DeviceAnswerBLL _instance;
        public static DeviceAnswerBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new DeviceAnswerBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(DeviceAnswer item)
        {
            return Transform.Int(Instance.Add(item));
        }
       

        public static int UpdateItem(DeviceAnswer Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }

        public static List<DeviceAnswer> GetDeviceAnswerListByProject(int projectAutoId,string externalPjId="",string exgroupid="")
        {
            var list = new List<DeviceAnswer>();
            list = Instance.Where("ProjectAutoId=@projectAutoId").Parms(projectAutoId).ListAll();
            Concept_LogExceptionBLL.InsertLogExceptionInfo(new Entity.BaseDB.Log.Concept_LogException { ModuleName = ":GetDeviceAnswerListByProject1", ExceptMessage = list.Count.ToString() });
            if (!string.IsNullOrEmpty(externalPjId) && externalPjId!="-1")
            list = list.Where(l=> MapGetUtil.MapGet(l.AppendData,"externalPjId")==externalPjId).ToList();
            Concept_LogExceptionBLL.InsertLogExceptionInfo(new Entity.BaseDB.Log.Concept_LogException { ModuleName = ":GetDeviceAnswerListByProject2", ExceptMessage = list.Count.ToString() });

            if (!string.IsNullOrEmpty(exgroupid) && exgroupid!="-1")
            list = list.Where(l=> MapGetUtil.MapGet(l.AppendData,"GroupId")==exgroupid).ToList();
            Concept_LogExceptionBLL.InsertLogExceptionInfo(new Entity.BaseDB.Log.Concept_LogException { ModuleName = ":GetDeviceAnswerListByProject3", ExceptMessage = list.Count.ToString() });
            Concept_LogExceptionBLL.InsertLogExceptionInfo(new  Entity.BaseDB.Log.Concept_LogException{ ModuleName = ":GetDeviceAnswerListByProject", ExceptMessage = JsonConvert.SerializeObject(list) });

            return list;
        }

        #endregion
       
    }
}
