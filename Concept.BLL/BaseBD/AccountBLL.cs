﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.BLL.BaseBD.Log;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using Concept.Entity.BaseDB.Log;
using FunLayer;
using Newtonsoft.Json;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class AccountBLL : BLL<Account>
    {
        #region 定义
        private static object lockObject = new object();
        protected AccountBLL()
            : base(new AccountDAL())
        {
        }
        private static volatile AccountBLL _instance;
        public static AccountBLL Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new AccountBLL());
                    }
                }
                return _instance;
            }
        }
        #endregion

        #region 方法
         

        public static int AddItem(Account item)
        {
            return Transform.Int(Instance.Add(item));
        }

        public static int UpdateAccount(Account Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }

        public static Account GetAccountByName(string loginName)
        {
            return Instance.Where("LoginName=@loginName").Parms(loginName).Get();
        }

        public static Account GetAccountById(long accountId)
        {
            return Instance.Where("AccountId=@accountId").Parms(accountId).Get();
        }

        public static Account GetAccountByNamePassWord(string name, string pwd)
        {
          
            Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { ModuleName = "GetAccountByNamePassWord", ExceptMessage = name+pwd});
            return Instance.Where("LoginName=@name").Parms(name).And("Password=@pwd").Parms(pwd).Get();
        } 
        #endregion
    }
}
