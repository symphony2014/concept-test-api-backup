﻿using ServeName;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;


namespace Service_SettingRunEntry
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
            Assembly currentExeAssembly = Assembly.GetExecutingAssembly();

            if (currentExeAssembly == null  || string.IsNullOrEmpty(currentExeAssembly.Location))
            {
                Console.WriteLine(string.Format("无法获得当前执行Assembly的信息"));
            }

            Console.WriteLine(string.Format("当前执行Assembly[{0}]", currentExeAssembly.Location));

            try
            {
                string strSeverName = ServeNameConfigHelper.GetServiceName(currentExeAssembly.Location);
                if (!string.IsNullOrEmpty(strSeverName))
                {
                    this.serviceInstaller1.ServiceName = strSeverName;
                }
                
            }
            catch(Exception ex)
            {
                Console.WriteLine(string.Format("有异常发生[{0}]", ex.Message));
            }
        }
       
    }
}
