﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using ConceptCommon.Interface;
using MappingServeice.Base;


namespace Service_SettingRunEntry
{
    /// <summary>
    /// It run the IServiceRun on the special time.
    /// </summary>
    public partial class ServiceSettingRunEntry : MappingServiceBase
    {
        public ServiceSettingRunEntry()
        {
            InitializeComponent();
        }
          
        protected override void TestRunProcessLogic()
        {
            IServiceRun serviceRunProvider = this.GetServiceRunProvider();
            serviceRunProvider.Process();
        }
         
        protected override IServiceRun GetServiceRunProvider()
        {
            return this.GetServiceRunProviderFromConfig();
        }
         
    }
}
