﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptWebApi.Models
{
    public class UserInfo
    {
        public string PjId { get; set; }
        public string OpenId { get; set; }
        public string Mobile { get; set; }
        public string Nick { get; set; }

    }
}