﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptWebApi.Models
{
    public  enum SuvType
    {
        Conceptest,
        Radar
    }
    public enum  LinkType
    {
        /// <summary>
        /// 唯一
        /// </summary>
        Unique,
        /// <summary>
        /// 预传
        /// </summary>
        Saved,
        /// <summary>
        /// 公开
        /// </summary>
        Public
    }
    public class Param
    {
        //老宋接口项目ID
        public string ExternalPjId { get; set; }
        public string ExternalUserId { get; set; }
        public string MessageId { get; set; }

        public string GroupId { get; set; }
        //SurveryID
        public string SuvId { get; set; }
        public string PjId { get; set; }

        public SuvType SuvType { get; set; }

        // public LinkType linkType { get; set; }

        public string ccQnttvMtrlDtlId{ get; set; }
        public string Mobile { get; set; }

        public int RadarId { get; set; }
        public string OpenId { get; set; }

        //1:预传 0：私用，2：公开
        public int LinkType { get; set; }

        public string returnUrl { get; set; }
        
    }
  
}