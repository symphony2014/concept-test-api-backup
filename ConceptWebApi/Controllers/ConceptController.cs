﻿using Concept.BLL.BaseBD;
using Concept.Entity.BaseDB;

using ConceptCommon.ReturnMode;
using ConceptWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
//using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

//using System.Net.Http;

using System.Web.Http;
using System.Net;
using System.Configuration;

using Concept.BLL.BaseBD.Log;
using Concept.Entity.BaseDB.Log;
using Newtonsoft.Json.Linq;
using ConceptCommon;
using Concept.Entity.External;
using ExternalAPI;
using Newtonsoft.Json;

namespace ConceptWebApi.Controllers
{
    public class ConceptController : Controller
    {
        string externalCompletePage = ConfigurationManager.AppSettings["externalCompletePage"];
        string externalUCompletePage = ConfigurationManager.AppSettings["externalUCompletePage"];
        public readonly string topicPath = ConfigurationManager.AppSettings["TopicPath"];
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        ///更新radarLink 状态信息 
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        public ActionResult UpdateRadarStatus(int radarId, string csq)
        {
            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                var result = RadarLinkBLL.UpdateItemStatus(radarId, csq);
                //TODO 更新图文消息
                var status = Import.Postqnttvmtrlresult(new ExternalQnt
                {
                    parms = new parms[]{
                    new parms
                    {
                        ccQnttvMtrlDtlId = result.messageId,
                        sampGrpId = result.GroupId,
                        usrId = result.externalUserId,
                        status = "3"
                    } }
                });
                Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException
                {
                    AddTime = DateTime.Now,
                    ExceptMessage = status.errMsg,
                    MessDescript = status.statusCode,
                    ModuleName = "ConceptController:UpdateRadarStatus"
                });
                if (string.IsNullOrEmpty(result.ReturnUrl))
                {
                    throw new Exception("返回地址为空！");
                }
                return Redirect(result.ReturnUrl);

            }
            catch (System.Exception e)
            {
                Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException
                {
                    AddTime = DateTime.Now,
                    ExceptMessage = e.ToString(),
                    MessDescript = "exceptionError",
                    ModuleName = "ConceptController:UpdateRadarStatus"
                });
                return Json(e.ToString(), JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// 获取完成链接用户个数
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        public ActionResult GetCompleteCount(Param param)
        {
            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                if (param.SuvType == SuvType.Radar)
                {
                    vr.Result = RadarLinkBLL.GetCompleteCount(param.ExternalPjId, param.GroupId, param.SuvId);
                }
                else
                {
                    vr.Result = AdoptAnswerBLL.GetCompleteCount(param.ExternalPjId, param.GroupId, param.SuvId);
                }
                return Json(vr, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception e)
            {
                return Json(HttpStatusCode.InternalServerError, e.ToString());
            }
        }
        /// <summaryd
        /// 获取用户信息
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        public ActionResult GetUserInfo(string openId)
        {
            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                List<VariableAnswer> answers = VariableAnswerBLL.GetUserData(openId, "xxxx");
                List<UserDetail> details = new List<UserDetail>();
                foreach (var item in answers)
                {
                    UserDetail detail = new UserDetail();
                    detail.Key = VariableBLL.GetById(item.varId).ColumnName;
                    detail.Value = item.Value;
                    details.Add(detail);
                }
                //vr.Result = result;
                return Json(details, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception e)
            {
                return Json(HttpStatusCode.InternalServerError, e.ToString());
            }
        }
        public ActionResult AppendUserInfo(ExternalUserInfo userInfo)
        {
            try
            {

            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
                Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { ModuleName = "ConceptController:AppendUserInfo0", ExceptMessage = JObject.FromObject(userInfo).ToString(), AddTime = DateTime.Now });
                List<Variable> variables = VariableBLL.GetByPrjId(userInfo.prjId);
                var nickVar = variables.Find(v => v.ColumnName == "昵称");
                var IDVar = variables.Find(v => v.ColumnName == "ID");
                VariableAnswer ans = VariableAnswerBLL.GetByOpenIdVariable(userInfo.openId, nickVar.VarId);

                if (ans == null)
                {
                    Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { ModuleName = "ConceptController:AppendUserInfo1", ExceptMessage = "nick name: "+ userInfo.jsonData.Find(o => o.key == "昵称").value, AddTime = DateTime.Now });

                    VariableAnswerBLL.AddItem(new VariableAnswer { OpenId = userInfo.openId, varId = nickVar.VarId, Value = userInfo.jsonData.Find(o => o.key == "昵称").value });
                    Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { ModuleName = "ConceptController:AppendUserInfo2", ExceptMessage = "openId: " + userInfo.openId+"IDVar:"+IDVar.VarId, AddTime = DateTime.Now });

                    VariableAnswerBLL.AddItem(new VariableAnswer { OpenId = userInfo.openId, varId = IDVar.VarId, Value = userInfo.openId });

                    var userIconVar = variables.Find(v => v.ColumnName == "微信头像");
                    VariableAnswerBLL.AddItem(new VariableAnswer { OpenId = userInfo.openId, varId = userIconVar.VarId, Value = userInfo.jsonData.Find(o => o.key == "微信头像").value });
                }
                return Json(vr);
            }
            catch (Exception e)
            {

                throw e;
            }

        }
        /// <summary>
        /// 更新用户信息
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        public ActionResult UpdateUserInfo(ExternalUserInfo userInfo)
        {
            Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { ModuleName = "ConceptController:UpdateUserInfo", ExceptMessage = JObject.FromObject(userInfo).ToString(), AddTime = DateTime.Now });
            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                List<VariableAnswer> answers = VariableAnswerBLL.GetUserData(userInfo.openId, userInfo.phone);
                foreach (var item in answers)
                {
                    Variable variable = VariableBLL.GetById(item.varId);

                    if (variable != null)
                    {
                        if (!string.IsNullOrWhiteSpace(userInfo.openId))
                        {
                            item.OpenId = userInfo.openId;
                            VariableAnswerBLL.UpdateItem(item);
                        }
                        var isOther = true;

                        foreach (var map in ExternalUserInfo.convertMapping)
                        {

                            if ((map[1].ToLower()) == (variable.Code.ToLower()))
                            {
                                PropertyInfo prop = userInfo.GetType().GetProperty(map[0], BindingFlags.Public | BindingFlags.Instance);
                                var propValue = prop.GetValue(userInfo);
                                if (propValue != null)
                                {
                                    item.Value = prop.GetValue(userInfo).ToString();
                                    VariableAnswerBLL.UpdateItem(item);
                                    isOther = false;
                                }
                            }
                        }

                        if (isOther)
                        {
                            if (userInfo.jsonData != null)
                            {
                                var other = userInfo.jsonData.FirstOrDefault(o => o.key == variable.Code || o.key == variable.ColumnName);
                                if (other != null)
                                {
                                    item.Value = other.value;
                                    VariableAnswerBLL.UpdateItem(item);
                                }
                            }
                        }
                    }
                }

                return Json(vr);
            }
            catch (System.Exception e)
            {
                Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { ModuleName = "ConceptController:UpdateUserInfo", ExceptMessage = e.ToString() });
                return Json(HttpStatusCode.InternalServerError, e.ToString());
            }
        }
        /// <summary>
        /// 中间跳转页
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ActionResult Redirect(Param param)
        {
            string link = string.Empty;

            //Update radarlink table info.

            if (param.SuvType == SuvType.Conceptest)
            {
                List<AdoptAnswer> answers = AdoptAnswerBLL.GetStatusByPidGidOpenId(param.SuvId, param.ExternalPjId, param.GroupId, param.OpenId,param.MessageId);
                if (answers.Count > 0)
                {
                    //静态提示已完成页面
                    link = externalCompletePage;
                }
                else
                {

                    //老宋用于回传页
                    link += externalUCompletePage;

                }
                link += buildAppendIds(param, true);

                //保存returnUrl供submitanswer 跳转使用
                var conceptLink = ConceptLinkBLL.GetConceptLinkByGroupOpenIdOrMobile(param.SuvId, param.ExternalPjId, param.GroupId, param.OpenId);
                if (conceptLink == null)
                    ConceptLinkBLL.AddItem(new ConceptLink
                    {
                        ExternalPjId = param.ExternalPjId,
                        externalUserId = param.ExternalUserId,
                        GroupId = param.GroupId,
                        messageId = param.MessageId,
                        OpenId = param.OpenId,
                        ReturnUrl = param.returnUrl,
                        SurveryId = param.SuvId
                    });

            }
            else if (param.SuvType == SuvType.Radar)
            {

                switch (param.LinkType)
                {
                    case 1:

                        var userLink = RadarLinkBLL.GetRadarLinkByOpenIdOrMobile(param.OpenId, param.Mobile);

                        if (userLink == null)
                        {
                            userLink = RadarLinkBLL.GetRadarLinkDefault(param.SuvId);
                            userLink.IsUsed = 1;
                            userLink.ExternalPjId = param.ExternalPjId;
                            userLink.GroupId = param.GroupId;
                            userLink.OpenId = param.OpenId;
                            userLink.ReturnUrl = param.returnUrl;
                            userLink.externalUserId = param.ExternalUserId;
                            userLink.messageId = param.MessageId;
                            RadarLinkBLL.UpdateItem(userLink);
                        }
                        else
                        {
                            //不为空不覆盖
                            if (userLink.ExternalPjId == null && userLink.GroupId == null)
                            {
                                userLink.ExternalPjId = param.ExternalPjId;
                                userLink.GroupId = param.GroupId;
                                userLink.messageId = param.MessageId;
                                userLink.ReturnUrl = param.returnUrl;
                                userLink.externalUserId = param.ExternalUserId;
                                RadarLinkBLL.UpdateItem(userLink);
                            }
                        }
                        link += userLink.Link;
                        link += "?1=1";
                        link += buildAppendIds(param, false, userLink.Id);

                        break;

                    case 0:
                    case 2:
                        var savedUserLink = RadarLinkBLL.GetRadarLinkByGroupOpenIdOrMobile(param.SuvId, param.ExternalPjId, param.OpenId, param.Mobile);
                        if (savedUserLink == null)
                        {
                            savedUserLink = RadarLinkBLL.GetRadarLinkDefault(param.SuvId);
                            if (savedUserLink == null)
                            {
                                link += "error=radarlinkempty";
                                break;
                            }
                            savedUserLink.IsUsed = 1;
                            savedUserLink.ExternalPjId = param.ExternalPjId;
                            savedUserLink.GroupId = param.GroupId;
                            savedUserLink.OpenId = param.OpenId;
                            savedUserLink.LinkType = param.LinkType;
                            savedUserLink.messageId = param.MessageId;
                            savedUserLink.ReturnUrl = param.returnUrl;
                            savedUserLink.externalUserId = param.ExternalUserId;
                            RadarLinkBLL.UpdateItem(savedUserLink);
                            link += savedUserLink.Link;
                            
                            var background = buildAppendBg(param, true);
                            if (background.Result > -1)
                            {
                               
                                link += '?' + background.data.Remove(0, 1);
                            }
                            else
                            {
                                //  link += "error=usernotexist";
                            }
                            link += buildAppendIds(param, false, savedUserLink.Id);
                        }
                        else
                        {
                            if (savedUserLink.Status != "c") {
                                link += savedUserLink.Link;

                                var background = buildAppendBg(param, true);
                                if (background.Result > -1)
                                {

                                    link += '?' + background.data.Remove(0, 1);
                                }
                                else
                                {
                                    //  link += "error=usernotexist";
                                }
                                link += buildAppendIds(param, false, savedUserLink.Id);
                            }
                            else
                            {
                                savedUserLink.ExternalPjId = param.ExternalPjId;
                                savedUserLink.GroupId = param.GroupId;
                                savedUserLink.LinkType = param.LinkType;
                                savedUserLink.messageId = param.MessageId;
                                savedUserLink.ReturnUrl = param.returnUrl;
                                savedUserLink.externalUserId = param.ExternalUserId;
                                RadarLinkBLL.UpdateItem(savedUserLink);
                                link += externalCompletePage;
                                link += buildAppendIds(param, true, savedUserLink.Id);
                            }
                          
                             
                        }

                        
                        break;
                    default:
                        break;
                }
            }
            link = link.Replace("?1=1&", "?");

            Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept_LogException { AddTime = DateTime.Now, ExceptStackTrace = JsonConvert.SerializeObject(param), ExceptMessage = link, ModuleName = "ConceptController:Redirect" });
            return Redirect(link);
        }

        /// <summary>
        /// 获取聊天数据。
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        public ActionResult GetTopicInfo(string pjId, string groupId, string topicId)
        {
            try
            {
                var data = MessageBLL.GetMessages(pjId, groupId, topicId);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception e) { throw e; }
        }

        public ActionResult SubmitAnswerRedirect(List<DeviceAnswer> answers, bool needRedirect = false)
        {

            NormalListReturn<object> result = new NormalListReturn<object>();
            result.Result = 1;

            if (answers == null || answers.Count == 0)
            {
                result.Result = 0;
                result.Message = "回传数据中没有答案！";
                return Json(result);
            }

            // 保存答案
            answers.ForEach(r =>
            {
                try
                {
                    r.CreateDate = DateTime.Now;
                    int i = DeviceAnswerBLL.AddItem(r);

                    //拆分答案
                    AdoptAnswerBLL.SaveFromDeviceAnswer(r);
                    if (i < 0)
                    {
                        if (result.data == null)
                        {
                            result.data = new List<object>();
                        }

                        result.data.Add(new { QuestionType = r.QuestionType, QuestionId = r.QuestionId });
                    }
                }
                catch (Exception e)
                {
                    if (result.data == null)
                    {
                        result.data = new List<object>();
                    }

                    result.data.Add(new { QuestionType = r.QuestionType, QuestionId = r.QuestionId });
                }
            });

            if (result.data != null && result.data.Count > 0)
            {
                result.Result = 0;
                result.Message = "有提交失败的答案";
                result.TotalCount = result.data.Count;
            }

            ////提交数据到report
            //Sync sync = new Sync(answers.FirstOrDefault().ProjectAutoId);
            //Task<IpsosReport.ModelBiz.External.ReturnData> task=sync.SyncToReport();
            //task.Wait();
            //var data = task.Result;

            //拆分答案
            //跳转到concept链接
            if (needRedirect)
            {
                string returnUrl = ConceptLinkBLL.GetConceptLinkByGroupOpenIdOrMobileByAppend(answers[0].AppendData + "&openId=" + answers[0].DeviceId);
                return Redirect(returnUrl);
            }
            return Json(result);
        }
        /// <summary>
        /// 添加背景资料
        /// </summary>
        /// <param name="link"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        private NormalReturn<string> buildAppendBg(Param param, Boolean showCode = false)
        {
            NormalReturn<string> normal = new NormalReturn<string>();

            string link = string.Empty;
            string[] neededs = new string[] { "手机号", "昵称", "姓名", "性别", "年龄段", "城市级别", "学历", "行业类型", "婚姻状况" };

            var userDatas = VariableAnswerBLL.GetUserData(param.OpenId, param.Mobile);

            if (userDatas.Count == 0)
            {
                normal.Result = -1;
                normal.Message = "用户信息不存在";
                normal.data = "";
                return normal;
            }
            foreach (var item in neededs)
            {
                Variable variable = VariableBLL.GetVarByText(item, param.ExternalPjId);
                if (variable != null)
                {
                    var answer = userDatas.Find(u => u.varId == variable.VarId);
                    if (answer != null)
                    {
                        string finalyResult = "";
                        if (showCode)
                        {
                            var value2 = VariableValue2BLL.GetItemCode(variable.VarId, answer.Value);
                            if (value2 != null)
                            {
                                finalyResult = value2.Code;
                            }
                            else
                            {
                                finalyResult = answer.Value;
                            }

                        }
                        else
                        {
                            finalyResult = answer.Value;
                        }

                        link += "&" + variable.Code + "=" + (finalyResult==""?"99":finalyResult);
                    }
                    else   //没有的数据添加默认值
                    {
                        link += "&"+variable.Code+"=99";
                    }

                }

            }
            normal.data = link;
            return normal;
        }
        /// <summary>
        /// 拼接回传参数
        /// </summary>
        /// <param name="param"></param>
        /// <param name="atFirst">是否直接拼在首个参数（?）,不是的话&</param>
        /// <param name="id"></param>
        /// <returns></returns>
        private string buildAppendIds(Param param, bool atFirst, int id = -1)
        {
            string link = string.Empty;
            if (atFirst)
            {
                link += "?externalpjid=" + param.ExternalPjId;

            }
            else
            {

                link += "&externalpjid=" + param.ExternalPjId;
            }
            link += "&SuvId=" + param.SuvId;
            link += "&GroupId=" + param.GroupId;
            link += "&returnUrl=" + HttpUtility.UrlEncode(param.returnUrl);
            link += "&externalUserId=" + param.ExternalUserId;
            link += "&messageId=" + param.MessageId;
             link += "&ccQnttvMtrlDtlId=" + param.MessageId;
            //link += "&Mobile=" + param.Mobile;
            link += "&openId=" + param.OpenId;
            link += "&linktype=" + param.LinkType;
            if (id > 0)
            {
                link += "&radarId=" + id;
            }
            return link;
        }



    }
}