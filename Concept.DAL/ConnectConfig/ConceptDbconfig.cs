﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Data;
using ORM;

namespace Concept.DAL.ConnectConfig
{ 
    public class ConceptDbconfig
    {
        private static Database _read;
        private static object readLockObj = new object();
        /// <summary>
        /// 数据库读节点
        /// </summary>
        public static Database Read
        {
            get
            {
                if (_read == null)
                {
                    lock (readLockObj) { return _read ?? (_read = CreateDatabase("ConceptRead")); }
                }
                return _read;
            }
        }

        private static Database _write;
        private static object writeLockObj = new object();
        /// <summary>
        /// 数据库写节点
        /// </summary>
        public static Database Write
        {
            get
            {
                if (_write == null)
                {
                    lock (writeLockObj) { return _write ?? (_write = CreateDatabase("ConceptWrite")); }
                }
                return _write;
            }
        }

        private static Database _owner;
        private static object ownerLockObj = new object();
        /// <summary>
        /// 数据库管理节点
        /// </summary>
        public static Database Owner
        {
            get
            {
                if (_owner == null)
                {
                    lock (ownerLockObj) { return _owner ?? (_owner = CreateDatabase("ConceptOwner")); }
                }
                return _owner;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static Database CreateDatabase(string name)
        {
            Database db = null;
            try
            {
                db = DatabaseFactory.CreateDatabase(name);
            }
            catch { }
            if (db == null)
            {
                db = DatabaseFactory.CreateDatabase(string.Concat("Zhengxin_", name));
            }
            return db;
        }

        public static GetDb GetRead = delegate() { return Read; };
        public static GetDb GetWrite = delegate() { return Write; };
        public static GetDb GetNormal = delegate() { return Owner; };
    }
}
