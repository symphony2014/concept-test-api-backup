﻿using Concept.DAL.ConnectConfig;
using Concept.Entity.BaseDB;
using ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.DAL.BaseDB
{
    public class Mapping_ServiceConfigDAL : DAL<Mapping_ServiceConfig>
    {
        public Mapping_ServiceConfigDAL()
        {
            initDb(ConceptDbconfig.GetNormal, ConceptDbconfig.GetRead, ConceptDbconfig.GetWrite);
        }
    }
}
