﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.ConnectConfig;
using Concept.Entity.BaseDB.Log;
using ORM;

namespace Concept.DAL.BaseDB.Log
{
    public class Concept_LogExceptionDAL: DAL<Concept_LogException>
    {
        public Concept_LogExceptionDAL()
        {
            initDb(ConceptDbconfig.GetNormal, ConceptDbconfig.GetRead, ConceptDbconfig.GetWrite);
        }
    }
}
