﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.ConnectConfig;
using Concept.Entity.BaseDB;
using Concept.Entity.SysMode;
using ORM;

namespace Concept.DAL.BaseDB
{
    public class TaskParamLogDLL : DAL<TaskParamLog>
    {
        public TaskParamLogDLL()
        {
            initDb(ConceptDbconfig.GetNormal, ConceptDbconfig.GetRead, ConceptDbconfig.GetWrite);
        }
    }
}
