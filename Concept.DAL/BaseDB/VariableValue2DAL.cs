﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.ConnectConfig;
using Concept.Entity.BaseDB;
using ORM;

namespace Concept.DAL.BaseDB
{
    public class VariableValue2DAL: DAL<VariableValue2>
    {
        public VariableValue2DAL()
        {
            initDb(ConceptDbconfig.GetNormal, ConceptDbconfig.GetRead, ConceptDbconfig.GetWrite);
        }
    }
}
