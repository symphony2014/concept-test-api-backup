﻿using Concept.DAL.ConnectConfig;
using Concept.Entity.BaseDB;
using ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.DAL.BaseDB
{
    public class PGRuleInfoDAL : DAL<PGRuleInfo>
    {
        public PGRuleInfoDAL()
        {
            initDb(ConceptDbconfig.GetNormal, ConceptDbconfig.GetRead, ConceptDbconfig.GetWrite);
        }
    }
}
