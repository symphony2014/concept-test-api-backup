﻿using Concept.BLL.BaseBD;
using Concept.BLL.BaseBD.Log;
using Concept.Entity.BaseDB;
using Concept.Entity.EnumMode;
using Concept.Entity.External;
using Concept.Entity.SysMode;
using ExternalAPI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptSys.BLL.Interface
{
    public class GetMessagesJobServiceRun : MappingServiceBase<TaskParamLog>
    {

        readonly string TopicPath = ConfigurationManager.AppSettings["TopicPath"];
        //readonly string TopicPath = @"E:\xiaolin.niu\servicelauch\Resources";
        readonly bool Debug = Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]);
        protected override List<TaskParamLog> LoadExpectedProcessTaskItems()
        {
            int dataSize = Convert.ToInt32(ConfigurationManager.AppSettings["GetMessagesJobServiceRunPerProcess"]);
            //int dataSize = 5;
            List<TaskParamLog> result = TaskParamLogBLL.LoadNeedProcessItem(dataSize, ParamType.getMessages);
            return result;

        }

        protected override void MarkupHasProcessingStatus(List<TaskParamLog> hasProcessingItems)
        {
            if (hasProcessingItems == null || hasProcessingItems.Count == 0)
            {
                throw new ArgumentNullException("hasProcessingItems");
            }

            Parallel.ForEach<TaskParamLog>(
                                                hasProcessingItems,
                                                new ParallelOptions() { MaxDegreeOfParallelism = 4 },
                                                taskItem =>
                                                {
                                                    taskItem.status = Convert.ToInt32(SearchStatusEnum.dealing);
                                                }
                                              );


            TaskParamLogBLL.BulkUpdateItems(hasProcessingItems, new string[] { "status" });
        }

        protected override void ProcessSingleTaskItem(TaskParamLog singleTaskItem)
        {
            try
            {

                Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept.Entity.BaseDB.Log.Concept_LogException { AddTime = DateTime.Now, ModuleName = "GetMessagesJob", ExceptMessage = "", MessDescript = "GetMessagesJobStart..." });
                string date = MessageBLL.GetLastUpdate(singleTaskItem.prjId, singleTaskItem.sampGrpId, singleTaskItem.topicId);
                ExternalTopicParam param = new ExternalTopicParam();
                param.parms = new param
                {
                    sttm = date,
                    prjId = singleTaskItem.prjId,
                    sampGrpId = singleTaskItem.sampGrpId,
                    tpcId = singleTaskItem.topicId
                };
                Task<ExternalMessage<Message>> topicInfo = Import.GetMessageInfo(param);


                topicInfo.Wait();


                if (topicInfo.Result.data.rows.Count > 0)
                {
                    foreach (var item in topicInfo.Result.data.rows)
                    {
                        item.ExternalPjId = singleTaskItem.prjId;
                        //update userIcon
                        item.usericon = VariableAnswerBLL.GetUserIconByOpenId(item.openId, item.ExternalPjId);
                        MessageBLL.AddItem(item);
                    }

                    //start baidu AI task.


                    ////start generate excel task.
                    //TaskParamLogBLL.AddItem(new TaskParamLog
                    //{
                    //    prjId = singleTaskItem.prjId,
                    //    sampGrpId = singleTaskItem.sampGrpId,
                    //    topicId = singleTaskItem.topicId,
                    //    status = 0,
                    //    AddDate = DateTime.Now,
                    //    paramType = Convert.ToInt32(ParamType.excelGenerate),
                    //    Data = TopicPath
                    //});
                }
                TaskParamLogBLL.AddItem(new TaskParamLog
                {
                    prjId = singleTaskItem.prjId,
                    sampGrpId = singleTaskItem.sampGrpId,
                    topicId = singleTaskItem.topicId,
                    status = 0,
                    AddDate = DateTime.Now,
                    paramType = Convert.ToInt32(ParamType.baiduAI)
                });

                singleTaskItem.status = Convert.ToInt32(SearchStatusEnum.done);
                TaskParamLogBLL.BulkUpdateItems(new List<TaskParamLog> { singleTaskItem }, new string[] { "status" });
            }
            catch (Exception e)
            {
                Concept_LogExceptionBLL.InsertLogExceptionInfo(new Concept.Entity.BaseDB.Log.Concept_LogException { AddTime = DateTime.Now, ModuleName = "GetMessagesJob", ExceptMessage = e.ToString(), MessDescript = "GetMessagesJobStart..." });
            }

        }

        protected override int DefaultParallelCount
        {
            get
            {
                // 设置到最大4并发量。
                return 4;
            }
        }
    }
}
