﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptSys.BLL.Interface.Log
{
    public class WriteLogModel
    {
        public WriteLogModel()
        {
            this.ExceptionModule = new ExceptionModel();
        }

        /// <remarks/>
        public ExceptionModel ExceptionModule { get; set; }

    }

    public class ExceptionModel
    {
        //模块名字
        public string ModuleName { set; get; }
        //ExceptMessage  用来区分Mess和
        public string ExceptMessage { set; get; }
        //ExceptStackTrace信息
        public string ExceptStackTrace { set; get; }
        //错误描述
        public string ExcptionDescript { set; get; }
    }

}
