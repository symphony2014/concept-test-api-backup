﻿
using Concept.BLL.BaseBD.Log;
using Concept.Entity.BaseDB.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 

namespace ConceptSys.BLL.Interface.Log
{
    public class WirteLogToDb
    {
        public static bool bOpen = true;
        /// <summary>
        /// 日志Exception
        /// </summary>
        public static int WriteLogToLogExceptionDB(WriteLogModel model)
        {
            if (model == null || model.ExceptionModule == null)
            {
                return -1;
            }

            Concept_LogException logException = new Concept_LogException();
            //模块位置
            logException.ModuleName = model.ExceptionModule.ModuleName;
            //信息描述
            logException.ExceptMessage = model.ExceptionModule.ExceptMessage;
            //ExceptStackTrace
            logException.ExceptStackTrace = model.ExceptionModule.ExceptStackTrace;
            //MessDescript
            logException.MessDescript = model.ExceptionModule.ExcptionDescript;
            //数据库存储
            int result = Concept_LogExceptionBLL.InsertLogExceptionInfo(logException);
            return result;
        }

        ///// <summary>
        ///// 日志
        ///// </summary>
        //public static void WriteLogToLogMessageDB(WriteLogModel model)
        //{
        //    if (model.messageModel != null)
        //    {
        //        if (bOpen)
        //        {
        //            LogMessage logMessage = new LogMessage();
        //            string strOderNum = string.Empty;
        //            bool bGetNum = GetOderNum(out strOderNum, model.messageModel.MessDescript);
        //            if (bGetNum)
        //            {
        //                if (!string.IsNullOrEmpty(strOderNum))
        //                {
        //                    logMessage.ModuleName = model.messageModel.ModuleName + "[订单号]:" + strOderNum;
        //                }
        //            }
        //            else
        //            {
        //                logMessage.ModuleName = model.messageModel.ModuleName;
        //            }
        //            logMessage.MessDescript = model.messageModel.MessDescript;
        //            if (model.messageModel.ExceptionID > 0)
        //            {
        //                logMessage.ExcepID = model.messageModel.ExceptionID;
        //            }
        //            logMessage.RelatedIdentityName = model.messageModel.RelatedIdentityName;
        //            logMessage.RequestHost = model.messageModel.RequestHost;
        //            logMessage.RequestStatus = model.messageModel.RequestStatus;
        //            LogMessageBLL.InsertLogMessageInfo(logMessage);
        //        }
        //    }
        //}
        
    }
}
