﻿
using ConceptSys.BLL.Interface.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptSys.BLL.Interface
{
    public class WriteLogMess : IWriteLog
    {
        #region 单例定义
        private static object lockObject = new object();
        protected WriteLogMess()
        {
        }

        private static volatile WriteLogMess _instance;
        public static WriteLogMess Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new WriteLogMess());
                    }
                }
                return _instance;
            }
        }

        #endregion 单例定义

        public int WritreLogExption(Exception ex, string strlocation)
        {
            int result = -1;
            try
            {
                WriteLogModel writeLogModel = new WriteLogModel();
                writeLogModel.ExceptionModule.ModuleName = strlocation;
                writeLogModel.ExceptionModule.ExceptMessage = ex.Message;
                writeLogModel.ExceptionModule.ExceptStackTrace = ex.StackTrace;
                result = WirteLogToDb.WriteLogToLogExceptionDB(writeLogModel);
            }
            catch(Exception ex1)
            {
                string r = ex1.ToString();
            }

            return result;
        }

    }
}
