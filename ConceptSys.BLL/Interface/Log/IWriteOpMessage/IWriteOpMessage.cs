﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptSys.BLL.Interface
{
    public interface IWriteOpMessage
    {
        int WriteOp(WriteOpMsgContext context);
    }

    public class WriteOpMsgContext
    {
        public Exception ExInstance {get; set;}
        
        public string OperationName {get; set;}

        public string Msg { get; set; }

        public string ErrorContext { get; set; }
    }
}
