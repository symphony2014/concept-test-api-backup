﻿
using Concept.BLL.BaseBD.Log;
using Concept.Entity.BaseDB.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 

namespace ConceptSys.BLL.Interface
{
    public class WriteOpMessageProvider : IWriteOpMessage
    {
        #region 单例定义
        private static object lockObject = new object();
        protected WriteOpMessageProvider()
        {
        }

        private static volatile WriteOpMessageProvider _instance;
        public static WriteOpMessageProvider Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new WriteOpMessageProvider());
                    }
                }
                return _instance;
            }
        }

        #endregion 单例定义

        protected virtual IWriteLog WriteLogFuncProvider
        {
            get
            {
                return WriteLogMess.Instance;
            }
        }

        public int WriteOp(WriteOpMsgContext context)
        {
            if (context == null || string.IsNullOrEmpty(context.OperationName) || string.IsNullOrEmpty(context.Msg))
             {
                 throw new ArgumentNullException("context");
             }

            int result = this.WriteOpLogic(context);
            return result;
        }

        protected virtual int WriteOpLogic(WriteOpMsgContext context)
        {
            int exId = 0;
            if (context.ExInstance != null)
            {
                exId = this.SaveException(context);
            }

            Concept_OpMessage opMsg = new Concept_OpMessage() 
            {
                ErrorOperation = context.OperationName,
                Msg = context.Msg
            };

            List<string> cols = new List<string>() { "ErrorOperation", "Msg" };
            if (exId > 0)
            {
                opMsg.LogExceptionId = exId;
                cols.Add("LogExceptionId");
            }

            if (!string.IsNullOrEmpty(context.ErrorContext))
            {
                opMsg.ErrorContext = context.ErrorContext;
                cols.Add("ErrorContext");
            }

               int result = Concept_OpMessageBLL.AddOpMessage(opMsg, cols.ToArray<string>());
             return result;
        }

        protected virtual int SaveException(WriteOpMsgContext context)
        {
            if (context.ExInstance == null)
            {
                throw new ArgumentNullException("context.ExInstance");
            }

            int exItemId = this.WriteLogFuncProvider.WritreLogExption(context.ExInstance, context.OperationName);
            return exItemId;
        }

    }
}
