﻿using Concept.BLL.BaseBD;
using Concept.BLL.BaseBD.Log;
using Concept.Entity.BaseDB;
using Concept.Entity.EnumMode;
using Concept.Entity.External;
using Concept.Entity.SysMode;
using ExternalAPI;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptSys.BLL.Interface
{
    public class GenerateExcelJobServiceRun: MappingServiceBase<TaskParamLog>
    {
    

        protected override List<TaskParamLog> LoadExpectedProcessTaskItems()
        {
            int dataSize = Convert.ToInt32(ConfigurationManager.AppSettings["GenerateExcelJobServiceRunPerProcess"]);
            //int dataSize = 5;
            List<TaskParamLog> result = TaskParamLogBLL.LoadNeedProcessItem(dataSize,ParamType.excelGenerate);
              return result;

        }

        protected override void MarkupHasProcessingStatus(List<TaskParamLog> hasProcessingItems)
        {
            if (hasProcessingItems == null || hasProcessingItems.Count == 0)
            {
                throw new ArgumentNullException("hasProcessingItems");
            }

            Parallel.ForEach<TaskParamLog>(
                                                hasProcessingItems,
                                                new ParallelOptions() { MaxDegreeOfParallelism = 4 },
                                                taskItem =>
                                                {
                                                    taskItem.status = Convert.ToInt32(SearchStatusEnum.dealing);
                                                }
                                              );


            TaskParamLogBLL.BulkUpdateItems(hasProcessingItems, new string[] { "status" });
        }

        protected override void ProcessSingleTaskItem(TaskParamLog singleTaskItem)
        {
            Import.GenreateExcel(singleTaskItem.Data, singleTaskItem.prjId, singleTaskItem.sampGrpId, singleTaskItem.topicId);
            singleTaskItem.status = Convert.ToInt32(SearchStatusEnum.done);
            TaskParamLogBLL.BulkUpdateItems(new List<TaskParamLog> { singleTaskItem}, new string[] { "status" });

        
        }

        protected override int DefaultParallelCount
        {
            get
            {
                // 设置到最大4并发量。
                return 4;
            }
        }
    }
}
