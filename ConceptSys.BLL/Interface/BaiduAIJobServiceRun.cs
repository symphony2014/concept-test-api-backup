﻿using Concept.BLL.BaseBD;
using Concept.BLL.BaseBD.Log;
using Concept.Entity.BaseDB;
using Concept.Entity.EnumMode;
using Concept.Entity.External;
using Concept.Entity.SysMode;
using ExternalAPI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConceptSys.BLL.Interface
{
    public class BaiduAIJobServiceRun : MappingServiceBase<TaskParamLog>
    {


        protected override List<TaskParamLog> LoadExpectedProcessTaskItems()
        {
            int dataSize = Convert.ToInt32(ConfigurationManager.AppSettings["BaiduAIJobServiceRun"]);
            //int dataSize = 5;
            List<TaskParamLog> result = TaskParamLogBLL.LoadNeedProcessItem(dataSize, ParamType.baiduAI);
            return result;

        }

        protected override void MarkupHasProcessingStatus(List<TaskParamLog> hasProcessingItems)
        {
            if (hasProcessingItems == null || hasProcessingItems.Count == 0)
            {
                throw new ArgumentNullException("hasProcessingItems");
            }

            Parallel.ForEach<TaskParamLog>(
                                                hasProcessingItems,
                                                new ParallelOptions() { MaxDegreeOfParallelism = 4 },
                                                taskItem =>
                                                {
                                                    taskItem.status = Convert.ToInt32(SearchStatusEnum.dealing);
                                                }
                                              );


            TaskParamLogBLL.BulkUpdateItems(hasProcessingItems, new string[] { "status" });
        }

        protected override void ProcessSingleTaskItem(TaskParamLog singleTaskItem)
        {
            var messages = MessageBLL.GetMessages(singleTaskItem.prjId, singleTaskItem.sampGrpId, singleTaskItem.topicId);
            for(var i=0;i<messages.Count();i++)
            {
                messages[i].username = VariableAnswerBLL.GetUserNameByOpenId(messages[i].openId);
                messages[i].usericon= VariableAnswerBLL.GetUserIconByOpenId(messages[i].openId);
            }
            ////生成词云
            //remove AIData 
            AIDataBLL.Remove(singleTaskItem.prjId, singleTaskItem.sampGrpId, singleTaskItem.topicId);
            var result = BaiduAI.GetcommentTags(messages.Where(m=>m.msgTp=="1").ToList());

           // if (result.HasData)
            //{
                ExternalJsonBLL.AddItem(new ExternalJson
                {
                    JSON = JsonConvert.SerializeObject(result),
                    ExternalPjId = singleTaskItem.prjId,
                    GroupId = singleTaskItem.sampGrpId,
                    TopicId = singleTaskItem.topicId,
                    UpdateTime = DateTime.Now,
                    type = Convert.ToInt32(AIType.wordCloud)
                });
            //}


            singleTaskItem.status = Convert.ToInt32(result.HasData? SearchStatusEnum.done: SearchStatusEnum.error);
            TaskParamLogBLL.BulkUpdateItems(new List<TaskParamLog> { singleTaskItem }, new string[] { "status" });


        }

        protected override int DefaultParallelCount
        {
            get
            {
                // 设置到最大4并发量。
                return 4;
            }
        }
    }
}
