﻿using Concept.BLL.BaseBD;
using Concept.Entity.BaseDB;
using ConceptCommon;
using ConceptCommon.Interface;
using ConceptCommon.Interface.ServiceRunner;
using ConceptCommon.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptSys.BLL.Interface.ServiceRun.ServiceLauncher
{
    public class MappingServiceLauncher : ServiceRunner<Mapping_ServiceConfig>, IServiceRun
    {
        protected IWriteOpMessage WriteOpFuncProvider
        {
            get
            {
                return WriteOpMessageProvider.Instance;
            }
        }
        public void Process()
        {
            try
            {
                this.Run();
            }
            catch (InvalidOperationException exinvalid)
            {
                WriteOpMsgContext opContext = new WriteOpMsgContext()
                {
                    Msg = @"MappingServiceLauncher.Process",
                    ErrorContext = exinvalid.ToString() + "|||" + exinvalid.InnerException.ToString(),
                    OperationName = @"MappingServiceLauncher\Process"
                };

                this.WriteOpFuncProvider.WriteOp(opContext);

            }
            catch (Exception ex)
            {
                // this.WriteLogProvider.WritreLogExption(ex, @"CbankUploadServiceLoader/Process");
                WriteOpMsgContext opContext = new WriteOpMsgContext()
                {
                    Msg = @"MappingServiceLauncher.Process",
                    ErrorContext = ex.ToString() + "|||" + ex.InnerException.ToString(),
                    OperationName = @"MappingServiceLauncher\Process"
                };
                this.WriteOpFuncProvider.WriteOp(opContext);
            }
        }

        /// <summary>
        /// 从配置文件指定的Config Name 读取期待要跑的Item.
        /// </summary>
        protected override string RunItemSettingConfigName
        {
            get
            {
                return "MappingServiceModelName";
            }
        }

        protected override string IServiceRunImplementDllName
        {
            get
            {
                //return "CbankQueryUpLoadData.dll";
                return "ConceptSys.BLL.dll";
            }
        }

        protected override string GetProcessorDisplayName(Mapping_ServiceConfig configItem)
        {
            if (configItem == null || string.IsNullOrEmpty(configItem.CqModelName))
            {
                throw new ArgumentNullException("configItem");
            }

            // 这里用CqModelName标识服务显示名字，配置文件中的"ChongQingReportModelName" 也使用一致的文字来标识启动那些服务。
            return configItem.CqModelName;
        }

        protected override string GetProcessImplementationInfo(Mapping_ServiceConfig configItem)
        {
            if (configItem == null || string.IsNullOrEmpty(configItem.Implementation))
            {
                throw new ArgumentNullException("configItem");
            }

            // 这里使用SloanChongQ_ServiceConfig 配置表的 Implementation字段。
            return configItem.Implementation;
        }

        /// <summary>
        /// 根据配置文件的配置，从配置表中取得对应的配置项。
        /// </summary>
        /// <returns></returns>
        protected override List<Mapping_ServiceConfig> LoadExpectedRunConfigurationItems()
        {
            string[] expectedItems;
            List<Mapping_ServiceConfig> result = null;

            // 如果是跑所有的。
            if (this.VerifyDeployServiceItemsIsAll(out expectedItems))
            {
                result = Mapping_ServiceConfigBLL.LoadAllConfigItems();
            }
            else // 如果是某些指定的。
            {
                result = Mapping_ServiceConfigBLL.LoadSpecifiedEnableItems(expectedItems);
            }

            return result;
        }

        protected override void LaunchProcessByConfigItem(Mapping_ServiceConfig configItem, IServiceRun serviceRunImplementItem)
        {
            if (serviceRunImplementItem == null)
            {
                throw new ArgumentNullException("serviceRunImplementItem");
            }

            int runningTypeId = configItem.RunningTypeId;
            if (runningTypeId <= 0)
            {
                throw new ArgumentException("runningTypeId 应该大于0", "configItem");
            }

            switch (runningTypeId)
            {
                case (int)ServiceRunningType.SettingRun:
                    {
                        this.LaunchSettingRun(configItem, serviceRunImplementItem);
                        break;
                    }
                case (int)ServiceRunningType.CtyleRun:
                    {
                        // 开启一个间隔run 的线程。
                        this.LaunchCtyleRun(configItem, serviceRunImplementItem);
                        break;
                    }
                default:
                    {
                        throw new InvalidOperationException(string.Format("当前不支持该RunningTypeId:[{0}]", runningTypeId));
                    }
            }
        }

        protected virtual void LaunchSettingRun(Mapping_ServiceConfig configItem, IServiceRun serviceRunImplementItem)
        {
            if (string.IsNullOrEmpty(configItem.ExpectedStartTime))
            {
                throw new ArgumentNullException("configItem");
            }

            DateTime expectedStartTime;
            if (!DateTimeHelper.TryParseDateTimeWithPattern(configItem.ExpectedStartTime, "HH:mm", out expectedStartTime))
            {
                throw new ArgumentException(string.Format("ExpectedStartTime 输入格式不对,应为[HH:mm]格式, 当前[{0}]", configItem.ExpectedStartTime), "configItem");
            }

            // 新起settingRunProvider 实例，在这个实例中线程安全。
            ISettingRun settingRunProvider = this.GetSettingRunProvider();
            settingRunProvider.SettingRun(serviceRunImplementItem, expectedStartTime);
        }

        protected virtual void LaunchCtyleRun(Mapping_ServiceConfig configItem, IServiceRun serviceRunImplementItem)
        {
            int interval = configItem.RunInterval;
            if (interval <= 0)
            {
                // 如果没有，默认值设置为5秒
                interval = 5000;  //1000毫秒=1秒
            }

            ISettingRun settingRunProvider = this.GetSettingRunProvider();

            // 开启一个间隔run 的线程。
            settingRunProvider.CtycleRun(serviceRunImplementItem, new TimeSpan(0, 0, 0, 0, interval), null);
        }

    }
}
