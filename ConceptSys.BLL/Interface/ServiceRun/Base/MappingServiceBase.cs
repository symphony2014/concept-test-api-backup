﻿using ConceptCommon.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptSys.BLL.Interface 
{
    public abstract class MappingServiceBase<T> : IServiceRun, IServiceParameterRun
         where T : class, new()
    {
        protected virtual int DefaultParallelCount
        {
            get
            {
                return 10;
            }
        }

        protected IConvertJson ConvertJsonFuncProvider
        {
            get
            {
                return ConvertJsonProvider.Instance;
            }
        }

        protected IWriteOpMessage WriteOpFuncProvider
        {
            get
            {
                return WriteOpMessageProvider.Instance;
            }
        }

        public void Process()
        {
            List<T> expectedProcessItems = this.LoadExpectedProcessTaskItems();
            if (expectedProcessItems == null || expectedProcessItems.Count == 0)
            {
                return;
            }

            // Updated Process status
            this.MarkupHasProcessingStatus(expectedProcessItems);

            this.ProcessWithParas(expectedProcessItems);
        }

        public void ProcessWithParas(params object[] paramets)
        {
            if (paramets == null || paramets.Length == 0)
            {
                throw new ArgumentNullException("paramets");
            }

            List<T> expectedProcessItems = paramets[0] as List<T>;
            if (expectedProcessItems == null || expectedProcessItems.Count == 0)
            {
                throw new ArgumentException("Could not get correct parameters", "paramets");
            }

            // 根据Task items的数量进行并发处理。
            int parallelCount = expectedProcessItems.Count;
            int defaultParallelCount = this.DefaultParallelCount;
            if (parallelCount > defaultParallelCount)
            {
                parallelCount = defaultParallelCount;
            }

            Parallel.ForEach<T>(
                                expectedProcessItems,
                                new ParallelOptions() { MaxDegreeOfParallelism = parallelCount },
                                item =>
                                {
                                    try
                                    {
                                        this.ProcessSingleTaskItem(item);
                                    }
                                    catch (Exception ex)
                                    {
                                        string s = ex.ToString();
                                        WriteOpMsgContext opContext = new WriteOpMsgContext()
                                        {
                                            Msg = ex.ToString(),
                                            ErrorContext = ex.ToString(),
                                            OperationName = @"MappingServiceBase\ProcessWithParas\ProcessSingleTaskItem",
                                            ExInstance = ex
                                        };

                                        this.WriteOpFuncProvider.WriteOp(opContext);
                                    }
                                }
                                );
        }



        protected abstract List<T> LoadExpectedProcessTaskItems();

        protected abstract void MarkupHasProcessingStatus(List<T> hasProcessingItems);

        protected abstract void ProcessSingleTaskItem(T singleTaskItem);
    }
}
