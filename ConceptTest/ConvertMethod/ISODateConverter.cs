﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace ConceptTest.ConvertMethod
{
    public class ISODateConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType.GetType() == typeof(DateTime);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            string value = reader.Value.ToString();

            var result = Regex.Replace(value, @"[^\x20-\x7F]", "");
            return DateTime.Parse(result, CultureInfo.InvariantCulture);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(value.ToString());
            writer.Flush();
        }
    }
}
